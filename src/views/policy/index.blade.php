@extends('pleaf-common::layouts.master')

@section('head')
<script src="/sts/pleaf-common/js/policy.js"></script>

    @authorize('editPolicy')
        <style type="text/css">
            .task-editPolicy {
                visibility: inherit;
            }
        </style>
    @endauthorize
    @authorize('removePolicy')
        <style type="text/css">
            .task-removePolicy {
                visibility: inherit;
            }
        </style>
    @endauthorize

@endsection

@section('title')
        Policy
@endsection

@section('page-title')
        Policy
@endsection

@section('bread-crumb')
        <li class="active">
            <a href="{{URL::to('admin')}}">Home</a>

        </li>

        <li class="active">
            <strong>Policy</strong>

        </li>
        
@endsection

@section('content-title')
    Policy Table
@endsection

@section('content')

@messages
@authorize('viewPolicy')
		<div ng-app="policyApp">
    		<div ng-controller="policyCtrl">
				@authorize('addPolicy')
					<a href="{{URL::to('admin/policy/create')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Add</a>&nbsp
				@endauthorize
				<a href="{{URL::to('admin/policy')}}" class="btn btn-primary"><i class="fa fa-refresh"></i> Refresh</a>
			<div class="form-group">
			</br></br>

	            <div id="gridOptions"></div>

	    		</div>
			</div>
		</div>
@endauthorize
@endsection

{{--<script>--}}
    {{--function policyEdit(editId) {--}}

        {{--var policyEditId = $("#gridOptions").data("kendoGrid").dataItem($(editId).closest("tr"));--}}
        {{--window.location.href='/admin/policy/edit/'+policyEditId.policy_id;--}}

    {{--}--}}
    {{--function deletepolicy(deleteId) {--}}

        {{--var ok = confirm('Are You Sure ?');--}}
        {{--if(ok == true){--}}
            {{--var policyDeleteId = $("#gridOptions").data("kendoGrid").dataItem($(deleteId).closest("tr"));--}}
            {{--window.location.href='/admin/policy/delete/'+policyDeleteId.policy_id;--}}
        {{--}--}}

    {{--}--}}
{{--</script>--}}
