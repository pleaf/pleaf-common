@extends('pleaf-common::layouts.master')
@section('title')
		Policy
@endsection

@section('page-title')
        Policy
@endsection

@section('bread-crumb')
        <li class="active">
            <a href="{{URL::to('admin')}}">Home</a>

        </li>
        <li class="active">
            <a href="{{URL::to('admin/policy')}}">Policy</a>

        </li>
        
        <li class="active">
            <strong>Edit Policy</strong>
        </li>

        
@endsection

@section('content-title')
    Edit Policy Form
@endsection 


@section('content')

@messages("onField")            

        <form action="{{URL::to('admin/policy/update')}}"  method="post" class="form-horizontal">
            <input type="hidden" value="{{$data_policy->policy_id}}" name="policy_id" id='policy_id'>
                <input type="hidden" name="version" value="{{$data_policy->version}}"> 
               
                <div class="form-group row">
                     <div class="col-md-2">
	                    <label>
	                        Policy Code 
	                    </label>
                     </div>
                     
                    <div class="col-md-4">
                        <input type="text" id="policy_code" name="policy_name" class="form-control" value= "{{$data_policy->policy_code}}" disabled>
                    </div>
                </div>

                <div class="form-group row">
                     <div class="col-md-2">
	                    <label>
	                        Policy Name
	                    </label>
                     </div>
                     
                    <div class="col-md-4">
                        <input type="text" id="policy_name" name="policy_name" class="form-control" value= "{{$data_policy->policy_name}}">
                    </div>
                 </div>
                 
                                 <div class="form-group row">
                     <div class="col-md-2">
	                    <label>

	                    </label>
                     </div>
                     
                    <div class="col-md-4">
				          <a href="{{URL::to('admin/policy')}}" class="btn btn-white">Cancle</a> &nbsp
				          <button type="submit" class="btn btn-primary">Save</button>
				            {{csrf_field()}}
                    </div>
                 </div>


        </form>

@endsection