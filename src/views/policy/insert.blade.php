@extends('pleaf-common::layouts.master')
@section('title')
        Policy
@endsection
@section('page-title')
        Policy
@endsection

@section('bread-crumb')
        <li class="active">
            <a href="{{URL::to('admin')}}">Home</a>

        </li>
        <li class="active">
            <a href="{{URL::to('admin/policy')}}">Policy</a>

        </li>
        
        <li class="active">
            <strong>Add Policy</strong>
        </li>

        
@endsection

@section('content-title')
    Add New Policy Form
@endsection 

@section('content')
@messages("onField")
    <div class="form-group row">
        <div class="col-md-2">

        </div>
        <div class="col-md-4">
            <div id="tenant_id"></div>
        </div>
    </div>
    
        <form action="{{URL::to('admin/policy/store')}}" method="post" class="form-horizontal">
  
                 
                 <div class="form-group row">
                     <div class="col-md-2">
                 	   <label for="policy_code">
                   	     Policy Code
                   	   </label>
                     </div>
                     
                    <div class="col-md-4">
                        <input type="text" id="policy_code" name="policy_code" class="form-control" placeholder= "Policy Code">
                    </div>
                 </div>

                 <div class="form-group row">
                     <div class="col-md-2">
                    	<label for="policy_name">
                       		Policy Name
                   	 	</label>
                     </div>
                     
                     <div class="col-md-4">
                     	   <input type="text" id="policy_name" name="policy_name" class="form-control" placeholder= "Policy Name">
                     </div>
                 </div>
                 
                 <div class="form-group row">
                     <div class="col-md-2">
                    	<label>

                    	</label>
                     </div>
                     
                 <div class="col-md-4">
                 	   <br>
				       <a href="{{URL::to('admin/policy')}}" class="btn btn-white">Cancel</a> &nbsp     
				       <button type="submit" class="btn btn-primary">Save</button>
				       {{csrf_field()}}
                    </div>
                 </div>
                              
        </form>
   

@endsection