<!DOCTYPE html>
<html>
<head>
	
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Reset password</title>

    <link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">

    <link href="{{URL::asset('css/animate.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/style.css')}}" rel="stylesheet">
</head>
<body class="gray-bg">
	
		<div class="middle-box text-center loginscreen  animated fadeInDown">
        <div>
            <div>

                <h1 class="logo-name">IN+</h1>

            </div>
            
            <p>Reset Password</p>
            <form class="m-t" role="form" action="{{URL::to('updatepasswords')}}" method="post">
                <input type="hidden" value="{{$email}}" name="email"></input>
                <input type="hidden" value="{{$key}}" name="randomkey"></input>

                <div class="form-group">
                    <label>New Password</label>
                    <input type="password" class="form-control" required="" name="new_password">
                </div>
                <div class="form-group">
                    <label>Confirm Password</label>
                    <input type="password" class="form-control" required="" name="confirm_password">
                </div>

                <button type="submit" class="btn btn-primary block full-width m-b">Save</button>
        
                
                
                {{csrf_field()}}
            </form>
            <p class="m-t"> <small>Inspinia we app framework base on Bootstrap 3 &copy; 2014</small> </p>
        </div>
    </div>
		
		
		
	<script src="{{URL::asset('js/jquery-2.1.1.js')}}"></script>
    <script src="{{URL::asset('js/bootstrap.min.js')}}"></script>
	


</body>
</html>