<!DOCTYPE html>
<html>
<head>
	
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Forgot password</title>

    <link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">

    <link href="{{URL::asset('css/animate.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/style.css')}}" rel="stylesheet">
</head>
<body class="gray-bg">
	
		<div class="middle-box text-center loginscreen  animated fadeInDown">
        <div>
            <div>

                <h1 class="logo-name">IN+</h1>

            </div>
            
            <p>Reset Password</p>
            <form class="m-t" role="form" action="{{URL::to('sendmailresets')}}" method="post">

                <div class="form-group">
                    <input type="text" class="form-control" placeholder="e.g. xxx@example.com" required="" name="email">
                </div>
                

                <button type="submit" class="btn btn-primary block full-width m-b">Send E-Mail Verification</button>
        
                
                
                {{csrf_field()}}
            </form>
            <p class="m-t"> <small>Inspinia we app framework base on Bootstrap 3 &copy; 2014</small> </p>
        </div>
    </div>
		
		
		
	<script src="{{URL::asset('js/jquery-2.1.1.js')}}"></script>
    <script src="{{URL::asset('js/bootstrap.min.js')}}"></script>
	


</body>
</html>