@extends('pleaf-common::layouts.master')
@section('title')
	Change Tenant
@endsection
@section('page-title')
	Change Tenant
@endsection
@section('bread-crumb')
	<li>
		<a href="{{URL::to('admin')}}">Home</a>
	</li>
	<li class="active">
		<strong>Change Tenant</strong>
	</li>
@endsection
@section('content-title')
	Change Tenant Form
@endsection
@section('content')

        <form action="{{URL::to('admin/gantitenant')}}" method="post" class="form-horizontal">
        <input type="hidden" name="u_id" value = "{{$data_session['user_id']}}">


        <div class = "form-group row">
        <div class="col-md-2">
        <label>
        Current Tenant
        </label>
        </div>

        <div class="col-md-4">
        <input type="text" id="tenant_default" name="tenant_default" class="form-control" value="{{$data_session['tenant_name']}}" disabled>
        </div>

        </div>

        <div class="form-group row">
        <div class="col-md-2">
        <label>
        Change Tenant
        </label>
        </div>

        <div class = "col-md-4">
        <select class="form-control selectpicker" name = "u_tenant_id">
        @foreach($tenant_list as $newrole)
        @if($newrole->tenant_name == Session::get("sessUser")["tenant_name"])
        <option value="{{$newrole->tenant_id}}" selected
        >{{$newrole->tenant_name}}</option>
        @else
        <option value="{{$newrole->tenant_id}}">{{$newrole->tenant_name}}</option>
        @endif
        @endforeach
        </select>
        </div>
        </div>

        <div class="form-group row">
        <div class="col-sm-4 col-sm-offset-2">
        <button class="btn btn-primary" type="submit" onclick="return confirm('Are you sure ?')">Save</button>
        </div>
        </div>

        {{csrf_field()}}
        </form>

@endsection

