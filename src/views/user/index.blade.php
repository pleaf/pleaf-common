{{--<!DOCTYPE html>--}}
{{--<html>--}}
{{--<head>--}}
    {{--<base href="http://demos.telerik.com/kendo-ui/grid/angular">--}}
    {{--<style>html { font-size: 12px; font-family: Arial, Helvetica, sans-serif; }</style>--}}
    {{--<title></title>--}}
    {{--<link href="http://cdn.kendostatic.com/2014.2.716/styles/kendo.common.min.css" rel="stylesheet" />--}}
    {{--<link href="http://cdn.kendostatic.com/2014.2.716/styles/kendo.default.min.css" rel="stylesheet" />--}}
    {{--<link href="http://cdn.kendostatic.com/2014.2.716/styles/kendo.dataviz.min.css" rel="stylesheet" />--}}
    {{--<link href="http://cdn.kendostatic.com/2014.2.716/styles/kendo.dataviz.default.min.css" rel="stylesheet" />--}}
    {{--<script src="http://cdn.kendostatic.com/2014.2.716/js/jquery.min.js"></script>--}}
    {{--<script src="http://cdn.kendostatic.com/2014.2.716/js/angular.min.js"></script>--}}
    {{--<script src="http://cdn.kendostatic.com/2014.2.716/js/kendo.all.min.js"></script>--}}
{{--</head>--}}
{{--<body>--}}
{{--<div id="example" ng-app="KendoDemos">--}}
    {{--<div ng-controller="MyCtrl">--}}

        {{--@{{ mainGridOptions }}--}}

        {{--<div kendo-grid k-options="mainGridOptions"></div>--}}
    {{--</div>--}}
{{--</div>--}}

{{--<script>--}}
    {{--angular.module("KendoDemos", [ "kendo.directives" ]);--}}
    {{--function MyCtrl($scope) {--}}

        {{--$scope.checkboxClicked = function(dataItem) {--}}
            {{--console.log(dataItem);--}}

        {{--};--}}
        {{--$scope.mainGridOptions = {--}}
            {{--dataSource: {--}}
                {{--type: "odata",--}}
                {{--transport: {--}}
                    {{--read: "http://demos.telerik.com/kendo-ui/service/Northwind.svc/Employees"--}}
                {{--},--}}
                {{--pageSize: 5,--}}
                {{--serverPaging: true,--}}
                {{--serverSorting: true--}}
            {{--},--}}
            {{--sortable: true,--}}
            {{--pageable: true,--}}
            {{--columns: [--}}
                {{--{ template: '<input type="checkbox" ng-click="checkboxClicked(dataItem)">' },--}}
                {{--{--}}
                    {{--field: "FirstName",--}}
                    {{--title: "First Name",--}}
                    {{--width: "120px"--}}
                {{--},{--}}
                    {{--field: "LastName",--}}
                    {{--title: "Last Name",--}}
                    {{--width: "120px"--}}
                {{--},{--}}
                    {{--field: "Country",--}}
                    {{--width: "120px"--}}
                {{--},{--}}
                    {{--field: "City",--}}
                    {{--width: "120px"--}}
                {{--},{--}}
                    {{--field: "Title"--}}
                {{--}]--}}
        {{--};--}}
    {{--}--}}
{{--</script>--}}


{{--</body>--}}
{{--</html>--}}


@extends('pleaf-common::layouts.master')
@section("head")
    <script src="/sts/pleaf-common/js/users.js"></script>

    @authorize('editUser')
        <style type="text/css">
            .task-editUser {
                visibility: inherit;
            }
        </style>
    @endauthorize
    @authorize('removeUser')
        <style type="text/css">
            .task-removeUser {
                visibility: inherit;
            }
        </style>
    @endauthorize
    @authorize('editOu')
        <style type="text/css">
            .task-editOu {
                visibility: inherit;
            }
        </style>
    @endauthorize

@endsection

@section('title')
        User
@endsection

@section('page-title')
        User
@endsection

@section('bread-crumb')
        <li class="active">
            <a href="{{URL::to('admin')}}">Home</a>

        </li>

        <li class="active">
            <strong>User</strong>

        </li>

@endsection

@section('content-title')
    User Table
@endsection

@section('content')

        @messages
@authorize('viewUser')
    <div ng-app="usersApp">
        <div ng-controller="usersCtrl">
            @authorize('addUser')
                <a href="{{URL::to('admin/user/create')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Add</a>&nbsp
            @endauthorize
            <a href="{{URL::to('admin/user')}}" class="btn btn-primary"><i class="fa fa-refresh"></i> Refresh</a>
            </br></br>

            <div id="grid"></div>

        </div>
    </div>
@endauthorize

<script id="popupTemplate" type="text/ng-template">
    <div class="k-edit-label">
        <label for="username">Username:</label>
    </div>
    <div class="k-edit-field">
        <input  data-bind="value:username" required />
        <span class="k-invalid-msg" data-for=""></span>
    </div>
    <div class="k-edit-label">
        <label for="email">Email:</label>
    </div>
    <div class="k-edit-field">
        <input  data-bind="value:email" required />
        <span class="k-invalid-msg" data-for=""></span>
    </div>
    <div class="k-edit-label">
        <label for="fullname">Fullname:</label>
    </div>
    <div class="k-edit-field">
        <input  data-bind="value:fullname" required />
        <span class="k-invalid-msg" data-for=""></span>
    </div>
     <div class="k-edit-label">
        <label for="fullname">Phone:</label>
    </div>

    <div class="k-edit-field">
        <input  data-bind="value:phone" />
    </div>

    <div class="k-edit-label">
        <label for="Role">Default Role:</label>
    </div>
    <div class="k-edit-field">
        <input id="Role" name="Role" data-bind="value:role_id" required />
        <span class="k-invalid-msg" data-for="role"></span>
    </div>
    <div class="k-edit-label">
        <label for="OU">Default OU:</label>
    </div>
    <div class="k-edit-field">
        <input id="OU" name="OU" data-bind="value:ou_id" required />
        <span class="k-invalid-msg" data-for="ou"></span>
    </div>
    <div class="k-edit-label">
        <label for="Policy">Default Policy:</label>
    </div>
    <div class="k-edit-field">
        <input id="Policy" name="Policy" data-bind="value:policy_id" required />
        <span class="k-invalid-msg" data-for="Policy"></span>
    </div>
</script>
@endsection
