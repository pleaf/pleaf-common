@extends('pleaf-common::layouts.master')
@section('title')
	Change Role
@endsection
@section('page-title')
	Change Role
@endsection
@section('bread-crumb')
	<li>
		<a href="{{URL::to('admin')}}">Home</a>
	</li>
	<li class="active">
		<strong>Change Role</strong>
	</li>
@endsection
@section('content-title')
	Change Role Form
@endsection

@section('content')
	<form action="{{URL::to('admin/gantirole')}}" method="post" class="form-horizontal">
		<input type="hidden" name="u_id" value = "{{$data_session['user_id']}}"></input>


		<div class = "form-group row">
		<div class="col-md-2">
			<label>
				Current Role
			</label>
		</div>

			<div class="col-md-4">
				<input type="text" id="role_default" name="role_default" class="form-control" value="{{$data_session['role_default_name']}}" disabled>
        	</div>

		</div>
		<div class="form-group row">
		<div class="col-md-2">
			<label>
				Change Role
			</label>
		</div>


			<div class = "col-md-4">
				<select class="form-control" name = "u_role">
					@foreach(Session::get(_PLEAF_SET_CURRENT_ROLE) as $key=>$value)
                        <option value="{{ $value["role_id"] }}">{{ $value["role_name"] }}</option>
                    @endforeach
				</select>
			</div>
		</div>
		<div class="form-group row">

        <div class="col-sm-4 col-sm-offset-2">
            <button class="btn btn-primary" type="submit" onclick="return confirm('Are you sure ?')">Save</button>
            </div>
        </div>

         {{csrf_field()}}
	</form>

    {{--<button type="button" id="open1">Open first Window</button>--}}

    {{--<div id="win1" style="display:none">--}}
        {{--<p>First Window</p>--}}
        <button type="button" id="open2">Open second Window</button>
    {{--</div>--}}

    <div id="win2" style="display:none">
        <label>Current Role:</label>
        <select name="u_role"
                style=" height: 30%; width: 70%;">
            @foreach(Session::get(_PLEAF_SET_CURRENT_ROLE) as $key=>$value)
                <option value="{{ $value["role_id"] }}">{{ $value["role_name"] }}</option>
            @endforeach
        </select>
    </div>

            <script>
                $("#open2").click(function() {
                    $("#win2").show().kendoWindow({
                        width: "300px",
                        height: "100px",
                        modal: true,
                        title: "Window 2"
                    }).data("kendoWindow").center();
                });

                $("#close2").click(function() {
                    $("#win2").data("kendoWindow").close();
                });
            </script>


@endsection
