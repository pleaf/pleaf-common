@extends('layouts.master')
@section('title')
        User
@endsection

@section('page-title')
        User
@endsection

@section('bread-crumb')
        <li class="active">
            <a href="{{URL::to('admin')}}">Home</a>

        </li>
        <li class="active">
            <a href="{{URL::to('admin/user')}}">User</a>

        </li>
        
        <li class="active">
            <strong>Edit User</strong>
        </li>

        
@endsection

@section('content-title')
    Edit User
@endsection 

@section('content')

    @messages

    <form id="editUser" action="{{URL::to('/admin/user/update')}}"  method="post" class="form-horizontal">
        </br>
        {{csrf_field()}}
        <input type="hidden" value="{{$user->user_id}}" name="user_id" id='user_id'>

                 <div class="form-group row">
                     <div class="col-md-2">
                    <label>
                        Username
                    </label>
                     </div>

                    <div class="col-md-4">
                        <input type="text" id="username" name="username" class="form-control" value= "{{$user->username}}">
                    </div>
                 </div>

                 <div class="form-group row">
                     <div class="col-md-2">
                    <label>
                        Email
                    </label>
                     </div>

                    <div class="col-md-4">
                        <input type="text" id="email" name="email" class="form-control" value= "{{$user->email}}">
                    </div>
                 </div>

                 <div class="form-group row">
                     <div class="col-md-2">
                    <label>
                        Fullname
                    </label>
                     </div>

                    <div class="col-md-4">
                        <input type="text" id="fullname" name="fullname" class="form-control" value= "{{$user->fullname}}">
                    </div>
                 </div>

                  <div class="form-group row">
                     <div class="col-md-2">
                    <label>
                        Phone
                    </label>
                     </div>

                    <div class="col-md-4">
                        <input type="text" id="phone" name="phone" class="form-control" value= "{{$user->phone}}">
                    </div>
                 </div>

                 <div class="form-group row">

                     <div class="col-md-2">
                    <label>
                        Role Default
                    </label>
                     </div>

                    <div class="col-md-4">
                        <select class="form-control m-b" id="role_default_id" name="role_default_id">
                            @foreach($role as $row)
                                @if($user->role_default_id == $row->role_id)
                                    <option value='{{$row->role_id}}'
                                            selected>{{$row->role_name}}</option>
                                @else
                                    <option value='{{$row->role_id}}'>{{$row->role_name}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                 </div>

         <div class="form-group row">

                     <div class="col-md-2">
                    <label>
                        Policy Default
                    </label>
                     </div>

                    <div class="col-md-4">
                        <select class="form-control m-b" id="policy_default_id" name="policy_default_id">
                            @foreach($policy as $row)
                                @if($user->policy_default_id == $row->policy_id)
                                    <option value='{{$row->policy_id}}'
                                            selected>{{$row->policy_name}}</option>
                                @else
                                    <option value='{{$row->policy_id}}'>
                                        {{$row->policy_name}}
                                    </option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                 </div>

         <div class="form-group row">

                     <div class="col-md-2">
                    <label>
                        OU Default
                    </label>
                     </div>

                    <div class="col-md-4">
                        <select class="form-control m-b" id="ou_default_id" name="ou_default_id">
                            @foreach($ou as $row)
                                @if($user->ou_default_id == $row->ou_id)
                                    <option value='{{$row->ou_id}}'
                                            selected>{{$row->ou_name}}</option>
                                @else
                                    <option value='{{$row->ou_id}}'>{{$row->ou_name}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                 </div>
            </form>

            <div class="form-group row">
                <div class="col-md-2">
                    <label>
                    </label>
                </div>
                <div class="col-md-4">
                    <button class="btn btn-white" onclick="window.location.href='/admin/user'">
                        Cancel
                    </button>
                    <button class="btn btn-primary" form="editUser" type="submit" onclick="return
                                    confirm
                                    ('Are you sure ?')">Save</button>
                </div>
            </div>

@endsection