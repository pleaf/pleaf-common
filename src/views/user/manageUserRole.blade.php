@extends('pleaf-common::layouts.master')
@section("head")
    <script src="/sts/pleaf-common/js/users.js"></script>
    <script src="/sts/pleaf-common/js/manageUserRole.js"></script>
@endsection

@section('title')
    User
@endsection

@section('page-title')
    User
@endsection

@section('bread-crumb')
    <li class="active">
        <a href="{{URL::to('admin')}}">Home</a>

    </li>
    <li class="active">
        <a href="{{URL::to('admin/user')}}">User</a>
    </li>

    <li class="active">
        <strong>Manage User Role</strong>
    </li>


@endsection

@section('content-title')
    Manage User Role
@endsection


@section('content')

@section('ibox-title')

@endsection

@messages

    <form class="m-t" id="manageUser" role="form" method="post"
          action="{{ "/admin/user/manage-user" }} ">
        {{csrf_field()}}
        <input type="hidden" value="{{ $user->user_id }}" name="user_id">
        <div class="form-group row">
            <div class="col-md-2">
                <label>
                    Current Name
                </label>
            </div>
            <div class="col-md-4">
                <input type="text" disabled class="form-control" value="{{ $user->username }}"
                       placeholder="Role Name">
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-2">
                <label>
                    Role
                </label>
            </div>

            <div class="col-md-4">
                <select class="form-control selectpicker" name="cmb_role">
                    @foreach($role_list AS $key=>$value)
                        <option value="{{ $value->role_id }}">{{ $value->role_name }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-2">
                <label>
                    Policy
                </label>
            </div>

            <div class="col-md-4">
                <select class="form-control selectpicker" name="cmb_policy">
                    @foreach($policy_list AS $key=>$value)
                        <option value="{{ $value->policy_id }}">{{ $value->policy_name }}</option>
                    @endforeach
                </select>
            </div>
        </div>

    </form>

    <div class="form-group row">
        <div class="col-md-2">
            <label>

            </label>
        </div>
        <div class="col-md-4">
            <button class="btn btn-white"
                    onclick="window.location.href='/admin/user'">
                Cancel
            </button>
            <button form="manageUser" class="btn btn-primary"
                    type="submit">Save</button>
        </div>
    </div>

    <div ng-app="manageUserRoleApp">
    		<div ng-controller="manageUserRoleCtrl">

                <input id="user_id" type="hidden" name="user_id" value="{{ $user->user_id }}" />

         	<div id="grid"></div>
    					
			</div>
	</div>
    		

@endsection
