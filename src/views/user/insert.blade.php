@extends('pleaf-common::layouts.master')
@section('title')
		User
@endsection

@section('page-title')
        User
@endsection

@section('bread-crumb')
        <li class="active">
            <a href="{{URL::to('admin')}}">Home</a>

        </li>
        <li class="active">
            <a href="{{URL::to('admin/user')}}">User</a>
        </li>
        
        <li class="active">
            <strong>Add User</strong>
        </li>

        
@endsection

@section('content-title')
    Add New User 
@endsection 


@section('content')
@section('ibox-title')
	Add New User
@endsection

@messages("onField")
    <div class="form-group row">
        <div class="col-md-2">

        </div>
        <div class="col-md-4">
            <div id="tenant_id"></div>
        </div>
    </div>

    <form action="{{URL::to('admin/user/store')}}" method="post" class="form-horizontal"></br>
        {{csrf_field()}}
        <div class="form-group row">
                     <div class="col-md-2">
                    <label for="username">
                        Username
                    </label>
                     </div>

                    <div class="col-md-4">
                        <input type="text" id="username" name="username" class="form-control" placeholder= "Username">
                    </div>
                 </div>

        <div class="form-group row">
                     <div class="col-md-2">
                    <label for="email">
                        Email
                    </label>
                     </div>

                    <div class="col-md-4">
                        <input type="text" id="email" name="email" class="form-control" placeholder= "someone@mydomain.com">
                    </div>
                 </div>

        <div class="form-group row">
                     <div class="col-md-2">
                    <label for="fullname">
                        Fullname
                    </label>
                     </div>

                    <div class="col-md-4">
                        <input type="text" id="fullname" name="fullname" class="form-control" placeholder= "Full Name">
                    </div>
                 </div>

        <div class="form-group row">
                     <div class="col-md-2">
                    <label>
                        Password
                    </label>
                     </div>

                    <div class="col-md-4">
                        <input type="password" id="password" name="password" class="form-control" placeholder= "Password">
                    </div>
                 </div>

        <div class="form-group row">
                     <div class="col-md-2">
                    <label>
                        Phone
                    </label>
                     </div>

                    <div class="col-md-4">
                        <input type="text" id="phone" name="phone" class="form-control" placeholder= "+62xxxxxxxxx">
                    </div>
                 </div>

            <div class="form-group row">

                <div class="col-md-2">
                    <label>
                        OU Default
                    </label>
                </div>

                <div class="col-md-4">
                    <select class="form-control m-b" id="ou_default_id" name="ou_default_id">
                        @foreach($ou as $row)
                            <option value='{{$row->ou_id}}'>{{$row->ou_name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group row">
                         <div class="col-md-2">
                        <label>
                            Role Default
                        </label>
                         </div>

                        <div class="col-md-4">
                            <select class="form-control m-b" id="role_default_id" name="role_default_id">
                                @foreach($role as $row)
                                <option value='{{$row->role_id}}'>{{$row->role_name}}</option>
                                @endforeach
                            </select>
                        </div>
                     </div>

             <div class="form-group row">
                         <div class="col-md-2">
                        <label>
                            Policy Default
                        </label>
                         </div>

                        <div class="col-md-4">
                            <select class="form-control m-b" id="policy_default_id" name="policy_default_id">
                                @foreach($policy as $row)
                                <option value='{{$row->policy_id}}'>{{$row->policy_name}}</option>
                                @endforeach
                            </select>
                        </div>
                     </div>

        <div class="form-group">
            <div class="col-sm-4 col-sm-offset-2">
                    <a href="{{ "/admin/user" }}" class="btn btn-white">Cancel</a>
                    <button class="btn btn-primary" type="submit" onclick="return confirm('Are you sure ?')">Save</button>
            </div>
        </div>
        </form>


@endsection