<?php
function liActive($str){
    $url = $_SERVER['REQUEST_URI'];
    if($url == $str)
    {
        echo 'class = "active"';
    }

}

function hitung()
{
    $url = $_SERVER['REQUEST_URI'];
    $str = explode("/",$url);
    $ndata = count($str);
    if($ndata>2)
    {
        if($str[2]=="tenant"||$str[2]=="ou"
                ||$str[2]=="outype"||$str[2]=="user"
                ||$str[2]=="policy"||$str[2]=="role"
                ||$str[2]=="comboconstant"||$str[2]=="systemconfig"
                ||$str[2]=="groupuser"||$str[2]=="dbquery")
            echo 'class="active"';
    }
    echo $ndata;

}

?>

<nav class="navbar navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li <?php liActive("/admin") ?>>
                <a href="/admin"><i class="fa fa-home"></i>
                    <span class="nav-label">Home</span>
                </a>
            </li>
            <li <?php hitung() ?>>
                <a href="#">
                    <i class="fa fa-terminal"></i> <span class="nav-label">Common</span>
                    <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse" aria-expanded="false">

                    <li <?php liActive("/admin/tenant") ?>>
                        <a href="/admin/tenant"> <i class="fa fa-suitcase"></i> <span class="nav-label">Tenant</span></a>
                    </li>
                    <li <?php liActive("/admin/outype") ?>>
                        <a href="/admin/outype"> <i class="fa fa-file-o"></i> <span class="nav-label">OU Type</span></a>
                    </li>
                    <li <?php liActive("/admin/ou") ?>>
                        <a href="/admin/ou"> <i class="fa fa-sitemap"></i> <span class="nav-label">OU</span></a>
                    </li>
                    <li <?php liActive("/admin/user") ?>>
                        <a href="/admin/user"> <i class="fa fa-users"></i> <span class="nav-label">User</span></a>
                    </li>
                    <li <?php liActive("/admin/policy") ?>>
                        <a href="/admin/policy"><i class="fa fa-file-text-o"></i> <span class="nav-label">Policy</span></a>
                    </li>
                    <li <?php liActive("/admin/role") ?>>
                        <a href="/admin/role"> <i class="fa fa-file"></i> <span class="nav-label">Role</span></a>
                    </li>
                    <!-- <li <?php liActive("/admin/role/role-task/".Session::get('sessUser')['user_id']) ?>>
                        <a href="/admin/role/role-task/{{Session::get('sessUser')['user_id']}}"> <span class="nav-label">Role Task</span></span></a>
                    </li> -->
                    <li <?php liActive("/admin/comboconstant") ?>>
                        <a href="/admin/comboconstant"><i class="fa fa-file"></i> <span class="nav-label">Combo Const</span></a>
                    </li>
                    <li <?php liActive("/admin/systemconfig") ?>>
                        <a href="/admin/systemconfig"> <i class="fa fa-cogs"></i>
                            <span class="nav-label">System Config</span></a>
                    </li>
                    <li <?php liActive("/admin/groupuser") ?>>
                        <a href="/admin/groupuser"> <i class="fa fa-users"></i> <span class="nav-label">Group User</span></a>
                    </li>
                    <li <?php liActive("/admin/dbquery") ?>>
                        <a href="/admin/dbquery"> <i class="fa fa-database"></i> <span class="nav-label">DB Query</span></a>
                    </li>
                </ul>

            </li>
        </ul>
    </div>
</nav>

<div id="win2" style="display:none">
    <form action="{{URL::to('admin/gantirole')}}" method="post" id="change">
        <center><label>Current Role:</label><br></center>
        <center>
            <select name="u_role"
                    style=" height: 30%; width: 70%;">
                @foreach(Session::get(_PLEAF_SET_CURRENT_ROLE) as $key=>$value)
                    @if(Session::get(_PLEAF_SESS_USERS)["role_default_id"] == $value["role_id"])
                        <option value="{{ $value["role_id"] }}"
                                selected><center>{{ $value["role_name"]}}</center></option>
                    @else
                        <option value="{{ $value["role_id"] }}"><center>{{ $value["role_name"] }}</center></option>
                    @endif
                @endforeach
            </select>

            {{--<div >--}}
            <button form="change" type="submit">Change Role</button>
            {{--</div>--}}
        </center>
    </form>

</div>

{{--<div id="win2" style="display:none">--}}
{{--<form action="{{URL::to('admin/gantirole')}}" method="post" id="change">--}}
{{--<center><label>Current Role:</label><br></center>--}}
{{--<center>--}}
{{--<select name="u_role"--}}
{{--style=" height: 30%; width: 70%;">--}}
{{--@foreach(Session::get(_PLEAF_SET_CURRENT_ROLE) as $key=>$value)--}}
{{--@if(Session::get(_PLEAF_SESS_USERS)["role_default_id"] == $value["role_id"])--}}
{{--<option value="{{ $value["role_id"] }}"--}}
{{--selected><center>{{ $value["role_name"]}}</center></option>--}}
{{--@else--}}
{{--<option value="{{ $value["role_id"] }}"><center>{{ $value["role_name"] }}</center></option>--}}
{{--@endif--}}
{{--@endforeach--}}
{{--</select>--}}

{{--<div >--}}
{{--<button form="change" type="submit">Change Role</button>--}}
{{--</div>--}}
{{--</center>--}}
{{--</form>--}}

{{--</div>--}}

<script>
    $("#role").click(function() {
        $("#win2").show().kendoWindow({
            width: "400px",
            height: "100px",
            modal: true,
            title: "Change Role"
        }).data("kendoWindow").center();
    });

    $("#roleClose").click(function() {
        $("#win2").data("kendoWindow").close();
    });
</script>
