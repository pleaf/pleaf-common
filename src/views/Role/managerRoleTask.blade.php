@extends('pleaf-common::layouts.master')
@section('head')
    <script src="/sts/pleaf-common/js/roletask.js"></script>
@endsection
@section('end-body')

@section('title')
    Manager Role Task
@endsection

@section('page-title')
   Role
@endsection

@section('bread-crumb')
    <li class="active">
        <a href="{{URL::to('admin')}}">Home</a>
    </li>
    <li class="active">
        <a href="{{URL::to('admin/role')}}">Role</a>
    </li>
    <li class="active">
        <strong>Manage Role Task</strong>
    </li>

@endsection

@section('content')
    <div class="ibox-content" ng-app="roletaskApp" ng-controller="roletaskCtrl">
        <input id="role_id" type="hidden" name="role_id" value="{{$data->role_id}}" />
        <div class="form-group row">
            <div class="col-md-2">
                <label>
                    Current Role Name
                </label>
            </div>
            <div class="col-md-4">
                <input type="text" class="form-control" value="{{$data->role_name}}" readonly/>
            </div>
        </div>
        <div style="height: 400px; overflow: auto">
            <div style="height:1500px">
                    <div id="treeview"></div>
             </div>
        </div>
    </div>
@endsection
