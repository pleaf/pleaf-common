@extends('pleaf-common::layouts.master')
@section('head')
    <script src="/sts/pleaf-common/js/role.js"></script>
    @authorize('editRole')
        <style type="text/css">
            .task-editRole {
                visibility: inherit;
            }
        </style>
    @endauthorize
    @authorize('removeRole')
        <style type="text/css">
            .task-removeRole {
                visibility: inherit;
            }
        </style>
    @endauthorize
    {{--@authorize('removeRole')--}}
        {{--<style type="text/css">--}}
            {{--.task-removeRole {--}}
                {{--visibility: inherit;--}}
            {{--}--}}
        {{--</style>--}}
    {{--@endauthorize--}}

@endsection
@section('end-body')

@section('title')
    Role
@endsection

@section('page-title')
    Role
@endsection

@section('bread-crumb')
    <li class="active">
        <a href="{{URL::to('admin')}}">Home</a>
    </li>
    <li class="active">
        <strong>Role</strong>
    </li>

@endsection

@section('content')

@messages
@authorize('viewRole')
    <div ng-app="roleApp">
        <div ng-controller="roleCtrl">
            @authorize('addRole')
                <a class="btn btn-primary" href="{{URL::to('admin/role/create')}}"><i class="fa fa-plus"></i> Add</a>&nbsp
            @endauthorize
            <a class="btn btn-primary" href="{{URL::to('admin/role')}}"><i class="fa fa-refresh"></i> Refresh</a>
            <br/><br/>

                <div id="grid"></div>
        </div>
    </div>
@endauthorize
@endsection
