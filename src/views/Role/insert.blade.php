@extends('pleaf-common::layouts.master')
@section('title')
       Add Role
@endsection
@section('page-title')
    Role
@endsection
@section('bread-crumb')
    <li>
        <a href="{{URL::to('admin')}}">Home</a>
    </li>
    <li class="active">
        <a href="{{URL::to('admin/role')}}">Role</a>
    </li>
    <li class="active">
        <strong>Add Role</strong>
    </li>
@endsection
@section('content-title')
    Add New Role Form
@endsection
@section('content')
@section('ibox-title')
	Add New Role
@endsection

@messages

    <div class="form-group row">
        <div class="col-md-2">

        </div>
        <div class="col-md-4">
            <div id="tenant_id"></div>
        </div>
    </div>
    <form class="m-t" id="formAdd" role="form" method="post" action="{{ "/admin/role/store" }} ">
        {{csrf_field()}}
        <div class="form-group row">
            <div class="col-md-2">
                 <label for="role_name">
                    Role Name
                 </label>
             </div>
             <div class="col-md-4">
                    <input type="text" class="form-control"
                           placeholder="Role Name"
                           name="role_name"
                           id ="role_name">
             </div>
         </div>
         
         <div class="form-group row">
            <div class="col-md-2">
                 <label>
                    Role Type
                 </label>
             </div>

         <div class="col-md-4">
                <select class="form-control selectpicker" id="role_type" name="role_type">
                    @if($type->role_name == "SA")
                        <option value="ADMIN" selected>Administrator</option>
                        <option value="USER">User</option>
                    @elseif($type->role_type == "ADMIN")
                        <option value="USER">User</option>
                    @elseif($type->role_type == "USER")
                        <option value="USER">User</option>
                    @endif
                </select>
         </div>
     </div>

    </form>

    <div class="form-group row">
        <div class="col-md-2">
            <label>

            </label>
        </div>
        <div class="col-md-4">
            <button class="btn btn-white" onclick="window.location.href='/admin/role'">Cancel</button>
            <button form="formAdd" class="btn btn-primary" form="formEdit"
                    type="submit">Save</button>
        </div>
    </div>


@endsection