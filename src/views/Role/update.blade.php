@extends('pleaf-common::layouts.master')
@section('title')
        Edit Role
@endsection
@section('page-title')
    Role
@endsection
@section('bread-crumb')
    <li>
        <a href="{{URL::to('admin')}}">Home</a>
    </li>
    <li>
        <a href="{{URL::to('admin/role')}}">Role</a>
    </li>
    <li class="active">
        <strong>Edit Role</strong>
    </li>
@endsection
@section('content-title')
    Edit Role Form
@endsection
@section('content')
@section('ibox-title')
	Edit Role
@endsection

@messages("onField")

    <form id="formEdit" class="m-t" role="form"
          method="post"
          action="{{ "/admin/role/update" }}">
        {{csrf_field()}}
        <input type="hidden" name="role_id" value="{{$role->role_id}}">
        <div class="form-group row">
            <div class="col-md-2">
                 <label>
                    Role Name
                 </label>
             </div>
             <div class="col-md-4">
                    <input type="text" class="form-control" placeholder="Role Name"
                           id="role_name" name="role_name" value="{{$role->role_name}}">
             </div>
         </div>
         <div class="form-group row">
            <div class="col-md-2">
                 <label>
                    Role Type
                 </label>
             </div>

             <div class="col-md-4">
                    <select class="form-control m-b selectpicker" name="cmbRole">
                        @foreach($role_type as $value)
                            @if($value->role_type == $role->role_type)
                                <option value="{{ $value->role_type }}" selected>
                                    {{ $value->role_type }}
                                </option>
                            @else
                                <option value="{{ $value->role_type }}">
                                    {{ $value->role_type }}
                                </option>
                            @endif
                        @endforeach
                    </select>
             </div>
         </div>
    </form>

    <div class="form-group row">
        <div class="col-md-2">
            <label>

            </label>
        </div>
        <div class="col-md-4">
            <button class="btn btn-white" onclick="window.location.href='/admin/role'">Cancel</button>
            <button form="formEdit" class="btn btn-primary" form="formEdit"
                    type="submit">Save</button>
        </div>
    </div>

@endsection