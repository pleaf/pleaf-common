
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Ganti Outlet</title>

    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="/css/animate.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <style>
        html, body { height:100%; font-size: 15px; }

        .outer-wrapper {
            display: table;
            width: 100%;
            height: 100%;
        }

        .inner-wrapper {
            display:table-cell;
            vertical-align:middle;
            padding:15px;
        }
        .login-btn { position:fixed; top:15px; right:15px; }

        .center {
            display: block;
            margin-left: auto;
            margin-right: auto;
            width: auto;
        }

    </style>

</head>

<body class="top-navigation">

<div id="wrapper">
    <div id="page-wrapper" class="gray-bg">
        {{--<div class="row border-bottom white-bg">--}}
            {{--<nav class="navbar navbar-expand-lg navbar-static-top" role="navigation">--}}
                {{--<!--<div class="navbar-header">-->--}}
                {{--<!--<button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">-->--}}
                {{--<!--<i class="fa fa-reorder"></i>-->--}}
                {{--<!--</button>-->--}}

                {{--<a href="javascript:;" class="navbar-brand">ADMIN</a>--}}
                {{--<!--</div>-->--}}
                {{--<div class="navbar-collapse collapse" id="navbar">--}}
                    {{--<ul class="nav navbar-nav mr-auto">--}}
                        {{--<li class="active">--}}
                            {{--<a aria-expanded="false" role="button" href="/dashboard"> Kembali</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--</nav>--}}
        {{--</div>--}}

        <div class="wrapper wrapper-content">

            <div class="row border-bottom white-bg dashboard-header">

                <section id="loginform" class="outer-wrapper">

                    <div class="inner-wrapper">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-4 col-sm-offset-4">
                                    <img src="/img/logo.png" class="center">
                                    {{--<h2 class="text-center">Welcome back.</h2>--}}
                                    <form class="m-t" id="manageUser" role="form" method="post" action="{{ config("pleaf_config.app_link_change_tenant") }}">
                                        {{ csrf_field() }}
                                        <input type="hidden" value="{{ $user->user_id }}" id="user_id">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">User</label>
                                            <input type="text" readonly class="form-control" value="{{ $user->username }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Nama</label>
                                            <input type="text" readonly class="form-control" value="{{ $user->fullname }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Outlet</label>
                                            <select class="form-control m-b" id="" name='tenant_id'>
                                                @foreach($policy_tenant_list as $value)
                                                    <option value='{{$value->tenant_id}}'>
                                                        {{ $value->tenant_code }} - {{ $value->tenant_name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div align="center">
                                            <button class="btn btn-primary" type="submit">
                                                OK
                                            </button>
                                        </div><br>
                                        <a href="{{ URL::asset('userlogouts') }}"><i class="fa fa-arrow-left"></i> Kembali ke Login</a>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>

            </div>
        </div>

    </div>
</div>

</body>
</html>