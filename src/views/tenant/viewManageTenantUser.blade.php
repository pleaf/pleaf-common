
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Manage User Outlet</title>

    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="/css/animate.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <style>
        .table-fixed thead {
            width: 97%;
        }
        .table-fixed tbody {
            height: 320px;
            overflow-y: auto;
            width: 100%;
        }
        .table-fixed thead, .table-fixed tbody, .table-fixed tr, .table-fixed td, .table-fixed th {
            display: block;
        }
        .table-fixed tbody td, .table-fixed thead > tr> th {
            float: left;
            border-bottom-width: 0;
        }
        body {

        }
        h1 {
            font-size: 2em;
            margin-bottom: 0.2em;
            padding-bottom: 0;
        }
        p {
            font-size: 1.5em;
            margin-top: 0;
            padding-top: 0;
        }
    </style>

    @include("pleaf-core::includes/include-kendo")

    <script src="/js/checklist-model.js"></script>

    <script src="/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="/sts/pleaf-common/js/manageUserTenant.js"></script>

</head>

<body class="top-navigation" ng-app="manageUserTenant"
      ng-controller="ManageUserTenantController as manageUserTenantController">

<div id="wrapper">
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom white-bg">
            <nav class="navbar navbar-expand-lg navbar-static-top" role="navigation">
                <!--<div class="navbar-header">-->
                <!--<button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">-->
                <!--<i class="fa fa-reorder"></i>-->
                <!--</button>-->

                <a href="javascript:;" class="navbar-brand">Manage User Outlet</a>
                <!--</div>-->
                <div class="navbar-collapse collapse" id="navbar">
                    <ul class="nav navbar-nav mr-auto">
                        <li class="active">
                            <a aria-expanded="false" role="button" href="{{ config("pleaf_config.app_redirect_back_manage_tenant") }}"> Kembali</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>

        <div class="wrapper wrapper-content">
            <div class="row border-bottom white-bg dashboard-header">


                <form class="m-t" id="manageUser" role="form" method="post">
                    <input type="hidden" value="{{ $user->user_id }}" id="user_id">
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label>
                                User
                            </label>
                        </div>

                        <div class="col-md-4">
                            <input type="text" readonly class="form-control" value="{{ $user->username }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-2">
                            <label>
                                Nama
                            </label>
                        </div>

                        <div class="col-md-4">
                            <input type="text" readonly class="form-control" value="{{ $user->fullname }}">
                        </div>
                    </div>

                </form>

                <div class="col-md-6">
                    <div align="right">
                        <button class="btn btn-primary"
                                ng-click="doAddManageTenantUser()">
                            Tambah
                        </button>
                    </div>

                    <br>
                    <div class="panel panel-default">
                        <table class="table">
                            <thead>
                            <tr>
                                <th class="col-xs-1">
                                    <input id="checkAll" type="checkbox"
                                           ng-model="checked"
                                           ng-click="checkAll(checked)">
                                </th>
                                <th class="col-xs-11">Nama</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr ng-repeat="item in manage_user_tenant_list">
                                <td class="col-xs-1">
                                    <input id="@{{ item.tenant_id }}" type="checkbox"
                                           checklist-model="manageUserTenantController.tenantSelected"
                                           checklist-value="item">
                                </td>
                                <td class="col-xs-11">@{{ item.tenant_code.concat(' - ').concat(item.tenant_name) }}</td>
                            </tr>
                            <tr ng-if="manage_user_tenant_list.length  == 0">
                                <td colspan="2">
                                    Tidak ada data
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="col-md-6">
                    <br><br><br>
                    <div class="panel panel-default">
                        <table class="table">
                            <thead>
                            <tr>
                                <th class="col-xs-2">#</th>
                                <th class="col-xs-10">Nama</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr ng-repeat="item in policy_tenant_list">
                                <td class="col-xs-2">
                                    <a href="javascript:;" ng-click="doClearManageTenantUser(item)">Hapus</a>
                                </td>
                                <td class="col-xs-10">@{{ item.tenant_code.concat(' - ').concat(item.tenant_name) }}</td>
                            </tr>
                            <tr ng-if="policy_tenant_list.length  == 0">
                                <td colspan="2">
                                    Tidak ada data
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

</body>
</html>
