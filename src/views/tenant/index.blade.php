@extends('pleaf-common::layouts.master')
@section('head')
    <script src="/sts/pleaf-common/js/tenant.js"></script>

    @authorize('editTenant')
    <style type="text/css">
        .task-editTenant {
            visibility: inherit;
        }
    </style>
    @endauthorize

@endsection
@section('end-body')

@section('title')
        Tenant
@endsection

@section('page-title')
        Tenant
@endsection

@section('bread-crumb')
        <li class="active">
            <a href="{{URL::to('admin')}}">Home</a>

        </li>

        <li class="active">
            <strong>Tenant</strong>

        </li>
@endsection

@section('content')
@messages

    @authorize('viewTenant')
        <div ng-app="tenantApp">
            <div ng-controller="tenantCtrl">
                @authorize('addTenant')
                    <a class="btn btn-primary" href="{{URL::to('admin/tenant/create')}}"><i class="fa fa-plus"></i> Add</a>&nbsp
                @endauthorize
                <a class="btn btn-primary" href="{{URL::to('admin/tenant/refresh')}}"><i class="fa fa-refresh"></i> Refresh</a>

                <br/><br/>
                    <div id="grid"></div>
            </div>
        </div>
    @endauthorize

@endsection
