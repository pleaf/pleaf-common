@extends('pleaf-common::layouts.master')
@section('title')
        Tenant
@endsection

@section('page-title')
        Tenant
@endsection

@section('ibox-title')
        Add New Tenant
@endsection

@section('bread-crumb')
        <li class="active">
            <a href="{{URL::to('admin')}}">Home</a>

        </li>
        <li class="active">
            <a href="{{URL::to('admin/tenant')}}">Tenant</a>

        </li>

        <li class="active">
            <strong>Add Tenant</strong>
        </li>


@endsection

@section('content-title')
    Add New Tenant Form
@endsection
@section('content')
    @messages("onField")
    </br>
    <div class="form-group row">
        <div class="col-md-2">

        </div>
        <div class="col-md-4">
            <div id="userTenantId"></div>
        </div>
    </div>
        <form action="{{URL::to('admin/tenant/store')}}" method="post" class="form-horizontal">

                 <div class="form-group row">
                     <div class="col-md-2">
                    <label for="tenant_code">
                        Tenant Code
                    </label>
                     </div>

                    <div class="col-md-5">
                        <input type="text" id="tenant_code" name="tenant_code" class="form-control" placeholder= "Tenant Code">
                    </div>
                 </div>

                 <div class="form-group row">
                     <div class="col-md-2">
                    <label for="tenant_name">
                        Tenant Name
                    </label>
                     </div>

                    <div class="col-md-5">
                        <input type="text" id="tenant_name" name="tenant_name" class="form-control" placeholder= "Tenant Name">
                    </div>
                 </div>

                 <div class="form-group row">
                     <div class="col-md-2">
                    <label for="description">
                        Tenant Description
                    </label>
                     </div>

                    <div class="col-md-5">
                        <input type="text" id="description" name="description" class="form-control" placeholder= "Description">
                    </div>
                 </div>

                 <div class="form-group row">
                     <div class="col-md-2">
                    <label for="email">
                        Email
                    </label>
                     </div>

                    <div class="col-md-5">
                        <input type="text" id="email" name="email" class="form-control" placeholder= "e.g : someone@mydomain.com">
                    </div>
                 </div>

                 <div class="form-group row">
                     <div class="col-md-2">
                    <label for="host">
                        Host
                    </label>
                     </div>

                    <div class="col-md-5">
                        <input type="text" id="host" name="host" class="form-control" placeholder= "e.g : http://www.mydomain.com">
                    </div>
                 </div>

                 <div class="form-group row">
                     <div class="col-md-2">
                    <label for="host">
                    </label>
                     </div>

                    <div class="col-md-5">
                        <input type="checkbox" name="check" title="Check for create initial tenant">
                    </div>
                 </div>

                <div class="form-group">
                    <div class="col-sm-5 col-sm-offset-2">
                        <button class="btn btn-white" type="button" onclick="window.location.href='/admin/tenant/'">Cancel</button>
                        <button class="btn btn-primary" type="submit" >Save</button>
                    </div>
                </div>
             {{csrf_field()}}
        </form>
@endsection
