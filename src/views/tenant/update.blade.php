@extends('pleaf-common::layouts.master')
@section('title')
        Tenant
@endsection

@section('page-title')
        Tenant
@endsection

@section('bread-crumb')
        <li class="active">
            <a href="{{URL::to('admin')}}">Home</a>

        </li>
        <li class="active">
            <a href="{{URL::to('admin/tenant')}}">Tenant</a>

        </li>

        <li class="active">
            <strong>Edit Tenant</strong>
        </li>


@endsection

@section('content-title')
    Edit Tenant Form
@endsection

@section('ibox-title')
    Edit Tenant
@endsection

@section('content')

    @messages("onField")
    <form action="{{URL::to('admin/tenant/update')}}"  method="post" class="form-horizontal">
            <input type="hidden" value="{{$data_tenant->tenant_id}}" name="tenant_id" id='tenant_id'>

             <input type="hidden" name="version" id="version" value="{{$data_tenant->version}}">

                <div class="form-group row">
                    <div class="col-md-2">
                        <label for="tenant_code">
                            Tenant Code
                        </label>
                    </div>

                    <div class="col-md-4">
                        <input type="text" id="tenant_code" name="tenant_code" class="form-control" value= "{{$data_tenant->tenant_code}}" disabled>
                    </div>

                </div>

                <div class="form-group row">

                    <div class="col-md-2">
                    <label for="tenant_name">
                        Tenant Name
                    </label>
                     </div>

                    <div class="col-md-4">
                        <input type="text" id="tenant_name" name="tenant_name" class="form-control" value= "{{$data_tenant->tenant_name}}">
                    </div>
                 </div>

                 <div class="form-group row">
                     <div class="col-md-2">
                    <label for="description">
                        Description
                    </label>
                     </div>

                    <div class="col-md-4">
                        <input type="text" id="description" name="description" class="form-control" value= "{{$data_tenant->description}}">
                    </div>
                 </div>

                 <div class="form-group row">
                     <div class="col-md-2">
                    <label for="email">
                        Email
                    </label>
                     </div>

                    <div class="col-md-4">
                        <input type="text" id="email" name="email" class="form-control" value= "{{$data_tenant->email}}">
                    </div>
                 </div>

                 <div class="form-group row">
                     <div class="col-md-2">
                    <label for="host">
                        Host
                    </label>
                     </div>

                    <div class="col-md-4">
                        <input type="text" id="host" name="host" class="form-control" value= "{{$data_tenant->host}}">
                    </div>
                 </div>


                     <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                        <button class="btn btn-white" type="button" onclick="window.location.href='/admin/tenant/'">Cancel</button>
                         <button class="btn btn-primary" type="submit">Save</button>
                         </div>
                    </div>
                {{csrf_field()}}
        </form>



@endsection
