<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PIS</title>
    <link rel="stylesheet" type="text/css" href="http://w2ui.com/src/w2ui-1.4.2.min.css" />
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
    <script type="text/javascript" src="http://w2ui.com/src/w2ui-1.4.2.min.js"></script>
    <script src="/js/bootstrap.js"></script>
</head>
<body class="top-navigation">

<style>
.tab {
    width: 100%;
    height: 768px;
    border: 1px solid silver;
    border-top: 0px;
    display: none;
    padding: 10px;
    overflow: auto;
}

.myFrame {
    width: 100%;
    height: 100%;
    border: none;
}
</style>
<div id="wrapper">
    <div id="page-wrapper" class="gray-bg">
        @include("pis-master::includes/navigation")
        <div class="row">
            <div class="col-lg-12">
                <div class="wrapper wrapper-content animated fadeInUp">
                <div class="ibox">
                    <div class = "ibox-content">
                        <div id="tab-example">
                            <div id="tabs" style="width: 100%; height: 29px;"></div>
                            <div id="home" class="tab">
                                Home Page.
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                    
                </div>
            </div>
            <div class="footer">
                <div>
                    <strong>Copyright</strong> SOLUSI TEKNOLOGI SEJATI &copy; 2016
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
var config = {
    tabs: {
        name: 'tabs',
        active: 'home',
        tabs: [
            { id: 'home', caption: 'Home' },
        ],
        onClick: function (event) {
            $('#tab-example .tab').hide();
            $('#tab-example #' + event.target).show();
        },
        onClose: function(event) {
            $('#tab-example #' +event.target).remove();
            this.click('home');
        }
    }
}

$(function () {
    $('#tabs').w2tabs(config.tabs);
    $('#home').show();
});


function addTab(tabId,tabName) {
    w2ui.tabs.add({ id: tabId, caption: tabName, closable: true });
    var output = document.getElementById('tab-example');
    if(!document.getElementById(tabId))
    {
        var ele=document.createElement("div");
        ele.setAttribute("id",tabId);
        ele.setAttribute("class","tab");
        var frame = document.createElement("IFRAME");
        frame.setAttribute("id","myFrame")
        frame.setAttribute("src",tabId);
        frame.setAttribute("class","myFrame");
        var view = document.createElement
        ele.appendChild(frame);
        output.appendChild(ele);
    }
    else
    {

        var iframe = document.getElementById('myFrame');
        iframe.src = iframe.src;
    }
    w2ui.tabs.click(tabId);
    
    
}

function addTabSrc(tabId,tabName,tabSrc) {
    w2ui.tabs.add({ id: tabId, caption: tabName, closable: true });
    var output = document.getElementById('tab-example');
    if(!document.getElementById(tabId))
    {
        var ele=document.createElement("div");
        ele.setAttribute("id",tabId);
        ele.setAttribute("class","tab");
        var frame = document.createElement("IFRAME");
        frame.setAttribute("src",tabSrc);
        frame.setAttribute("class","myFrame");
        var view = document.createElement
        ele.appendChild(frame);
        output.appendChild(ele);
    }
    w2ui.tabs.click(tabId);
}

</script>

</body>
</html>