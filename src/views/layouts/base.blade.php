<!DOCTYPE html>
<html>
<head>
    <title>@yield('title')</title>
    <link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">

    <!-- Adding Kendo Stuff -->
    @include("pleaf-core::includes/include-kendo")

    <link href="{{URL::asset('css/default.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/animate.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/style.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/override.css')}}" rel="stylesheet">
    @yield("head")
</head>
<body>
    @yield("body-content")
</body>
</html>
