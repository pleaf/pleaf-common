<!DOCTYPE html>
<html>
<head>
    <title>@yield('title')</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="{{URL::asset('css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/tambahan/stylesheet.css')}}" rel="stylesheet">
    <link href="{{URL::asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">

    <!-- Toastr style -->
    <link href="{{URL::asset('css/plugins/toastr/toastr.min.css')}}" rel="stylesheet">

    <!-- Gritter -->
    <link href="{{URL::asset('js/plugins/gritter/jquery.gritter.css')}}" rel="stylesheet">


    <!-- Adding Kendo Stuff -->
    @include("pleaf-core::includes/include-kendo")

    <link href="{{URL::asset('css/default.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/animate.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/style.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/override.css')}}" rel="stylesheet">
    @yield("head")
</head>
<body>
<div id="wrapper">
    @include("pleaf-common::includes/navigation")


    <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation">
                <div class="navbar-header col-md-12">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " style="margin-top: 9px;"><i class="fa fa-bars"></i> </a>
                    <ul class="ultenant">
                        <div class="tenant col-md-8">
                            <li><label>
                                    Current Role : &nbsp; &nbsp;{{ Session::get("sessUser")["role_default_name"] }}
                                </label></li>

                            @if(Session::get("sessUser")["user_id"] == -1)
                                <li><label>
                                        Current Tenant : &nbsp; &nbsp;{{ Session::get("sessUser")["tenant_name"]}}
                                    </label></li>
                            @endif
                        </div></ul>

                    <ul class="topmenu">

                        <li><div class="dropdown profile-element">
                                <a data-toggle="dropdown" class="dropdown-toggle welcome" href="#" >
                                    <strong>Welcome, {{ Session::get("sessUser")["username"] }}&nbsp;<i class="fa fa-chevron-down"></i> </strong>
                                </a>

                                <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                    <li>
                                        <a href="{{URL::to('admin/changePasswords')}}">Change Password</a>
                                    </li>
                                    <li>
                                        <a id="role" href="/admin/changerole">Change Role</a>
                                    </li>

                                    @if(Session::get("sessUser")["user_id"] == -1)
                                        {{--<li>--}}
                                        {{--<a id="tenant" href="#">Change Tenant</a>--}}
                                        {{--</li>--}}
                                        <li>
                                            <a href="{{URL::to('admin/changetenant')}}">Change Tenant</a>
                                        </li>
                                    @endif
                                </ul>

                            </div></li>
                        <li>
                            <a href="{{URL::to('userlogouts')}}" class="toplogout"><i class="fa fa-sign-out">&nbsp;</i>Log Out</a>
                        </li>
                    </ul>


                </div></nav></div>
        <div class="row wrapper border-bottom white-bg page-heading" style="margin-top:-30px;height:80px;">
            <div class="col-lg-10" style="margin-top: -5px;">
                <h2>@yield('page-title')</h2>
                <ol class="breadcrumb">
                    @yield('bread-crumb')

                </ol>
            </div>
            <div class="col-lg-2">

            </div>
        </div>


        <br>
        <br>

        <div class="wrapper wrapper-content animated fadeInUp" style="margin-top:-25px;">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>@yield("ibox-title")</h5>
                </div>
                <div class="ibox-content">
                    @yield("content")
                </div>
            </div>
        </div>

        {{--<div class="wrapper wrapper-content">--}}
            {{--<div class = "row">--}}
                {{--<div class="col-lg-12 col-md-12">--}}
                    {{--@yield('content')--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

        <div class="row">
            <div class="footer">
                <div>
                    <strong>Copyright</strong> SOLUSI TEKNOLOGI SEJATI &copy; 2016
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Mainly scripts -->

<script src="{{URL::asset('js/bootstrap.min.js')}}"></script>
<script src="{{URL::asset('js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
<script src="{{URL::asset('js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>

<!-- Mainly scripts -->

<!-- Custom and plugin javascript -->


<!-- Flot -->
<script src="{{URL::asset('js/plugins/flot/jquery.flot.js')}}"></script>
<script src="{{URL::asset('js/plugins/flot/jquery.flot.tooltip.min.js')}}"></script>
<script src="{{URL::asset('js/plugins/flot/jquery.flot.spline.js')}}"></script>
<script src="{{URL::asset('js/plugins/flot/jquery.flot.resize.js')}}"></script>
<script src="{{URL::asset('js/plugins/flot/jquery.flot.pie.js')}}"></script>

<!-- Peity -->
<script src="{{URL::asset('js/plugins/peity/jquery.peity.min.js')}}"></script>
<script src="{{URL::asset('js/demo/peity-demo.js')}}"></script>

<!-- Custom and plugin javascript -->
<script src="{{URL::asset('js/inspinia.js')}}"></script>
<script src="{{URL::asset('js/plugins/pace/pace.min.js')}}"></script>

<!-- jQuery UI -->
<script src="{{URL::asset('js/plugins/jquery-ui/jquery-ui.min.js')}}"></script>

<script src="{{URL::asset('js/plugins/peity/jquery.peity.min.js')}}"></script>
<script src="{{URL::asset('js/demo/peity-demo.js')}}"></script>

<script src="{{URL::asset('js/plugins/jquery-ui/jquery-ui.min.js')}}"></script>

{{--<!-- GITTER -->--}}
<script src="{{URL::asset('js/plugins/gritter/jquery.gritter.min.js')}}"></script>

{{--<!-- Sparkline -->--}}
<script src="{{URL::asset('js/plugins/sparkline/jquery.sparkline.min.js')}}"></script>

{{--<!-- Sparkline demo data  -->--}}
<script src="{{URL::asset('js/demo/sparkline-demo.js')}}"></script>

{{--<!-- ChartJS-->--}}
<script src="{{URL::asset('js/plugins/chartJs/Chart.min.js')}}"></script>

{{--<!-- Toastr -->--}}
<script src="{{URL::asset('js/plugins/toastr/toastr.min.js')}}"></script>


@yield("end-body")
<script type="text/javascript">
    jQuery(document).ready(function () {

        console.log($(".task"));


    });
</script>

<script>
    $(document).ready(function(){
        $(".task").each(function(key,value){
            console.log($(value).html());
        });

        console.log("ready function javascript");

    });
</script>


</body>
</html>
