@extends('pleaf-common::layouts.master')
@section('title')
        Combo Constant
@endsection

@section('page-title')
        Combo Constant
@endsection

@section('ibox-title')
        Edit Combo Constant
@endsection

@section('bread-crumb')
        <li class="active">
            <a href="{{URL::to('admin')}}">Home</a>

        </li>

        <li class="active">
            <a href="{{URL::to('admin/comboconstant')}}">Combo Constant</a>

        </li>

        <li class="active">
            <strong>Edit Combo Constant</strong>

        </li>

@endsection

@section('content-title')
    Edit Combo Constant Form
@endsection

@section('content')
    @messages("onField")
    <form action="{{URL::to('admin/comboconstant/update')}}"  method="post" class="form-horizontal">
        <br>
        <input type="hidden" value="{{$data_combo_constant->combo_id}}" name="combo_id" id='combo_id'>
        <input type="hidden" value="{{$data_combo_constant->code}}" name="code" id='code'>

        <div class="form-group">
          <div class="col-md-2">
            <label>Combo Id</label>
          </div>
          <div class="col-md-4">
            <input type="text" id="combo_id" name="combo_id" class="form-control" value="{{$data_combo_constant->combo_id}}" disabled>
          </div>
        </div>

        <div class="form-group">
          <div class="col-md-2">
            <label>Code</label>
          </div>
          <div class="col-md-4">
            <input type="text" id="code" name="code" class="form-control" value="{{$data_combo_constant->code}}" disabled>
          </div>
        </div>

        <div class="form-group">
            <div class="col-md-2">
              <label for="prop_key">Property Key</label>
            </div>
            <div class="col-md-4">
              <input type="text" id="prop_key" name="prop_key" class="form-control" value="{{$data_combo_constant->prop_key}}">
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-2">
              <label for="code_group">Code Group</label>
            </div>
            <div class="col-md-4">
              <input type="text" id="code_group" name="code_group" class="form-control" value="{{$data_combo_constant->code_group}}">
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-2">
              <label for="sort_no">Sort Number</label>
            </div>
            <div class="col-md-4">
              <input type="number" id="sort_no" name="sort_no" class="form-control" min="0" value="{{$data_combo_constant->sort_no}}">
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-2"></div>
            <div class="col-md-4">
                <a class="btn btn-white" href="{{URL::to('admin/comboconstant')}}">Cancel</a>
                <button class="btn btn-primary" type="submit" onclick="return'admin/combocontant'">Save</button>
            </div>
        </div>
    </form>


@endsection
