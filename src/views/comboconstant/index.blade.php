@extends('pleaf-common::layouts.master')
@section('head')
    <script src="/sts/pleaf-common/js/comboconstant.js"></script>
    @authorize('editComboValue')
        <style type="text/css">
            .task-editComboValue {
                visibility: inherit;
            }
        </style>
    @endauthorize
    @authorize('removeComboValue')
        <style type="text/css">
            .task-removeComboValue {
                visibility: inherit;
            }
        </style>
    @endauthorize

@endsection
@section('end-body')

@section('title')
        Combo Constant
@endsection

@section('page-title')
        Combo Constant
@endsection



@section('bread-crumb')
        <li class="active">
            <a href="{{URL::to('admin')}}">Home</a>

        </li>

        <li class="active">
            <strong>Combo Constant</strong>
        </li>

@endsection


@section('content')
@messages
@authorize('viewCombo')
    <div ng-app="comboconstantApp">
        <div ng-controller="comboconstantCtrl">
            @authorize('addComboValue')
                <a class="btn btn-primary" href="{{URL::to('admin/comboconstant/create')}}"><i class="fa fa-plus"></i> Add</a>&nbsp
            @endauthorize
            <a class="btn btn-primary" href="{{URL::to('admin/comboconstant')}}"><i class="fa fa-refresh"></i> Refresh</a>
            <br/><br/>

                <div id="grid"></div>
        </div>
    </div>
@endauthorize
@endsection
