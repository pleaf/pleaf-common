@extends('pleaf-common::layouts.master')
@section('head')
    <script src="/sts/pleaf-common/js/comboconstant.js"></script>
@endsection
@section('end-body')

@section('title')
        Combo Constant
@endsection

@section('page-title')
        Add Combo Constant
@endsection

@section('ibox-title')
        Add New Combo Constant
@endsection

@section('bread-crumb')
        <li class="active">
            <a href="{{URL::to('admin')}}">Home</a>

        </li>

        <li class="active">
            <a href="{{URL::to('admin/comboconstant')}}">Combo Constant</a>

        </li>

        <li class="active">
            <strong>Add Combo Constant</strong>

        </li>

@endsection


@section('content')
@messages("onField")
<div class="form-group row">
    <div class="col-md-2">

    </div>
    <div class="col-md-4">
        <div id="tenantId"></div>
    </div>
</div>
<form class="m-t" role="form" method="post" action="{{URL::to('admin/comboconstant/store')}}">
    <div ng-app="comboconstantApp">
        <div ng-controller="comboconstantCtrl">
            <div class="form-group row">
                <div class="col-md-2">
                    <label for="combo_id">Combo Id</label>
                </div>
                <div class="col-md-4">
                     <input type="text" id="combo_id" class="k-textbox" placeholder="Combo Id" style="width: 100%;height: 100%;" name="combo_id"/>
                </div>
            </div>

             <div class="form-group row">
                <div class="col-md-2">
                    <label for="code">Code</label>
                </div>
                <div class="col-md-4">
                     <input type="text" id="code" class="k-textbox" placeholder="Code" style="width: 100%;" name="code"/>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="prop_key">Property Key</label>
                </div>
                <div class="col-md-4">
                    <input type="text" id="prop_key" class="k-textbox" placeholder="Property Key" style="width: 100%;" name="prop_key"/>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="code_group">Code Group</label>
                </div>
                <div class="col-md-4">
                    <input type="text" id="code_group" class="k-textbox" placeholder="Code Group" style="width: 100%;" name="code_group"/>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="sort_no">Sort Number</label>
                </div>
                <div class="col-md-4">
                    <input type="number" id="sort_no" class="k-textbox" placeholder="0" style="width: 100%;" name="sort_no" value="0" min="0"/>
                </div>
            </div>


            <div class="form-group row">
                <div class="col-sm-4 col-sm-offset-2">
                    <a href="{{URL::to('/admin/comboconstant')}}" class="btn btn-white">Cancel</a>
                    <button class="btn btn-primary" type="submit" onclick="return confirm('Are you sure ?')">Save</button>
                </div>
            </div>
        </div>
    </div>

</form>

@endsection
