@extends('pleaf-common::layouts.master')
@section('head')
    <script src="/sts/pleaf-common/js/db_query.js"></script>
@endsection
@section('title')
		DB Query
@endsection

@section('page-title')
	DB Query
@endsection
@section('bread-crumb')
	<li>
		<a href="{{URL::to('admin')}}">Home</a>
	</li>
	<li class='active'>
		<strong>DB Query</strong>
	</li>
@endsection
@section('content-title')
	DB Query
@endsection
@section('content')
<div ng-app="DBQueryApp">
	<div ng-controller="DBQueryCtrl">

		<div class="row">
			<form ng-submit="onSubmit()">
				<div class="col-md-12">
					<button class="btn btn-primary"><i class="fa fa-play"></i> Run</button>&nbsp 
					<button type="button" ng-click="clearQuery()" class="btn btn-primary"><i class="fa fa-times"></i> Clear</button>
					<br/><br/>
				</div>
				<div class="col-md-12">                     
				    <textarea class="form-control" id="query" name="query" ng-model="query" placeholder="Silahkan masukan query anda" autofocus style="height: 200px; resize: vertical;"></textarea>
				</div>
			</form>
			
		</div>
		<br />
		<div class="row">
			<div class="col-md-12"> 
				<div id="messages" ng-if="showMessage">
	        		<div class="alert alert-danger" ng-if="messageType=='error'">
	        			<% errorMessage %>
	        		</div>
	        		<div class="alert alert-warning" ng-if="messageType=='warning'">
	        			<% errorMessage %>
	        		</div>
	        		<div class="alert alert-success" ng-if="messageType=='success'">
	        			<% errorMessage %>
	        		</div>
        		</div>
        		<div id="grid"></div>
        	</div>
		</div>

	</div>
</div>
@endsection
