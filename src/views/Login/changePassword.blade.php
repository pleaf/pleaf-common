@extends('pleaf-common::layouts.master')
@section('title')
		Change Password
@endsection

@section('content')
    <h1>Change Password</h1>
	<div class="ibox-content">
	                        @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                        <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                        </ul>
                                </div>
                            @endif
                            @if (Session::has('message'))
                                <div class="alert alert-danger">
                                        {{Session::get('message')}}
                                </div>
                            @endif
                            @if (Session::has('successMessage'))
                                <div class="alert alert-success">
                                        {{Session::get('successMessage')}}
                                </div>
                            @endif
                            <form action="{{URL::to('admin/doChangePasswords')}}"  method="post" class="form-horizontal">
                                {{--<input type="hidden" value="{{$t->tenant_id}}" name="t_id1" id='t_id1'>--}}
                                {{--<input type="hidden" name="t_version" value="{{$t->version}}"> --}}

                                <div class="form-group"><label class="col-sm-2 control-label">Username</label>
                                    <div class="col-sm-10"><input type="text" name="username" value="{{$dataUser->username}}" class="form-control" disabled>
                                    </div>
                                </div>
                                <div class="form-group"><label class="col-sm-2 control-label">Old Password</label>
                                    <div class="col-sm-10"><input type="password" value="{{old('password')}}" name="password" class="form-control" >
                                    </div>
                                </div>
                                <div class="form-group"><label class="col-sm-2 control-label">New Password</label>
                                    <div class="col-sm-10"><input type="password"  name="newPassword" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group"><label class="col-sm-2 control-label">Confirm New Password</label>
                                    <div class="col-sm-10"><input type="password"  name="confirmPassword" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <button class="btn btn-white" type="reset">Cancel</button>
                                        <button class="btn btn-primary" type="submit" onclick="return confirm('Are you sure ?')">Save changes</button>
                                    </div>
                                </div>
                                {{csrf_field()}}
                            </form>
                        </div>



@endsection