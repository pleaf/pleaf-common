<!DOCTYPE html>
<html>
<head>
	
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{ Config::get("pleaf_config.page_title") }} | Login</title>

    <link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">

    <link href="{{URL::asset('css/animate.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/style.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/override.css')}}" rel="stylesheet">
</head>
<body class="gray-bg">
	
		<div class="middle-box text-center loginscreen  animated fadeInDown">
        <div>
            <div>

                <h1 class="logo-name">
                    <img src="/img/{{Config::get("pleaf_config.app_logo") }} ">

                </h1>

            </div>

            <p>Please Login in here</p>
                            @if (Session::has('successMessage'))
                                <div class="alert alert-success">
                                        {{Session::get('successMessage')}}
                                </div>
                            @endif
                             @if (Session::has('errormsg'))
                                <div class="alert alert-danger">
                                        {{Session::get('errormsg')}}
                                </div>
                            @endif
            <div class="col-md-12">
                    <form class="m-t" role="form" action="{{URL::to('getlogins')}}" method="post">

                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Username" required="" name="txtUser">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" placeholder="Password" required="" name="txtPass">
                        </div>

                        <div class="form-group">
                            <td><input type="checkbox" name="chkRem" value='Y'></td>
                            <td><label data-toggle="tooltip" data-placement="left" title="Remember Me ?"> Remember Me? </label></td>
                        </div>

                        <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

                        <a href="{{URL::to('forgotpasswords')}}"><small>Forgot password?</small></a>

                        {{csrf_field()}}
                    </form>
                </div></center>
            <p class="m-t"> <small>{{ Config::get("pleaf_config.app_name") }} - {{ Config::get("pleaf_config.app_version") }}  {{ Config::get("pleaf_config.app_copyright_text") }}</small> </p>
        </div>
    </div>
		
		
		
	<script src="{{URL::asset('js/jquery-2.1.1.js')}}"></script>
    <script src="{{URL::asset('js/bootstrap.min.js')}}"></script>
	


</body> 
</html>