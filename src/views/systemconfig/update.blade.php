@extends('pleaf-common::layouts.master')
@section('head')
    <script src="/sts/pleaf-common/js/systemConfig.js"></script>
@endsection
@section('title')
        System Config
@endsection

@section('page-title')
        System Config
@endsection

@section('bread-crumb')
        <li class="active">
            <a href="{{URL::to('admin')}}">Home</a>
        </li>

        <li class="active">
            <a href="{{URL::to('/admin/systemconfig')}}">System Config</a>

        </li>

        <li class="active">
            <strong>Edit System Config</strong>

        </li>


@endsection

@section('content')
@section('ibox-title')
    Edit System Config
@endSection
    @messages("onField")
    <form action="{{URL::to('admin/systemconfig/update')}}"  method="post" class="form-horizontal">
        {{csrf_field()}}
        <input type="hidden" value="{{$parameter->parameter_id}}" name="parameterId" >
        <input type="hidden" value="{{$parameter->system_config_id}}" name="systemConfigId" >

        <div class="form-group"><label class="col-sm-2 control-label">Parameter</label>
            <div class="col-sm-10"><input type="text" name="parameterCode" class="form-control" value="{{$parameter->parameter_code}}" readonly>
            </div>
        </div>

        <div class="form-group"><label class="col-sm-2 control-label">Description</label>
            <div class="col-sm-10"><input type="text" name="parameterName" class="form-control" value="{{$parameter->parameter_desc}}" readonly>
            </div>
        </div>
        <div class="hr-line-dashed"></div>
        @if($parameter->parameter_data_type=='numeric')
        <div class="form-group"><label class="col-lg-2 control-label">Value *</label>
            <div class="col-sm-10"><input type="number" id="parameterValue" name = "parameterValue" class="form-control" value="{{$parameter->parameter_def_value}}">
            </div>
        </div>
        @else
        <div class="form-group"><label class="col-lg-2 control-label">Value *</label>
            <div class="col-sm-10"><input type="text" id="parameterValue" name = "parameterValue" class="form-control" value="{{$parameter->parameter_def_value}}">
            </div>
        </div>
        @endif
        <div class="hr-line-dashed"></div>


        <div class="form-group">
            <div class="col-sm-4 col-sm-offset-2">
                <a class="btn btn-white" href="{{URL::to('/admin/systemconfig')}}">Cancel</a>
                <button class="btn btn-primary" type="submit" onclick="return'admin/systemconfig'">Save changes</button>
            </div>
        </div>
    </form>
@endsection
