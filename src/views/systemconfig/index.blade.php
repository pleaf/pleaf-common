@extends('pleaf-common::layouts.master')
@section('head')
    <script src="/sts/pleaf-common/js/systemConfig.js"></script>
@endsection
@section('title')
        System Config
@endsection

@section('page-title')
        System Config
@endsection

@section('bread-crumb')
        <li class="active">
            <a href="{{URL::to('admin')}}">Home</a>
        </li>

        <li class="active">
            <strong>System Config</strong>
        </li>

        {{--<li class="active">--}}
            {{--<a href="{{URL::to('admin/tenant/create')}}">Add Tenant</a>--}}

        {{--</li>--}}


@endsection

@section('content-title')
    System Config Table
@endsection

@section('content')
@authorize('viewConfig')
    <a class="btn btn-primary" href="{{URL::to('admin/systemconfig')}}"><i class="fa fa-refresh"></i> Refresh</a>
    <br>
    <br>
    <div ng-app="systemConfigApp">
        <div ng-controller="systemConfigCtrl">
            @messages
                <div id="gridOptions"></div>
        </div>
        @endauthorize
    </div>
@endsection

<script>
    angular.module("KendoDemos", [ "kendo.directives" ]);
    function MyCtrl($scope) {

        $scope.checkboxClicked = function(dataItem) {
            console.log(dataItem);

        };
        $scope.mainGridOptions = {
            dataSource: {
                type: "odata",
                transport: {
                    read: "http://demos.telerik.com/kendo-ui/service/Northwind.svc/Employees"
                },
                pageSize: 5,
                serverPaging: true,
                serverSorting: true
            },
            sortable: true,
            pageable: true,
            columns: [
                { template: '<input type="checkbox" ng-click="checkboxClicked(dataItem)">' },
                {
                    field: "FirstName",
                    title: "First Name",
                    width: "120px"
                },{
                    field: "LastName",
                    title: "Last Name",
                    width: "120px"
                },{
                    field: "Country",
                    width: "120px"
                },{
                    field: "City",
                    width: "120px"
                },{
                    field: "Title"
                }]
        };
    }
</script>