@extends('pleaf-common::layouts.master')
@section('title')
		OU-Type
@endsection
@section('page-title')
    Edit OU Type
@endsection
@section('bread-crumb')
    <li>
        <a href="{{URL::to('admin')}}">Home</a>
    </li>
    <li>
        <a href="{{URL::to('admin/outype')}}">OU Type</a>
    </li>
    <li class="active">
        <strong>Edit OU Type</strong>
    </li>
@endsection
@section('content-title')
    Edit OU Type Form
@endsection

@section('ibox-title')
    Edit OU Type
@endsection

@section('content')
@messages("onField")

	<form class="m-t" role="form" method="post" action="{{URL::to('admin/outype/update')}}">
         {{csrf_field()}}
         <input type="hidden" value="{{$ouType -> ou_type_id}}" name="txtID">
         <input type="hidden" value="{{$ouType -> ou_type_version}}" name="version">
		<div class="form-group row">
            <div class="col-md-2">
                <label>
                    OU Type Code
                </label>
            </div>
            <div class="col-md-4">
                <input type="text" class="form-control" placeholder="OU Type Code" id="ou_type_code" name="ou_type_code" class="form-control" value="{{$ouType->ou_type_code}}" disabled>
            </div>
        </div>

         <div class="form-group row">
            <div class="col-md-2">
                <label>
                    OU Type Name
                </label>
            </div>
            <div class="col-md-4">
                <input type="text" class="form-control" placeholder="OU Type Name" id="ou_type_name" name="ou_type_name" class="form-control" value="{{$ouType->ou_type_name}}" enable>
            </div>
         </div>

         <div class="form-group row">
           <div class="col-md-2">
                <label>

                </label>
            </div>

         	<div class="col-md-4">
         		<a href="{{URL::to('admin/outype')}}" class="btn btn-white">Cancel</a> &nbsp
         		<button type="submit" class="btn btn-primary">Save</button>
         		{{csrf_field()}}
         </div>

	</form>

@endsection
