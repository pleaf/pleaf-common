@extends('pleaf-common::layouts.master')

@section('head')
<script src="/sts/pleaf-common/js/outype.js"></script>

    @authorize('editOuType')
        <style type="text/css">
            .task-editOuType {
                visibility: inherit;
            }
        </style>
    @endauthorize

    @authorize('removeOuType')
        <style type="text/css">
            .task-removeOuType {
                visibility: inherit;
            }
        </style>
    @endauthorize

@endsection

@section('title')
	OU-Type
@endsection

@section('page-title')
	OU Type
@endsection

@section('bread-crumb')
	<li>
		<a href="{{URL::to('admin')}}">Home</a>
	</li>
	<li class="active">
		<strong>OU Type</strong>
	</li>
@endsection

@section('content-title')
	OU Type Table
@endsection

@section('content')
@messages

@authorize('viewOuType')
    <div ng-app="ouTypeApp">
        <div ng-controller="ouTypeCtrl">
            @authorize('addOuType')
                <a href="{{URL::to('admin/outype/create')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Add</a>&nbsp
            @endauthorize
             <a href="{{URL::to('admin/outype')}}" class="btn btn-primary"><i class="fa fa-refresh"></i> Refresh</a>
            </br>
            </br>
                <div id="gridOptions"></div>

            @endauthorize
        </div>
    </div>
@endsection

{{--<script>--}}
	{{--function editOuTypeId(editId) {--}}

		{{--var ouTypeIdEdit = $("#gridOptions").data("kendoGrid").dataItem($(editId).closest("tr"));--}}
		{{--window.location.href='/admin/outype/edit/'+ouTypeIdEdit.ou_type_id;--}}

	{{--}--}}
	{{--function deleteOuTypeId(deleteId) {--}}

		{{--var ok = confirm('Are You Sure ?');--}}
		{{--if(ok == true){--}}
			{{--var ouTypeIdDelete = $("#gridOptions").data("kendoGrid").dataItem($(deleteId).closest("tr"));--}}
			{{--window.location.href='/admin/outype/destroy/'+ouTypeIdDelete.ou_type_id;--}}
		{{--}--}}

	{{--}--}}
{{--</script>--}}