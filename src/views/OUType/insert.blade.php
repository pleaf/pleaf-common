@extends('pleaf-common::layouts.master')

@section('head')
<script src="/sts/pleaf-common/js/outype.js"></script>

@endsection

@section('title')
		OU-Type
@endsection
@section('page-title')
    Add New OU Type
@endsection
@section('bread-crumb')
    <li>
        <a href="{{URL::to('admin')}}">Home</a>
    </li>
    <li>
        <a href="{{URL::to('admin/outype')}}">OU Type</a>
    </li>
    <li>
        <strong>Add OU Type</strong>
    </li>
@endsection
@section('content-title')
    Add New OU Type Form
@endsection

@section('ibox-title')
    Add New OU Type 
@endsection

@section('content')

@messages("onField")
    <div class="form-group row">
        <div class="col-md-2">

        </div>
        <div class="col-md-4">
            <div id="tenant_id"></div>
        </div>
    </div>

	<form class="m-t" role="form" method="post" action="{{URL::to('admin/outype/store')}}">
		<div class="form-group row">
            <div class="col-md-2">
                 <label for="ou_type_code">
                    OU Type Code
                 </label>
             </div>
             <div class="col-md-4">
                <input type="text" class="form-control" placeholder="OU Type Code"  id="ou_type_code" name="ou_type_code">
             </div>

         </div>

         <div class="form-group row">
            <div class="col-md-2">
                 <label for="ou_type_name">
                    OU Type Name
                 </label>
             </div>
             <div class="col-md-4">
                 <input type="text" class="form-control" placeholder="OU Type Name" id="ou_type_name" name="ou_type_name">
             </div>

         </div>

         <div class="form-group row">
            <div class="col-md-2">
             </div>
             <div class="col-md-4">
             
				<table width="200">
		    		<tr>
		        		<td><input type="checkbox" name="chkBU" value='Y'></td>
		        		<td><label data-toggle="tooltip" data-placement="left" title="Do you want cheked Business Unit ?">Business Unit </label></td>
		    			
		    		</tr>

		    		<tr>
		    			<td><input type="checkbox" name="chkAccounting" value='Y'></td>
		    			<td> <label data-toggle="tooltip" data-placement="left" title="Do you want cheked Accounting ?"> Accounting </label></td>
		    		</tr>

		    		<tr>
		    			<td><input type="checkbox" name="chkLegal" value='Y'></td>
		    			<td><label data-toggle="tooltip" data-placement="left" title="Do you want cheked Legal ?"> Legal </label></td>
		    		</tr>
		
		     		<tr>
		    			<td><input type="checkbox" name="chkBranch" value='Y'></td>
		    			<td><label data-toggle="tooltip" data-placement="left" title="Do you want cheked Sub Business Unit ?"> Sub Business Unit </label></td>
		    		</tr>
		
				    <tr>
		    			<td><input type="checkbox" name="chkSBU" value='Y'></td>
		    			<td><label data-toggle="tooltip" data-placement="left" title="Do you want cheked Branch ?"> Branch </label></td>
		    		</tr>
		
				</table>
             </div>

         </div>
		
		<div class="form-group row">
         	<div class="col-md-2">
  		    </div>
            <div class="col-md-4">
             	<a href="{{URL::to('admin/outype')}}" class="btn btn-white">Cancel</a> &nbsp
         	 	<button type="submit" class="btn btn-primary">Save</button>
         	  	{{csrf_field()}}
		 	</div>

	</form>

@endsection
