@extends('pleaf-common::layouts.master')
@section('head')
    <script src="/sts/pleaf-common/js/groupUser.js"></script>
@endsection
@section('title')
        Group User
@endsection

@section('page-title')
        Group User
@endsection

@section('bread-crumb')
        <li class="active">
            <a href="{{URL::to('admin')}}">Home</a>
        </li>

        <li class="active">
            <a href="{{URL::to('admin/groupuser')}}">Group User</a>

        </li>

        <li class="active">
            <strong>Add Group User</strong>
        </li>


@endsection

@section('content')
   @section('ibox-title')
    Add Group User
   @endsection

   @messages("onField")
    <div class="form-group row">
        <div class="col-md-2">

        </div>
        <div class="col-md-4">
            <div id="tenantId"></div>
        </div>
    </div>

   <form action="{{URL::to('/admin/groupuser/save')}}" method="post">
       <div class="form-group row">
            <div class="col-md-2">
                <label for="groupUserCode">Code</label>
            </div>
            <div class="col-md-4">
                <input type="text" class="form-control" id="groupUserCode" name="groupUserCode"
                placeholder="765" value="{{old('groupUserCode')}}">
            </div>
       </div>
       <div class="form-group row">
            <div class="col-md-2">
                <label for="groupUserName">Name</label>
            </div>
            <div class="col-md-4">
                <input type="text" class="form-control" id="groupUserName" name="groupUserName"
                placeholder="STS" value="{{old('groupUserName')}}">
            </div>
       </div>
       <div class="form-group row">
           <div class="col-md-2">
               <label for="groupUserDesc">Description</label>
           </div>
           <div class="col-md-4">
               <textarea id="groupUserDesc" name="groupUserDesc" class="form-control"
               placeholder="Group Description" value="{{old('groupUserDesc')}}"></textarea>
           </div>
       </div>
       <div class="form-group row">
           <div class="col-md-2">

           </div>
           <div class="col-md-4">
               <a class="btn btn-white" href="{{URL::to('admin/groupuser')}}">Cancel</a>
               <button type="submit" class="btn btn-primary">Save</button>
           </div>
       </div>


     {{csrf_field()}}
   </form>

@endsection
