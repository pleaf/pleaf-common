@extends('pleaf-common::layouts.master')
@section('head')
    <script src="/sts/pleaf-common/js/groupUser.js"></script>

    @authorize('editGroupUser')
        <style type="text/css">
            .task-editGroupUser {
                visibility: inherit;
            }
        </style>
    @endauthorize
    @authorize('removeGroupUser')
        <style type="text/css">
            .task-removeGroupUser {
                visibility: inherit;
            }
        </style>
    @endauthorize
@endsection
@section('title')
        Group User
@endsection

@section('page-title')
        Group User
@endsection

@section('bread-crumb')
        <li class="active">
            <a href="{{URL::to('admin')}}">Home</a>
        </li>

        <li class="active">
            <strong>Group User</strong>

        </li>
<!--
        <li class="active">
            <a href="{{URL::to('/admin/groupuser/add')}}">Add Group User</a>

        </li> -->


@endsection

@section('content')
@messages
@authorize('viewGroupUser')
    @authorize('addGroupUser')
    <a href="{{URL::to('admin/groupuser/add')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Add</a>&nbsp
    @endauthorize
    <a href="{{URL::to('admin/groupuser')}}" class="btn btn-primary"><i class="fa fa-refresh"></i> Refresh</a>
    <br/><br/>

    <div ng-app="groupUserApp">
        <div ng-controller="groupUserCtrl">

                <div id="gridOptions"></div>

        </div>
    </div>
@endauthorize
@endsection
