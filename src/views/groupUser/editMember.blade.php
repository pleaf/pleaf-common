@extends('pleaf-common::layouts.master')
@section('head')
    <script src="/sts/pleaf-common/js/editMemberGroupUser.js"></script>
@endsection
@section('title')
        Group User
@endsection

@section('page-title')
        Group User
@endsection

@section('bread-crumb')
        <li class="active">
            <a href="{{URL::to('admin')}}">Home</a>
        </li>

        <li class="active">
            <a href="{{URL::to('admin/groupuser')}}">Group User</a>

        </li>

        <li class="active">
            <strong>Edit Member</strong>
        </li>


@endsection

@section('content')

    @authorize('viewMemberGroupUser')
        <div ng-app="editMemberGroupUserApp">
            <div ng-controller="editMemberGroupUserCtrl">
                <div ng-init='getId({{$groupUserId}})'></div>
                <div ng-if="showMessage==true">
                    <div class="alert alert-success" ng-if="messageType=='success'">
                        <% message %>
                    </div>
                    <div class="alert alert-danger" ng-if="messageType=='error'">
                        <% message %>
                    </div>
                </div>
                <div class="rows">
                    <div class="col-md-5">
                        <div id="gridOptions2"></div>
                    </div>
                    <div class="col-md-2">
                        <button class="btn btn-primary" id="showSelection2">Add Member >></button>
                        <br>
                        <br>
                        <button class="btn btn-primary" id="showSelection"><< Delete Member</button>
                    </div>
                    <div class="col-md-5">
                        <div id="gridOptions"></div>
                    </div>
                </div>
            </div>
        <div>
       </div>
      </div>
    @endauthorize

@endsection
