@authorize('editGroupUser')

@extends('pleaf-common::layouts.master')
@section('head')
    <script src="/sts/pleaf-common/js/groupUser.js"></script>
@endsection
@section('title')
        Group User
@endsection

@section('page-title')
        Group User
@endsection

@section('bread-crumb')
        <li class="active">
            <a href="{{URL::to('admin')}}">Home</a>
        </li>

        <li class="active">
            <a href="{{URL::to('admin/groupuser')}}">Group User</a>

        </li>

        <li class="active">
            <strong>Edit Group User</strong>
        </li>


@endsection

@section('content')
@section('ibox-title')
 Edit Group User
@endsection
   <!-- @if(count($errors)>0)
       <div class="alert alert-danger"
           <ul>
           @foreach($errors->all() as $error)
               <li>{{$error}}</li>
           @endforeach
           </ul>
       </div>
   @endif -->
   @messages("onField")
   <form action="{{URL::to('/admin/groupuser/update')}}" method="post">
       <div class="form-group row">
           <div class="col-md-2">
           <label>Code</label>
           </div>
           <div class="col-md-4">
           <input type="hidden" name="groupUserId" value="{{$groupUserData->group_user_id}}">
           <input id="code" type="text" class="form-control" name="code" placeholder="765"
                value="{{$groupUserData->group_user_code}}" disabled>
            </div>
       </div>
       <div class="form-group row">
           <div class="col-md-2">
           <label>Name</label>
           </div>
           <div class="col-md-4">
           <input type="text" id="groupUserName" class="form-control" name="groupUserName" placeholder="STS"
                value="{{$groupUserData->group_user_name}}">
           </div>
       </div>
       <div class="form-group row">
           <div class="col-md-2">
               <label>Description</label>
           </div>
           <div class="col-md-4">
               <textarea id="groupUserDesc" name="groupUserDesc" class="form-control" placeholder="Group Description">{{$groupUserData->group_user_desc}}</textarea>
           </div>
       </div>
       <div class="form-group row">
           <div class="col-md-2">

           </div>
           <div class="col-md-4">
               <a class="btn btn-white" href="{{URL::to('admin/groupuser')}}">Cancel</a>
               <button type="submit" class="btn btn-primary">Save</button>
           </div>
       </di>

     {{csrf_field()}}
   </form>
@endsection
@endauthorize
