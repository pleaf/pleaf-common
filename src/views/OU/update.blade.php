@extends('pleaf-common::layouts.master')
@section('title')
    Edit OU
@endsection
@section('page-title')
    Edit OU
@endsection
@section('bread-crumb')
    <li>
        <a href="{{URL::to('admin')}}">Home</a>
    </li>
    <li>
        <a href="{{URL::to('admin/ou')}}">OU</a>
    </li>
    <li class="active">
        <strong>Edit OU</strong>
    </li>
@endsection
@section('content-title')
    Edit OU Form
@endsection

@section('ibox-title')
    Edit OU
@endsection

@section('content')
    @messages
    <form class="m-t" role="form" method="post" action="{{URL::to('admin/ou/update')}}">
        <input type="hidden" name="ou_id" value="{{$data_ou->ou_id}}">
        <input type="hidden" name="txtVersion" value="{{$data_ou->version}}">
        <div class="form-group row">
            <div class = "col-md-2">
                <label>
                    OU Code
                </label>
            </div>
            <div class="col-md-4">
                <input type="text" class="form-control" placeholder="Code" value="{{$data_ou->ou_code}}" name="txtCode" disabled>
            </div>

        </div>

        <div class="form-group row">
            <div class="col-md-2">
                <label>
                    OU Type
                </label>
            </div>
            <div class="col-md-4">
                <select class='form-control m-b' id="ou_type_id" name='ou_type_id'>
                    @foreach($ou_type_list as $row)
                        @if($row->ou_type_id==$data_ou->ou_type_id)
                            <option value='{{$row->ou_type_id}}' selected>
                                {{$row->ou_type_name}}
                            </option>
                        @else
                            <option value='{{$row->ou_type_id}}' >
                                {{$row->ou_type_name}}
                            </option>

                        @endif
                    @endforeach -->
                </select>
            </div>

        </div>

        <div class="form-group row">
            <div class="col-md-2">
                <label>
                    OU Name
                </label>
            </div>
            <div class="col-md-4">
                <input type="text" class="form-control" placeholder="Name" value="{{$data_ou->ou_name}}" id="ou_name" name="ou_name">

            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-2">
                <label>Parent OU</label>
            </div>
            <div class="col-md-4">
                <select class='form-control m-b' name='parent_ou_id'>
                    <option value="0">None</option>
                    @foreach($ou_list as $row)
                        @if($row->ou_id == $data_ou->ou_parent_id)
                            <option value='{{$row->ou_id}}' selected>
                                {{$row->ou_name}}
                            </option>
                        @else
                            <option value='{{$row->ou_id}}'>
                                {{$row->ou_name}}
                            </option>
                        @endif
                    @endforeach
                </select>
            </div>

        </div>

        <div class="form-group row">
            <div class="col-sm-4 col-sm-offset-2">
                <a href="{{URL::to('admin/ou')}}" class="btn btn-white">Cancel</a>&nbsp
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </div>

        {{csrf_field()}}

    </form>

@endsection
