@extends('pleaf-common::layouts.master')

@section('head')
<script src="/sts/pleaf-common/js/ou.js"></script>
	@authorize('editOu')
		<style type="text/css">
			.task-editOu {
				visibility: inherit;
			}
		</style>
	@endauthorize

	@authorize('removeOu')
		<style type="text/css">
			.task-removeOu {
				visibility: inherit;
			}
		</style>
	@endauthorize

@endsection

@section('title')
		OU
@endsection

@section('page-title')
	OU
@endsection
@section('bread-crumb')
	<li>
		<a href="{{URL::to('admin')}}">Home</a>
	</li>
	<li class='active'>
		<strong>OU</strong>
	</li>
@endsection
@section('content-title')
	OU Table
@endsection
@section('content')

@messages
@authorize('viewOu')
		<div ng-app="ouApp">
    		<div ng-controller="ouCtrl">
                @authorize('addOu')
                    <a href="{{URL::to('admin/ou/create')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Add</a>&nbsp
                @endauthorize
				<a href="{{URL::to('admin/ou')}}" class="btn btn-primary"><i class="fa fa-refresh"></i> Refresh</a>
		</br>
		</br>
		<div id="gridOptions"></div>

	    	</div>
		</div>
@endauthorize
@endsection


{{--<script>--}}
    {{--function editOu(editId) {--}}

        {{--var ouEditId = $("#gridOptions").data("kendoGrid").dataItem($(editId).closest("tr"));--}}
        {{--window.location.href='/admin/ou/edit/'+ouEditId.ou_id;--}}

    {{--}--}}
    {{--function deleteOu(deleteId) {--}}

        {{--var ok = confirm('Are You Sure ?');--}}
        {{--if(ok == true){--}}
            {{--var ouDeleteId = $("#gridOptions").data("kendoGrid").dataItem($(deleteId).closest("tr"));--}}
            {{--window.location.href='/admin/ou/destroy/'+ouDeleteId.ou_id;--}}
        {{--}--}}

    {{--}--}}
{{--</script>--}}
