@extends('pleaf-common::layouts.master')
@section('title')
		OU
@endsection
@section('page-title')
    Add New OU
@endsection
@section('bread-crumb')
    <li>
        <a href="{{URL::to('admin')}}">Home</a>
    </li>
    <li>
        <a href="{{URL::to('admin/ou')}}">OU</a>
    </li>
    <li class="active">
        <strong>Add OU</strong>
    </li>
@endsection
@section('content-title')
    Add New OU Form
@endsection

@section('ibox-title')
    Add New OU
@endsection

@section('content')

@messages("onField")
    <div class="form-group row">
        <div class="col-md-2">

        </div>
        <div class="col-md-4">
            <div id="tenant_id"></div>
        </div>
    </div>

	<form class="m-t" role="form" method="post" action="{{URL::to('admin/ou/store')}}">
		<div class="form-group row">
            <div class="col-md-2">
                 <label for="ou_code">
                   OU Code
                 </label>
             </div>
             <div class="col-md-4">
                    <input type="text" class="form-control" placeholder="Code" id="ou_code" name="ou_code">
             </div>
         </div>

         <div class="form-group row">
             <div class="col-md-2">
                 <label>
                   OU Type
                 </label>
             </div>
             <div class="col-md-4">
                 <select class="form-control m-b" id="" name='ou_type_id'>
                     @foreach($ou_type_list as $row)
                        <option value='{{$row->ou_type_id}}'>
                            {{$row->ou_type_name}}
                        </option>
                    @endforeach
                 </select>
             </div>
         </div>

         <div class="form-group row">
            <div class="col-md-2">
            	<label for="ou_name">
                   OU Name
                 </label>
             </div>
             <div class="col-md-4">
                 <input type="text" class="form-control" placeholder="Name" id="ou_name" name="ou_name">
             </div>
         </div>

         <div class="form-group row">
             <div class="col-md-2">
                 <label>
                    Parent OU
                 </label>
             </div>
             <div class="col-md-4">
                 <select class="form-control m-b" name='ou_parent_id'>
                    <option value="0">None</option>
                     @foreach($ou_name_list as $row)
                        <option value='{{$row->ou_id}}'>
                            {{$row->ou_name}}
                        </option>
                    @endforeach
                 </select>
             </div>
         </div>

				 <div class="form-group row">
	         <div class = "col-sm-4 col-sm-offset-2">
	                <a href="{{URL::to('admin/ou')}}" class="btn btn-white">Cancel</a> &nbsp
	                <button type="submit" class="btn btn-primary">Save</button>
	         </div>
				 </div>

        {{csrf_field()}}

	</form>

@endsection
