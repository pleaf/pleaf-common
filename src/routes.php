<?php

Route::group(['middleware'=>['web']],function() {
	Route::get('/','\Sts\PleafCommon\Controllers\DoLoginController@index');
	Route::post('getlogins','\Sts\PleafCommon\Controllers\DoLoginController@getLogin');
	Route::get('userlogouts', '\Sts\PleafCommon\Controllers\DoLogoutController@index');
	Route::get('forgotpasswords', '\Sts\PleafCommon\Controllers\ForgotPasswordController@index');
	Route::post('sendmailresets', '\Sts\PleafCommon\Controllers\ForgotPasswordController@mailReset');
	Route::get('resetPasswords/{email}/{key}', ['as'=>'reset-password' , 'uses'=>'\Sts\PleafCommon\Controllers\ForgotPasswordController@resetindex'] );
	Route::post('updatepasswords','\Sts\PleafCommon\Controllers\ForgotPasswordController@update');


	Route::group(['prefix' => '/api-rest'],function(){
		Route::get('/systemconfig/list','\Sts\PleafCommon\Controllers\SystemConfigController@getSystemConfigListRest');
		Route::post('/add-tenant','\Sts\PleafCommon\Controllers\TenantController@addTenantRest');
        Route::put('/update-tenant','\Sts\PleafCommon\Controllers\TenantController@updateTenantRest');
		Route::delete('/delete-tenant','\Sts\PleafCommon\Controllers\TenantController@deleteTenantRest');
	});

	Route::get('session', function() {
		$data = Session::get('sessUser');
		print_r($data);
		
	});

	Route::get('cookie',function(){
		$data = Cookie::get("user_cookie");
		$data_cookie=unserialize($data);
		print_r ($data);
	});

	Route::group(['middleware'=>'loginuser'],function(){
		Route::get('/multi',function(){
			return view('pleaf-common::multitab.multi');
		});
	
	});

    include "api-routes.php";

	Route::group(['middleware'=>'loginuser','prefix'=>'admin'],function()
	{	
		Route::get('/',function()
		{
			return view('pleaf-common::Admin.index');
		});
		
		Route::group(['prefix' => '/api'],function(){
			Route::get('/tenant/list','\Sts\PleafCommon\Controllers\TenantController@getTenantList');
			Route::get('/systemconfig/list','\Sts\PleafCommon\Controllers\SystemConfigController@getSystemConfigList');
			Route::get('/outype/list', '\Sts\PleafCommon\Controllers\OUTypeController@getOUTypeList');
			Route::get('/users/list', '\Sts\PleafCommon\Controllers\UserController@getUsersList');
			Route::get('/tenant/list','\Sts\PleafCommon\Controllers\TenantController@getTenantList');
			Route::get('/systemconfig/list','\Sts\PleafCommon\Controllers\SystemConfigController@getSystemConfigList');
			Route::get('/comboconstant/list','\Sts\PleafCommon\Controllers\ComboConstantController@getComboConstantList');
			Route::get('/policy/list','\Sts\PleafCommon\Controllers\PolicyController@getPolicyList');
			Route::get('/ou/list','\Sts\PleafCommon\Controllers\OUController@getOUList');
			Route::post('/roletask/list','\Sts\PleafCommon\Controllers\RoleController@getRoleTask');
			Route::post('/manage-user-role/list', '\Sts\PleafCommon\Controllers\UserController@getManageUserRoleList');
		});

        Route::get('changePasswords','Sts\PleafCommon\Controllers\ChangePasswordController@index');
        Route::post('doChangePasswords','Sts\PleafCommon\Controllers\ChangePasswordController@doChangepassword');
        Route::get('defaultPasswords/{id}','Sts\PleafCommon\Controllers\ResetPasswordController@resetpassword');

        Route::get('changerole','Sts\PleafCommon\Controllers\UserController@changerole');
        Route::post('gantirole','Sts\PleafCommon\Controllers\UserController@gantirole');
        Route::get('changetenant','Sts\PleafCommon\Controllers\UserController@changetenant');
        Route::post('gantitenant','Sts\PleafCommon\Controllers\UserController@gantitenant');


		Route::group(['prefix'=>'outype'],function(){
			Route::get('/', '\Sts\PleafCommon\Controllers\OUTypeController@index');
			Route::get('create', '\Sts\PleafCommon\Controllers\OUTypeController@create');
			Route::post('store', '\Sts\PleafCommon\Controllers\OUTypeController@store');
			Route::get('destroy/{id}', '\Sts\PleafCommon\Controllers\OUTypeController@destroy');
			Route::get('edit/{id}', '\Sts\PleafCommon\Controllers\OUTypeController@edit');
			Route::post('update','\Sts\PleafCommon\Controllers\OUTypeController@update');
			Route::get('search', '\Sts\PleafCommon\Controllers\OUTypeController@search');
		});
			
		Route::group(['prefix'=>'ou'],function()
		{
			Route::get('/', '\Sts\PleafCommon\Controllers\OUController@index');
			Route::get('create', '\Sts\PleafCommon\Controllers\OUController@create');
			Route::post('store', '\Sts\PleafCommon\Controllers\OUController@store');
			Route::get('destroy/{id}', '\Sts\PleafCommon\Controllers\OUController@destroy');
			Route::get('edit/{id}', '\Sts\PleafCommon\Controllers\OUController@edit');
			Route::post('update','\Sts\PleafCommon\Controllers\OUController@update');
			Route::get('search', '\Sts\PleafCommon\Controllers\OUController@search');
		});

		Route::group(['prefix'=>'tenant'],function(){
			Route::get('/', '\Sts\PleafCommon\Controllers\TenantController@index');
			Route::get('create','\Sts\PleafCommon\Controllers\TenantController@create');
			Route::post('store','\Sts\PleafCommon\Controllers\TenantController@store');
			Route::post('update'	,'\Sts\PleafCommon\Controllers\TenantController@update');
			Route::get('edit/{id}','\Sts\PleafCommon\Controllers\TenantController@edit');
			Route::get('refresh'	,'\Sts\PleafCommon\Controllers\TenantController@refresh');
			Route::get('search'	, '\Sts\PleafCommon\Controllers\TenantController@search');

            Route::get('/change-tenant', '\Sts\PleafCommon\Controllers\TenantController@changeTenantForUser');
            Route::get('/kickout-tenant', '\Sts\PleafCommon\Controllers\TenantController@kickOutSessionTenant');
            Route::get('/view-manage-tenant/{id}', '\Sts\PleafCommon\Controllers\TenantController@viewManageTenantUser');
            Route::get('/policy-user-tenant/{id}', '\Sts\PleafCommon\Controllers\TenantController@getPolicyManageUserTenantList');
            Route::post('/do-change-tenant', '\Sts\PleafCommon\Controllers\TenantController@doChangeTenant');

		});

		Route::group(['prefix'=>'policy'],function(){
			Route::get('create','\Sts\PleafCommon\Controllers\PolicyController@create');
			Route::post('store','\Sts\PleafCommon\Controllers\PolicyController@store');			        
			Route::post('update','\Sts\PleafCommon\Controllers\PolicyController@update');			        
			Route::get('edit/{id}','\Sts\PleafCommon\Controllers\PolicyController@edit');			        
			Route::get('delete/{id}','\Sts\PleafCommon\Controllers\PolicyController@destroy');			        
			Route::get('search', '\Sts\PleafCommon\Controllers\PolicyController@search');		        
			Route::get('/','\Sts\PleafCommon\Controllers\PolicyController@index');
		});

		Route::group(['prefix'=>'user'],function(){
			Route::get('create','\Sts\PleafCommon\Controllers\UserController@create');
			Route::post('store','\Sts\PleafCommon\Controllers\UserController@store');
			Route::post('update','\Sts\PleafCommon\Controllers\UserController@update');
			Route::get('edit/{id}','\Sts\PleafCommon\Controllers\UserController@edit');			        
			Route::get('delete/{id}','\Sts\PleafCommon\Controllers\UserController@destroy');
			Route::get('removeUserRole/{id}/{userId}','\Sts\PleafCommon\Controllers\UserController@removeUserRole');
			Route::get('search', '\Sts\PleafCommon\Controllers\UserController@search');		        
			Route::get('/','\Sts\PleafCommon\Controllers\UserController@index');
			Route::get('/manage-user/{id}','\Sts\PleafCommon\Controllers\UserController@manageUser');
			Route::post('/manage-user',
                '\Sts\PleafCommon\Controllers\UserController@doAddManageUser');
		});

		Route::group(['prefix'=>'/role'],function(){
            // ONLY GET
			Route::get('/','\Sts\PleafCommon\Controllers\RoleController@index');
			Route::get('/create','\Sts\PleafCommon\Controllers\RoleController@create');
			Route::get('/edit/{id}' , '\Sts\PleafCommon\Controllers\RoleController@getEditRole');
			Route::get('search', '\Sts\PleafCommon\Controllers\RoleController@search');
			Route::get('role-task/{id}','\Sts\PleafCommon\Controllers\RoleController@role');
            Route::get('/delete/{id}','Sts\PleafCommon\Controllers\RoleController@doDeleteRole');
            Route::get('/list' , '\Sts\PleafCommon\Controllers\RoleController@getRoleList');

            //POST,PUT
            Route::post('/store', '\Sts\PleafCommon\Controllers\RoleController@store');
            Route::post('/update','\Sts\PleafCommon\Controllers\RoleController@doUpdateRole');

			Route::post('show-role-task', '\Sts\PleafCommon\Controllers\RoleTaskController@getRoleTask');
			Route::post('show-role-task/update', '\Sts\PleafCommon\Controllers\RoleTaskController@updateRoleTask');

		});

		Route::group(['prefix'=>'/comboconstant'],function(){
  			Route::get('create','\Sts\PleafCommon\Controllers\ComboConstantController@create');
  			Route::post('store','\Sts\PleafCommon\Controllers\ComboConstantController@store');
  			Route::post('update','\Sts\PleafCommon\Controllers\ComboConstantController@update');
  			Route::get('edit/{id}/{code}','\Sts\PleafCommon\Controllers\ComboConstantController@edit');
 			Route::get('/delete/{id}/{code}','\Sts\PleafCommon\Controllers\ComboConstantController@destroy');
  			Route::get('search', '\Sts\PleafCommon\Controllers\ComboConstantController@search');
  			Route::get('/','\Sts\PleafCommon\Controllers\ComboConstantController@index');
		});

        Route::group(['prefix'=>'systemconfig'],function(){
	        Route::get('/','\Sts\PleafCommon\Controllers\SystemConfigController@index');
	        Route::get('/edit/{id}', '\Sts\PleafCommon\Controllers\SystemConfigController@editSystemConfig');
	        Route::post('/update', '\Sts\PleafCommon\Controllers\SystemConfigController@updateSystemConfig');
        });

        Route::group(['prefix'=>'groupuser'],function(){
            Route::get('/','\Sts\PleafCommon\Controllers\GroupUserController@index');
            Route::get('/api/list','\Sts\PleafCommon\Controllers\GroupUserController@getGroupUserList');
			Route::get('/add','\Sts\PleafCommon\Controllers\GroupUserController@addGroupUser');
			Route::post('/save','\Sts\PleafCommon\Controllers\GroupUserController@store');
			Route::get('/edit/{id}','\Sts\PleafCommon\Controllers\GroupUserController@editGroupUser');
			Route::post('/update','\Sts\PleafCommon\Controllers\GroupUserController@updateGroupUser');
			Route::get('/delete/{id}','\Sts\PleafCommon\Controllers\GroupUserController@deleteGroupUser');
			Route::get('/viewMember/{id}','\Sts\PleafCommon\Controllers\GroupUserController@viewEditMember');
			Route::get('/editMember/{id}','\Sts\PleafCommon\Controllers\GroupUserController@getDataForEditMemberGroupUser');
			Route::post('/deleteMember','\Sts\PleafCommon\Controllers\GroupUserController@deleteMemberFromGroupUser');
			Route::post('/joinMember','\Sts\PleafCommon\Controllers\GroupUserController@joinMember');

        });

		Route::get('/dbquery', '\Sts\PleafCommon\Controllers\DBQueryController@index');
		Route::post('/api/dbquery/list', '\Sts\PleafCommon\Controllers\DBQueryController@query');

	});
});




