<?php

return [
    "page_title" => "",
    "app_name" => "",



    "app_login_view" => "",

    "routes_redirect_login_success" => "",
    "routes_redirect_login_success_by_role" => [],

    // Login page settings
    "app_logo" => "logo.png",




    "app_version" => "v1.0",
    "app_copyright_text" => "Copyright &copy; 2016",

    "app_redirect_success_change_tenant" => "",
    "app_redirect_back_manage_tenant" => "",
	"app_link_change_tenant"        => "/admin/tenant/do-change-tenant"

];