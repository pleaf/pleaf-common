var app = angular.module("manageUserRoleApp",['kendo.directives'], ['$interpolateProvider', function($interpolateProvider){
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
}]);

app.controller('manageUserRoleCtrl',
    ['$scope','$http', function($scope, $http){

        $scope.user_id = $('#user_id').val();
        $scope.role_name = '';
        
        $scope.showManageUserRole = function () {
            var data = {
                user_id:$scope.user_id,
                role_name: $scope.role_name
            };

            return $http.post('/admin/api/manage-user-role/list',data)
                .then(function (response) {

                    var dataList = response.data;
                    var gridOptions = {
                        sortable:true,
                        pageable:true,
                        dataSource: {
                            data : dataList,
                            schema: {
                                data: "list"
                            },
                            pageSize:50
                        },
                        height:500,
                        sortable:true,
                        selectable:"row",
                        schema: {
                            model: {
                                role_name: {type: "string"},
                                policy_name: {type: "string"}
                            }
                        },
                        serverPaging: true,
                        serverFilter: true,
                        filterable: {
                            model:"row"
                        },
                        pageable: true,
                        columns:
                            [
                                {template :"<a href='javascript:;' onclick=removeUserRole(this)><i" +
                    " class='fa fa-trash'> </i></a>",
                                    title:"Action",
                                    width: 100},
                                {
                                    field: "role_name",
                                    title: "Role Name",
                                    width: 100,
                                    filterable: {
                                    }
                                },
                                {
                                    field: "policy_name",
                                    title: "Policy",
                                    width: 100,
                                    filterable: {
                                    }
                                }]
                    };

                    $('#grid').kendoGrid(gridOptions);
                });
        };

        $scope.showManageUserRole();

}]);

function removeUserRole (data){

    var data = $("#grid").data("kendoGrid").dataItem($(data).closest("tr"));
    console.log(data.user_role_id);
    window.location.href='/admin/user/removeUserRole/'+data.user_role_id+'/'+data.user_id;

}




