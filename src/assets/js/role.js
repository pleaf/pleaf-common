var roleApp = angular.module("roleApp",['kendo.directives'], ['$interpolateProvider',
    function($interpolateProvider){
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
    }]);

roleApp.controller("roleCtrl" , ['$scope','$http',function($scope,$http){

    $http.get('/admin/role/list').
        then(function(response){

            var dataList = response.data;

            var gridOptions = {
                sortable: true,
                pageable: true,
                dataSource: {
                    data: dataList,
                    schema: {
                        //model: { role_id : "role_id" },
                        data: "list",
                        total: function(){ return response.data.list.length; }
                    },
                    pageSize: 50
                },
                height: 500,
                sortable: true,
                selectable: "row",
                schema: {
                    model: {
                        fields: {
                            role_name: { type: "string" },
                            role_type: { type: "string" },
                            detail   : {type: "number"}
                        }
                    }
                },
                serverPaging: true,
                serverFiltering: true,
                filterable: {
                    mode: "row"
                },
                columns: [
                    {
                        template: '' +
                            '<a class="task task-editRole" href="javascript:;" onclick="onEdit(this)"> <i class="fa fa-pencil"></i></a> ' +
                            '&nbsp; &nbsp;'
                            +'<a href="javascript:;" class="task task-removeRole" onclick="onRemove(this)"> <i class="fa fa-trash"></i></a>' +
                            '&nbsp; &nbsp;'
                            +'<a href="javascript:;" onclick="managerRoleTask(this)"> Manage Role Task</a>',
                        title: "Action"
                    },
                    {
                        field: "role_name",
                        title: "Role Name"
                    },
                    {
                        field: "role_type",
                        title: "Role Type"
                    },
                    {
                        field: "detail",
                        title: "Detail"
                    }
                ]
            }
            $('#grid').kendoGrid(gridOptions);
        });
}]);

function onRemove(role){
    var confirmRemove = confirm('Are you sure this remove role ?');

    if(confirmRemove){
        var url = window.location.origin;
        var item = $("#grid").data("kendoGrid").dataItem($(role).closest("tr"));
        window.location.href = url+'/admin/role/delete/'+item.role_id;
    }
}

function onEdit(role){
    var url = window.location.origin;
    var item = $("#grid").data("kendoGrid").dataItem($(role).closest("tr"));
    window.location.href = url+'/admin/role/edit/'+item.role_id;
}

function managerRoleTask(role){
    var url = window.location.origin;
    var item = $('#grid').data("kendoGrid").dataItem($(role).closest("tr"));
    window.location.href = url+'/admin/role/role-task/'+item.role_id;
}