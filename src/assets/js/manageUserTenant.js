(function() {

    angular.module('manageUserTenant', ['checklist-model'])
        .controller('ManageUserTenantController', ManageUserTenantController);

    ManageUserTenantController.$inject = ['$http', '$scope', '$rootScope'];

    function ManageUserTenantController($http, $scope, $rootScope) {

        var ctrl = this;

        $scope.checkAll = checkAll;
        $scope.doAddManageTenantUser = doAddManageTenantUser;
        $scope.doClearManageTenantUser = doClearManageTenantUser;

        ctrl.tenantSelected = [];
        $scope.manage_user_tenant_list = [];

        var user_id = document.getElementById("user_id").value;

        getPolicyTenantUserList();

        function checkAll(condition) {
            if(condition){
                if(ctrl.tenantSelected.length == 0) {
                    ctrl.tenantSelected = angular.copy($scope.manage_user_tenant_list);
                } else {
                    if(ctrl.tenantSelected.length >= 0 && condition) {
                        ctrl.tenantSelected = angular.copy($scope.manage_user_tenant_list);
                    } else {
                        ctrl.tenantSelected = [];
                    }
                }
            } else {
                ctrl.tenantSelected = [];
            }

        }

        function getPolicyTenantUserList() {

            $http.get("/admin/tenant/policy-user-tenant/"+ user_id)
                .then(function(response) {
                    $scope.manage_user_tenant_list = response.data.result.list.manage_user_tenant_list;
                    $scope.policy_tenant_list = response.data.result.list.policy_tenant_list;
                }).catch(function(response) {

                });

        }

        function doAddManageTenantUser() {

            var payload = {
                service: "addPolicyTenantUser",
                taskName: "system",
                payload: {
                    user_id: user_id,
                    tenant_list: ctrl.tenantSelected
                }
            };

            if(ctrl.tenantSelected.length > 0) {
                $http.post("/api/service", payload)
                    .then(function(response) {
                        if(response.data.result.status == "OK") {
                            ctrl.tenantSelected = [];
                            getPolicyTenantUserList()
                        }
                    }).catch(function(response) {

                    });
            }

        }

        function doClearManageTenantUser(item) {
            var payload = {
                service: "removePolicyTenantUser",
                taskName: "system",
                payload: {
                    policy_tenant_list: [item]
                }
            };

            $http.post("/api/service", payload)
                .then(function(response) {
                    if(response.data.result.status == "OK") {
                        getPolicyTenantUserList()
                    }
                }).catch(function(response) {

                });
        }


    }

})();