var app = angular.module("roletaskApp",[], ['$interpolateProvider', function($interpolateProvider){
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
}]);

// function that gathers IDs of checked nodes
function checkedNodeIds(nodes, checkedNodes) {
    for (var i = 0; i < nodes.length; i++) {
        if (nodes[i].checked) {
            checkedNodes.push(nodes[i].id);
        }

        if (nodes[i].hasChildren) {
            checkedNodeIds(nodes[i].children.view(), checkedNodes);
        }
    }
}

app.controller('roletaskCtrl',
    ['$scope','$http', function($scope, $http) {

    // Get list of Role Task by Role ID
    $scope.currentRoleId = $('#role_id').val();
    $scope.taskGroup = '';
    $scope.onCheck = function(e){
        e.preventDefault();
        var tree = $('#treeview').data('kendoTreeView');
        var dataItem = tree.dataItem(e.node);
        console.log(dataItem);
        console.log("Role ID: " + $scope.currentRoleId);
        console.log("Task ID: " + dataItem.id);
        console.log("Checked: " + dataItem.checked);

        $http.post('/admin/role/show-role-task/update',{
            role_id: $scope.currentRoleId,
            task_id: dataItem.id,
            checked: dataItem.checked
        }).then(function(response){
            
        }, function(error){
            alert(error);
        });
    };

    $scope.showRoleTask = function(){
        $http.post('/admin/role/show-role-task',{
                role_id: $scope.currentRoleId,
                task_group: $scope.taskGroup
            })
            .then(function(response){
                $scope.list = response.data.result.list;

                var data = [];
                angular.forEach($scope.list, function(value,key){
                   data.push({
                       id: value.task_id,
                       text: value.task_code,
                       checked: (value.role_id!=undefined)
                   });
                });

                $("#treeview").kendoTreeView({
                    checkboxes: {
                        checkChildren: true
                    },

                    check: $scope.onCheck,
                    dataSource: data
                });

            }, function(error){
                console.log(error);
            });
    }

    $scope.showRoleTask();
}]);