var app = angular.module("comboconstantApp",[], ['$interpolateProvider', function($interpolateProvider){
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
}]);

app.controller('comboconstantCtrl',
    ['$scope','$http', function($scope, $http){

        $http.get('/admin/api/comboconstant/list').
            then(function(response){

            var dataList = response.data.list
            var gridOptions = {
                sortable: true,
                pageable: true,
                dataSource: {
                    data: dataList,
                    schema: {
                        total: function(){ return response.data.total; }
                    },
                    pageSize: 50
                },
                height: 500,
                sortable: true,
                selectable: "row",
                schema: {
                          model: {
                            fields: {
                                  combo_id: { type: "string" },
                                  code: { type: "string" },
                                  prop_key: { type: "string" },
                                  code_group: { type: "string" },
                                  sort_no: { type: "number" }
                            }
                          }
                        },
                serverPaging: true,
                serverFiltering: true,
                filterable: {
                                mode: "row"
                            },
                columns: [
                   {
                    template: '' +
                            '<a class="task task-editComboValue" href="javascript:;" onclick="onEdit(this)"> <i class="fa fa-pencil"></i></a> ' +
                            '&nbsp; &nbsp;'
                            +'<a class="task task-removeComboValue" href="javascript:;" onclick="onRemove(this)"> <i class="fa fa-trash"></i></a>',
                    title: "Action"
                    },
                    {
                        field: "combo_id",
                        title: "Combo Id"
                    },
                    {
                        field: "code",
                        title: "Combo Code"
                    },
                    {
                        field: "prop_key",
                        title: "Property Key"
                    },
                    {
                        field: "code_group",
                        title: "Code Group"
                    },
                    {
                        field: "sort_no",
                        title: "Sort No"
                    }

                ]
            };

            var data = [];
            var i;

            for(i=0; i<dataList.length; i++) {
              if(data.indexOf(dataList[i].combo_id) < 0){
                 data.push(dataList[i].combo_id);
              }
            }


            $('#grid').kendoGrid(gridOptions);
            $("#combo_id").kendoAutoComplete({
                        dataSource: data
                    });

         });

    }]);

function onRemove(comboconstant){
    var confirmRemove = confirm('Are you sure this remove combo constant ?');

    if(confirmRemove){
      var url = window.location.origin;
      var item = $("#grid").data("kendoGrid").dataItem($(comboconstant).closest("tr"));
      window.location.href = url+'/admin/comboconstant/delete/'+item.combo_id+'/'+item.code;
    }

}

function onEdit(comboconstant){
    var url = window.location.origin;
    var item = $("#grid").data("kendoGrid").dataItem($(comboconstant).closest("tr"));
    window.location.href = url+'/admin/comboconstant/edit/'+item.combo_id+'/'+item.code;
}
