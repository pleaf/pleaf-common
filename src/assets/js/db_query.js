var app = angular.module("DBQueryApp",[], ['$interpolateProvider', function($interpolateProvider){
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
}]);

app.controller('DBQueryCtrl',
    ['$scope','$http', function($scope, $http){

    	$scope.onSubmit = function() {

    	var select = '';

    	if (!angular.isUndefined($scope.query)) {
    		select = $scope.query.replace(/\s/g, '').substr(0, 6);
    	}

    	if (select == '') {
			$scope.showMessage = true;
    		$scope.messageType = 'warning';
    		$scope.errorMessage = 'Silahkan masukan query anda';
    		return;
    	} else if (select.toLowerCase() == 'delete' || select.toLowerCase() == 'insert' ||
    		select.toLowerCase() == 'update') {
    		$scope.showMessage = true;
    		$scope.messageType = 'error';
    		$scope.errorMessage = 'Hanya bisa melakukan select table';
    		return;
    	} else if (select.toLowerCase() != 'select' && !angular.isUndefined($scope.query)) {
    		$scope.showMessage = true;
    		$scope.messageType = 'error';
    		$scope.errorMessage = 'Illegal syntax sql';
    		return;
    	} else {
    		$scope.showMessage = false;
    	}

        var req = {
			method: 'POST',
			url: '/admin/api/dbquery/list',
			headers: {
			  'Content-Type': 'application/json'
			},
			data: { query: $scope.query }
		}

		$http(req).then(
			function (response) {

				if (response.data.result.status == 'OK') {
					var columns = [];
					if (response.data.result.list.length > 0) {
					
						var keys = Object.keys(response.data.result.list[0]);
						var i;

						for(i=0; i<keys.length; i++) {
							columns.push(
								{
									field: keys[i],
				                    title: keys[i],
				                    width:"150px"	
								}
							)
						}
					

						var dataSource = new kendo.data.DataSource({
							data: response.data.result.list,
					        pageSize: 10,
					    });


				        $('#grid').kendoGrid({
				            pageable: true,
				            dataSource: dataSource,
				            height: 350,
				            selectable: "row",
				            columns: columns
				        });

						$('#grid').show();
				        var grid = $('#grid').data('kendoGrid');
				        grid.setOptions({
				            columns: columns
				        });
				        grid.refresh();

			   		} else {
			   			$('#grid').hide();
	    				$scope.messageType = 'success';
						$scope.showMessage = true;
		    			$scope.errorMessage = 'Tidak ada data di temukan';
			   		}
			   	} else {
		    		$scope.showMessage = true;
		    		$scope.messageType = 'warning';
		    		$scope.errorMessage = 'Silahkan masukan query anda';
		    		return;
		    	}
			},
			function (error) {
				$('#grid').hide();
    			$scope.messageType = 'warning';
				$scope.showMessage = true;
    			$scope.errorMessage = 'Terjadi masalah di internal server atau table tidak di temukan';
			}

		)
		}

		$scope.clearQuery = function() {
			$scope.query = "";
			$('#grid').hide();
    		$scope.showMessage = false;
			document.getElementById("query").focus();
		}
}]);