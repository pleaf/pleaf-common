var app = angular.module("systemConfigApp",['kendo.directives'], ['$interpolateProvider', function($interpolateProvider){
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
}]);

app.controller('systemConfigCtrl',
    ['$scope','$http', function($scope, $http){

        $http.get('/admin/api/systemconfig/list').
            then(function(response){

                var gridOptions = {
                    sortable: true,
                    pageable: {
                        input: true,
                        numeric: false
                    },
                    dataSource: {
                        data: response.data.list,
                        schema: {
                            total: function(){ return response.data.total; }
                        },
                        pageSize: 50
                    },
                    height: 350,
                    sortable: true,
                    selectable: "row",
                    columns: [
                        {
                            template: "<a href='\\#' onclick='goToEdit(this)'><i class='fa fa-pencil'></i></a>",
                            title:"Action",
                            width:75
                        },
                        {
                            field: "parameter_code",
                            title: "Name"
                        }, {
                            field: "parameter_desc",
                            title: "Description"
                        },{
                            field: "parameter_value",
                            title: "Value"
                        }
                    ]
                };

                $('#gridOptions').kendoGrid(gridOptions);

        });


    }]);

function goToEdit (data){

    var data = $("#gridOptions").data("kendoGrid").dataItem($(data).closest("tr"));
    console.log("item", data);
    window.location.href='/admin/systemconfig/edit/'+data.parameter_id;

};
