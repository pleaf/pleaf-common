var app = angular.module("editMemberGroupUserApp",[], ['$interpolateProvider', function($interpolateProvider){
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
}]);

app.controller('editMemberGroupUserCtrl',
    ['$scope','$http', function($scope, $http){
        
        $scope.getId = function(id){

            var getId = id;
            var api = '/admin/groupuser/editMember/'+getId;


            $http.get(api).
                then(function(response){
                    console.log(response);
                    var gridOptions = {
                        sortable: true,
                        pageable: true,
                        dataBound: onDataBoundMember,
                        dataSource: {
                            data: response.data.members,
                            schema: {
                                total: function(){ return response.data.total; },
                                model:{
                                    id:"user_id"
                                }
                            },
                            pageSize: 100
                        },
                        selectable: "row",
                        sortable: true,
                        height: 350,
                        schema: {
                          model: {
                            fields: {
                                  username: { type: "string" },
                                  fullname: { type: "string" }
                            }
                          }
                        },
                        filterable: {
                            mode: "row"
                        },
                        serverPaging: true,
                        serverFiltering: true,
                        columns: [
                            {template: "<input value='true' type='checkbox' class='checkbox' onchange='handleCheckBoxMember(this)' />",
                                width:20
                            },
                            {
                                field: "fullname",
                                title: "Full Name"
                            }, {
                                field: "username",
                                title: "User Name"
                            }
                        ]
                    };

                    $scope.showMessage = false;
                    console.log($scope.showMessage);
                    var grid = $('#gridOptions').kendoGrid(gridOptions).data("kendoGrid");
                    grid.table.on("click", ".checkbox" , selectRow);

                    var param = {
                        "groupUserId":getId,
                        "userId":checkedIdsMember.user_id
                    }
                    console.log(param);

                    $("#showSelection").bind("click", function () {
                        var checked = [];
                        angular.forEach(checkedIdsMember.user_id,function(item){
                            console.log(item);
                            checked.push(item);
                        });
                        console.log(param);
                        $http.post('/admin/groupuser/deleteMember',param)
                            .then(function(response){
                                console.log(response);
                                if(response.data.status=="OK"){
                                    window.location.href='/admin/groupuser/viewMember/'+getId;
                                    $scope.showMessage = true;
                                    $scope.messageType = 'success';
                            		$scope.message = response.data.message;

                                }else{
                                    $scope.showMessage = true;
                                    $scope.messageType = 'error';
                                    angular.forEach(response.data.errors,function(item){
                                        $scope.message = item.message;
                                    });

                                }

                            },
                            function(error){
                                console.log(error);
                            });


                    });

                });

                $http.get(api).
                    then(function(response){
                        console.log(response);
                        var gridOptions2 = {
                            sortable: true,
                            pageable: true,
                            dataBound: onDataBoundUser,
                            dataSource: {
                                data: response.data.users,
                                schema: {
                                    total: function(){ return response.data.total; },
                                    model:{
                                        id:"user_id"
                                    }
                                },
                                pageSize: 100
                            },
                            selectable: "row",
                            sortable: true,
                            height: 350,
                            schema: {
                              model: {
                                fields: {
                                      username: { type: "string" },
                                      fullname: { type: "string" }
                                }

                              }
                            },
                            filterable: {
                                mode: "row"
                            },
                            serverPaging: true,
                            serverFiltering: true,
                            columns: [
                                {template: "<input value='true' type='checkbox' class='checkbox' onchange='handleCheckBoxUser(this)' />"
                                    ,width:20
                                },
                                {
                                    field: "fullname",
                                    title: "Full Name"
                                }, {
                                    field: "username",
                                    title: "User Name"
                                }                    ]
                        };

                        var param = {
                            "groupUserId":getId,
                            "userId":checkedIdsUser.user_id
                        }
                        console.log(param);

                        var grid2 = $('#gridOptions2').kendoGrid(gridOptions2).data("kendoGrid");
                        grid2.table.on("click", ".checkbox" , selectRow2);

                        $("#showSelection2").bind("click", function () {
                            var checked = [];
                            angular.forEach(checkedIdsUser.user_id,function(item){
                                console.log(item);
                                checked.push(item);
                            });
                            console.log(param);
                            $http.post('http://localhost:8000/admin/groupuser/joinMember',param)
                                .then(function(response){
                                    console.log(response);
                                    if(response.data.status=="OK"){
                                        window.location.href='/admin/groupuser/viewMember/'+getId;
                                        $scope.showMessage = true;
                                        $scope.messageType = 'success';
                                		$scope.message = response.data.message;

                                    }else{
                                        $scope.showMessage = true;
                                        $scope.messageType = 'error';
                                        angular.forEach(response.data.errors,function(item){
                                            $scope.message = item.message;
                                        });

                                    }
                                })
                                .catch(function(error){
                                    console.log(error);
                                });
                        });
                    });
                }

    }]);

function goToEdit (data){

    var data = $("#gridOptions").data("kendoGrid").dataItem($(data).closest("tr"));
    console.log("item", data);
    window.location.href='/admin/groupuser/edit/'+data.group_id;
    console.log('Edit Group USer');

};


function deleteGroupUser (data){

    if(confirm('are you sure?')){
        var data = $("#gridOptions").data("kendoGrid").dataItem($(data).closest("tr"));
        console.log("item", data);
        window.location.href='/admin/groupuser/delete/'+data.group_id;
        console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');

    }

};

function goToEditMember (data){


    var data = $("#gridOptions").data("kendoGrid").dataItem($(data).closest("tr"));
    console.log("item", data);
    window.location.href='/admin/groupuser/editMember/'+data.group_id;
    console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');



};

var checkedIdsUser = {
    "user_id":[]
};

var checkedIdsMember = {
    "user_id":[]
};


function selectRow() {
    var checked = this.checked,
    row = $(this).closest("tr"),
    grid = $("#gridOptions").data("kendoGrid"),
    dataItem = grid.dataItem(row);


    if (checked) {
        //-select the row
        row.addClass("k-state-selected");
        } else {
        //-remove selection
        row.removeClass("k-state-selected");
    }
}

function handleCheckBoxMember(data){
    if(data.checked == true){

     var grid = $("#gridOptions").data("kendoGrid").dataItem($(data).closest("tr"));
     checkedIdsMember.user_id.push(grid.id);

    //  console.log(checkedIds.user_id);

     console.log(true);
   } else {
       Array.prototype.remove = function() {
            var what, a = arguments, L = a.length, ax;
            while (L && this.length) {
                what = a[--L];
                while ((ax = this.indexOf(what)) !== -1) {
                    this.splice(ax, 1);
                }
            }
            return this;
        };

       var grid = $("#gridOptions").data("kendoGrid").dataItem($(data).closest("tr"));
       checkedIdsMember.user_id.remove(grid.id);


     console.log(false);
   }

}

function handleCheckBoxUser(data){
    if(data.checked == true){

     var grid = $("#gridOptions2").data("kendoGrid").dataItem($(data).closest("tr"));
     checkedIdsUser.user_id.push(grid.id);



     console.log(true);
   } else {
       Array.prototype.remove = function() {
            var what, a = arguments, L = a.length, ax;
            while (L && this.length) {
                what = a[--L];
                while ((ax = this.indexOf(what)) !== -1) {
                    this.splice(ax, 1);
                }
            }
            return this;
        };

       var grid = $("#gridOptions2").data("kendoGrid").dataItem($(data).closest("tr"));
       checkedIdsUser.user_id.remove(grid.id);


     console.log(false);
   }

}


function selectRow2() {
    var checked = this.checked,
    row = $(this).closest("tr"),
    grid = $("#gridOptions2").data("kendoGrid"),
    dataItem = grid.dataItem(row);

    // checkedIdsUser.user_id.push(dataItem.id);
    if (checked) {
        //-select the row
        row.addClass("k-state-selected");
        } else {
        //-remove selection
        row.removeClass("k-state-selected");
    }
}

function onDataBoundUser(e) {
    var view = this.dataSource.view();
    for(var i = 0; i < view.length;i++){
        if(checkedIdsUser[view[i].id]){
            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
            .addClass("k-state-selected")
            .find(".checkbox")
            .attr("checked","checked");
        }
    }
}


function onDataBoundMember(e) {
    var view = this.dataSource.view();
    for(var i = 0; i < view.length;i++){
        if(checkedIdsUser[view[i].id]){
            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
            .addClass("k-state-selected")
            .find(".checkbox")
            .attr("checked","checked");
        }
    }
}
