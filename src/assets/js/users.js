var app = angular.module("usersApp",[], ['$interpolateProvider', function($interpolateProvider){
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
}]);

app.controller('usersCtrl',
    ['$scope','$http', function($scope, $http){

        //var data = $http.get('/admin/api/users/list').then(function (){
        //
        //});


        $http.get('/admin/api/users/list').then(function(response){

            var dataSource = new kendo.data.DataSource({
                transport: {
                    read:  {
                        url: "/admin/api/users/list",
                        dataType: "json"
                    },
                    update: {
                        dataType: "json",
                        // url: "/admin/user/update",
                        // type:"put",

                        data:function(e){
                            console.log(e);

                            angular.forEach(e.models,function(item){
                                console.log(item);
                                console.log(item.ou_id.ou_id);

                                var data = {
                                    user_id:item.user_id,
                                    username:item.username,
                                    email:item.email,
                                    fullname:item.fullname,
                                    phone:item.phone,
                                    role_default_id:item.role_id,
                                    ou_default_id:item.ou_id,
                                    policy_default_id:item.policy_id,
                                    active:item.active

                                };

                                console.log(data);

                                $http.post('/admin/user/update',data).then(function(response){
                                    console.log(response);
                                    // $('span.k-i-close').click();
                                    location.reload(true);

                                });
                            });


                        },
                        success:function(options){
                            console.log(options);
                        }
                    },
                    parameterMap: function(options, operation) {
                        if (operation !== "read" && options.models) {
                            return {models: kendo.stringify(options.models)};
                        }
                    }
                },
                batch: true,
                pageSize: 20,
                schema: {
                    data:'list',
                    total: function(){return response.data.list.length},
                    model: {
                        id: "user_id",
                        fields: {
                            username: { nullable: true },
                            email: { type:'email', validation: { required: true } },
                            fullname: { type: "string", validation: { required: true} },
                            role_name: { type: "string",validation: { required: true } },
                            ou_name: { type: "string", validation: { required: true } },
                            policy_name: {type: "string",validation: {  required: true } },
                            active: {type:"text", nullable: false},
                            phone:{type:'text'}

                        }
                    }
                }
            });

            $("#grid").kendoGrid({
                dataSource: dataSource,
                height: 550,
                filterable: {
                    mode: "row"
                },
                pageable:true,
                selectable: "row",
                columns: [
                    { command: [
                        {template:"<a class='task task-editUser k-grid-edit'><i class='fa fa-pencil'></i>&nbsp; &nbsp;</a>"},
                        {template:"<a class='task task-removeUser' href= 'javascript:;' onclick='onRemove(this)'> <i class='fa fa-trash'></i>&nbsp; &nbsp;</a>"},
                        {template:"<a href='javascript:;' onclick='manageUser(this)'>Manage</a>"}
                    ],
                        title: "Action", width: "120px" },
                    { field:"username", title: "Username" },
                    // { field: "email", title:"Email", width: "220px" },
                    { field: "fullname", title:"Fullname", width: "120px" },
                    { field: "role_name", title:'Role Name', template: "#: role_name,role_default_id #" },
                    { field: "ou_name", title:'Default OU',template: "#: ou_name,ou_id #" },
                    { field: "policy_name", title:'Default Policy',template: "#: policy_name,policy_default_id #" }
                ],
                editable: {
                    mode:'popup',
                    template:$("#popupTemplate").html(),
                    update:true
                },
                edit:function(e){
                    initDropDownLists();
                    // e.container.find(".k-button.k-grid-update").hide()
                    //     .append('<a class="k-button k-button-icontext k-grid-delete" href="#"><span class="k-icon k-delete"></span>Delete</a>'); //update button
                    // e.container.find(".k-button.k-grid-cancel").hide();
                },
                autoBind:true
            });


        });

        function initDropDownLists() {

            var role = $("#Role").kendoDropDownList({
                optionLabel: "Select role...",
                dataTextField: "role_name",
                dataValueField: "role_id",
                dataSource: {
                    type: "json",
                    serverFiltering: true,
                    transport: {
                        read: "/admin/role/list"
                    },
                    schema:{
                        data:'list'
                    }
                }
            }).data("kendoDropDownList");

            var ou = $("#OU").kendoDropDownList({
                optionLabel: "Select Default Ou..",
                dataTextField: "ou_name",
                dataValueField: "ou_id",
                dataSource: {
                    type: "json",
                    serverFiltering: true,
                    transport: {
                        read: "/admin/api/ou/list"
                    }

                }
            }).data("kendoDropDownList");

            var policy = $("#Policy").kendoDropDownList({
                optionLabel: "Select Default Policy..",
                dataTextField: "policy_name",
                dataValueField: "policy_id",
                dataSource: {
                    type: "json",
                    serverFiltering: true,
                    transport: {
                        read: "/admin/api/policy/list"
                    }
                }
            }).data("kendoDropDownList");
        }



    }]);

function onRemove(user){
    var confirmRemove = confirm('Are you sure this remove role ?');
    if(confirmRemove){
        var url = window.location.origin;
        var item = $("#grid").data("kendoGrid").dataItem($(user).closest("tr"));
        window.location.href = url+'/admin/user/delete/'+item.user_id;
    }
}

function onEdit(user){
    var url = window.location.origin;
    var item = $("#grid").data("kendoGrid").dataItem($(user).closest("tr"));
    window.location.href = url+'/admin/user/edit/'+item.user_id;
}

function manageUser(user){

    var url = window.location.origin;
    var item = $("#grid").data("kendoGrid").dataItem($(user).closest("tr"));
    window.location.href = url+'/admin/user/manage-user/'+item.user_id;

}