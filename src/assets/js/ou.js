var app = angular.module("ouApp",['kendo.directives'], ['$interpolateProvider', function($interpolateProvider){
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
}]);

app.controller('ouCtrl',
    ['$scope','$http', function($scope, $http){

    	 $(document).ready(function() {
             $("#gridOptions").kendoGrid({
                 dataSource: {
                     type: "json",
                     transport: {
                         read: "/admin/api/ou/list"
                     },
                     pageSize: 50
                     
                 },
                 serverPaging: true,
                 serverFiltering: true,
                 schema: {
                     model: {
                       fields: {                          
                             ou_code: { type: "string" },
                             ou_type_name: { type: "string" },
                             ou_name: { type: "string" },
                             ou_parent_name: { type: "string" },
                       }

                     }
                 },
                 height: 550,
                 filterable: {
                     mode: "row"
                 },
                 pageable: true,
                 columns: 
                 [
					 {template :"" +
                     "<a class='task task-editOu' href='javascript:;' onclick='editOu(this)'><i class='fa fa-pencil'></i> </a> &nbsp; &nbsp; " +
                     "<a class='task task-removeOu' href='javascript:;' onclick='deleteOu(this)'> <i class='fa fa-trash'></i></a>",
					  title:"Action", 
					  width: 100},
					
                  {
                     field: "ou_code",
                     title: "OU Code",
                     width: 100,
                     filterable: {
                         
                     }
                 },
                 {
                     field: "ou_name",
                     title: "OU Name",
                     width: 100,
                     filterable: {
                         
                     }
                 },{
                     field: "ou_type_name",
                     title: "OU Type",
                     width: 100,
                     filterable: {
                         
                     }
                 },{
                 	field: "ou_parent_name",
                     title: "OU Parent",
                     width: 100,
                     filterable: {
                         
                     }
                 }]
             });
         });
         
}]);

function editOu(editId) {

    var ouEditId = $("#gridOptions").data("kendoGrid").dataItem($(editId).closest("tr"));
    window.location.href='/admin/ou/edit/'+ouEditId.ou_id;

}
function deleteOu(deleteId) {

    var ok = confirm('Are You Sure ?');
    if(ok == true){
        var ouDeleteId = $("#gridOptions").data("kendoGrid").dataItem($(deleteId).closest("tr"));
        window.location.href='/admin/ou/destroy/'+ouDeleteId.ou_id;
    }

}
