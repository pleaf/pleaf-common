var app = angular.module("groupUserApp",[], ['$interpolateProvider', function($interpolateProvider){
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
}]);

app.controller('groupUserCtrl',
    ['$scope','$http', function($scope, $http){

        $http.get('/admin/groupuser/api/list').
            then(function(response){
                var gridOptions = {
                    sortable: true,
                    pageable: true,
                    dataSource: {
                        data: response.data.list,
                        schema: {
                            total: function(){ return response.data.total; }
                        },
                        pageSize: 50
                    },
                    selectable: "row",
                    sortable: true,
                    height: 350,
                    schema: {
                      model: {
                        fields: {
                              group_user_code: { type: "string" },
                              group_user_name: { type: "string" },
                              group_user_desc: { type: "string" },
                              total: { type: "string" }
                        }
                      }
                    },
                    filterable: {
                        mode: "row"
                    },
                    serverPaging: true,
                    serverFiltering: true,
                    columns: [
                        {
                            template: "<a class='task task-editGroupUser' onclick='goToEdit(this)'><i class='fa fa-pencil'>"+
                                    "</i>&nbsp &nbsp</a><a class='task task-removeGroupUser'" +
                            " onclick='deleteGroupUser(this)'><i class='fa fa-trash'>"+
                                    "</i>&nbsp &nbsp</a><a class=''" +
                            " onclick='goToEditMember(this)'>Edit Member</a>",
                            title: "Action"
                        },{
                            field: "group_user_code",
                            title: "Code"
                        }, {
                            field: "group_user_name",
                            title: "Name"
                        },{
                            field: "group_user_desc",
                            title: "Value"
                        },{
                            field: "total",
                            title:"Total User"
                        }
                    ]
                };

                var datas = $('#gridOptions').kendoGrid(gridOptions);

            });

            $('#gridOptions').kendoGrid();
    }]);

function goToEdit (data){

    var data = $("#gridOptions").data("kendoGrid").dataItem($(data).closest("tr"));
    console.log("item", data);
    window.location.href='/admin/groupuser/edit/'+data.group_id;
    console.log('Edit Group USer');


};


function deleteGroupUser (data){

    if(confirm('are you sure?')){
        var data = $("#gridOptions").data("kendoGrid").dataItem($(data).closest("tr"));
        console.log("item", data);
        window.location.href='/admin/groupuser/delete/'+data.group_id;
        console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');

    }

};

function goToEditMember (data){


    var data = $("#gridOptions").data("kendoGrid").dataItem($(data).closest("tr"));
    console.log("item", data);
    window.location.href='/admin/groupuser/viewMember/'+data.group_id;
    console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');



};