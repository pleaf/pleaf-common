var app = angular.module("tenantApp",[], ['$interpolateProvider', function($interpolateProvider){
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
}]);

app.controller('tenantCtrl',
    ['$scope','$http', function($scope, $http){

        $http.get('/admin/api/tenant/list').
            then(function(response){

            var list = [];
            var total = 0;

            if (response.data.result.status == "OK") {
                list = response.data.result.list;
                total = response.data.result.total
            }

            var gridOptions = {
                sortable: true,
                pageable: {
                    input: true,
                    numeric: false
                },
                dataSource: {
                    data: list,
                    schema: {
                        total: function(){ return total; }
                    },
                    pageSize: 50
                },

                sortable: true,
                selectable: "row",
                columns: [
                    {
                        template : "<a class='task task-editTenant' onclick='editTenant(this)'><i class='fa fa-pencil'></i> </a>",
                        title: "Action"
                    },
                    {
                        field: "tenant_code",
                        title: "Tenant Code"
                    }, {
                        field: "tenant_name",
                        title: "Tenant Name"
                    }, {
                        field: "description",
                        title: "Description"
                    }, {
                        field: "email",
                        title: "Email"
                    }, {
                        field: "host",
                        title: "Host"
                    }
                ]
            };

            $('#grid').kendoGrid(gridOptions);

        });


    }]);

function editTenant(editId) {                 
    var tenant = $("#grid").data("kendoGrid").dataItem($(editId).closest("tr"));
        window.location.href='/admin/tenant/edit/'+tenant.tenant_id;
}