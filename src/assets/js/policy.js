var app = angular.module("policyApp",['kendo.directives'], ['$interpolateProvider', function($interpolateProvider){
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
}]);

app.controller('policyCtrl',
    ['$scope','$http', function($scope, $http){

   	 $(document).ready(function() {
         $("#gridOptions").kendoGrid({
             dataSource: {
                 type: "json",
                 transport: {
                     read: "/admin/api/policy/list"
                 },
                 pageSize: 50
                 
             },
             serverPaging: true,
             serverFiltering: true,
             schema: {
                 model: {
                   fields: {                          
                         policy_code: { type: "string" },
                         policy_name: { type: "string" },
                         
                   }

                 }
             },
             height: 550,
             filterable: {
                 mode: "row"
             },
             pageable: true,
             columns: 
             [
				 {template :"" +
                 "<a class='task task-editPolicy' href='javascript:;' onclick='policyEdit(this)'><i class='fa fa-pencil'></i> </a> " +
                 "&nbsp; &nbsp; " +
                 "<a class='task task-removePolicy' href='javascript:;' onclick='deletepolicy(this)'> <i class='fa fa-trash'></i></a>",
				  title:"Action", 
				  width: 100},
				
             {
                 field: "policy_code",
                 title: "Policy Code",
                 width: 100,
                 filterable: {
                     
                 }
             },{
                 field: "policy_name",
                 title: "Policy Name",
                 width: 100,
                 filterable: {
                     
                 }             
             }]
         });
     });
        
}]);

function policyEdit(editId) {

    var policyEditId = $("#gridOptions").data("kendoGrid").dataItem($(editId).closest("tr"));
    window.location.href='/admin/policy/edit/'+policyEditId.policy_id;

}
function deletepolicy(deleteId) {

    var ok = confirm('Are You Sure ?');
    if(ok == true){
        var policyDeleteId = $("#gridOptions").data("kendoGrid").dataItem($(deleteId).closest("tr"));
        window.location.href='/admin/policy/delete/'+policyDeleteId.policy_id;
    }

}
