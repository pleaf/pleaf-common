var app = angular.module("ouTypeApp",['kendo.directives'], ['$interpolateProvider', function($interpolateProvider){
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
}]);

app.controller('ouTypeCtrl',
    ['$scope','$http', function($scope, $http){

        var data = new kendo.data.DataSource({
            transport: {
                read: {
                    url: "/admin/api/outype/list",
                    dataType: "json"
                }
            }
        });
        
        $(function () {
        	  $('[data-toggle="tooltip"]').tooltip()
        	})

        $("#gridOptions").kendoGrid({
            dataSource: data,
            sortable: true,
            selectable: "row",
            columns: [
                {template :"" +
                "<a class='task task-editOuType' href='javascript:;' onclick='editOuTypeId(this)'><i class='fa fa-pencil'></i> </a> &nbsp; &nbsp; " +
                "<a class='task task-removeOuType' href='javascript:;' onclick='deleteOuTypeId(this)'> <i class='fa fa-trash'></i></a>", title:"Action"},
                { field: "ou_type_code", title: "OU Type Code" },
                { field: "ou_type_name", title: "OU Type Name" },
            	{ field: "flg_bu", title: "Business Unit" },
            	{ field: "flg_accounting", title: "Acounting" },
            	{ field: "flg_legal", title: "Legal" },
            	{ field: "flg_sub_bu", title: "Sub Business Unit" },
				{ field: "flg_branch", title: "Branch" }
            ]
        });
        

}]);

function editOuTypeId(editId) {

    var ouTypeIdEdit = $("#gridOptions").data("kendoGrid").dataItem($(editId).closest("tr"));
    window.location.href='/admin/outype/edit/'+ouTypeIdEdit.ou_type_id;

}
function deleteOuTypeId(deleteId) {

    var ok = confirm('Are You Sure ?');
    if(ok == true){
        var ouTypeIdDelete = $("#gridOptions").data("kendoGrid").dataItem($(deleteId).closest("tr"));
        window.location.href='/admin/outype/destroy/'+ouTypeIdDelete.ou_type_id;
    }

}