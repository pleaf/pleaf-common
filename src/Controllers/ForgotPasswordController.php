<?php

namespace Sts\PleafCommon\Controllers;;
use App;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Sts\PleafCore\DateUtil;
class ForgotPasswordController extends \App\Http\Controllers\Controller {
	
	public function index(){
        return view('pleaf-common::Forgot.index');
    }

    public function mailReset(Request $request){
        $addForgotPassword = App::make('addForgotPassword');

        $input = [
            "email"=>$request->email,
            "random_key"=>str_random(15)
        ];

        try {   
            $output = $addForgotPassword->execute($input);

        } catch(Exception $ex){
            Log::error($ex->getMessage());
        }

        return redirect('/')->with("successMessage","Link for reset password successfully sent to your email!");



    }

    public function resetindex($email,$key)
    {
//        \Log::debug($this->linkExpired($email,$key)['status']);

        if($this->linkExpired($email,$key)['status']=='NOPE'){
            return View('pleaf-common::Forgot.expired');
        }else{
            return view('pleaf-common::Forgot.form')->with('email',$email)->with('key',$key);
        }

    }

    private function linkExpired($email,$key){
        $dateNow = DateUtil::dateTimeNow();
        $dateTime  = DB::select("SELECT create_date_time FROM t_forgot_password
                                WHERE user_email LIKE '%".$email."%'
                                AND random_key LIKE '%".$key."%' ");
        $data=[];
        foreach ($dateTime as $value)
        {
            $data = $value->create_date_time;
        }


//        \Log::debug($dateNow-$data);

        $date = ($dateNow-$data)/60;
//        \Log::debug($date);


        $env = [
            "nama"=>env('RESET_KEY_EXPIRED','1')
        ];

        if($date<=$env['nama'])
        {

            return [
                "status" => "OK"
            ];
        }
        else
        {
            return [
                "status" => "NOPE"
            ];
        }


    }
    
	public function update(Request $request)
    {
        $getForgotPassword = App::make('getForgotPassword');       
        

        $inputforgotpassword = [
            "email" => $request->email,
            "random_key" => $request->randomkey
        ];

        
        
        $outputforgotpassword = $getForgotPassword->execute($inputforgotpassword);
        

        if($outputforgotpassword['status']=="NOPE")
        {
            return redirect('/')->with("errormsg","You don't have access to reset your password");
            
        }
        else
        {
            $updateUserForgot = App::make('updateUserForgot');
            $input = [
                "email"=>$request->email,
                "password"=>$request->new_password
            ];  
            try {
                
                 $output = $updateUserForgot->execute($input);

             } catch(Exception $ex){

                 Log::error($ex->getMessage());

             }

            return redirect('/')->with("successMessage","You have successfully set your new password");
            
        }
        
    }
}