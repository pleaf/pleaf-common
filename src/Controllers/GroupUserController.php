<?php
namespace Sts\PleafCommon\Controllers;

use App;
use Illuminate\Http\Request;
use App\Http\Requests;
use Log;
use Sts\PleafCore\DateUtil;
use Sts\PleafCore\SessionUtil;
use Sts\PleafCore\MessageHelper;
use Sts\PleafCore\CoreException;
use Sts\PleafCore\Response;

class GroupUserController extends \App\Http\Controllers\Controller {

    public function index(){

        return view("pleaf-common::groupUser.index");
    }

    public function getGroupUserList(){

        $getGroupUserList = App::make('getGroupUserList');


        $input = [
            'tenantId'=>SessionUtil::getTenantId()
        ];

        $output = $getGroupUserList->execute($input);
        $groupUserList = $output['groupUserList'];
        $count = count($groupUserList);

        return response()
            ->json([
                'list'=>$groupUserList,
                'total'=>$count
            ]);
    }

    public function addGroupUser(){
        return view("pleaf-common::groupUser.addGroupUser");
    }

    public function store(Request $request){

       $saveGroupUser = App::make('addGroupUser');

       $input = [
           'tenantId'=>SessionUtil::getTenantId(),
           'groupUserCode'=> $request->input('groupUserCode'),
           'groupUserName'=> $request->input('groupUserName'),
           'groupUserDesc'=> $request->input('groupUserDesc'),
           'userLoginId'=> SessionUtil::getUserLoginId(),
           'datetime'=>DateUtil::dateTimeNow()
       ];

       try {

         $output = $saveGroupUser->execute($input);

         $newGroupUser = $output['groupUser'];

         MessageHelper::displaySuccess(_ADD_DATA);
         return redirect('/admin/groupuser');

       } catch(CoreException $ex){

         MessageHelper::displayError($ex);
         return redirect('admin/groupuser/add');
       }


    }

    public function editGroupUser($id){
        $getGroupUser = App::make('findGroupUserById');


        $input = [
            'groupUserId' => $id
        ];

        $output = $getGroupUser->execute($input);
        $getGroupUserData = $output['groupUser'];

        return view('pleaf-common::groupUser.editGroupUser')
            ->with('groupUserData',$getGroupUserData);

    }

    public function updateGroupUser (Request $request){
        $loggedUserId=SessionUtil::getUserLoginId();
        $updateGroupUser = App::make('editGroupUser');
        $time = DateUtil::dateTimeNow();

        $input = [
            'userLoginId'=>$loggedUserId,
            'groupUserId'=>$request->input('groupUserId'),
            'groupUserName'=>$request->input('groupUserName'),
            'groupUserDesc'=>$request->input('groupUserDesc'),
            'datetime'=>$time
        ];

        try {
            $output = $updateGroupUser->execute($input);
            MessageHelper::displaySuccess(_EDIT_DATA);
            return redirect('/admin/groupuser');

        } catch(CoreException $ex){

          MessageHelper::displayError($ex);
          return redirect('admin/groupuser/edit/'.$request->input('groupUserId'));
        }


    }

    public function deleteGroupUser($id){
        $deleteGroupUser = App::make('removeGroupUser');

        $input = [
            'groupUserId'=>$id
        ];
        try{
            $deleteGroupUser->execute($input);
            MessageHelper::displaySuccess(_DELETE_DATA);
            return redirect('/admin/groupuser');
        }catch(CoreException $ex){
            MessageHelper::displayError($ex);
            return redirect('/admin/groupuser');
        }
    }

    public function viewEditMember($id){
        $groupUserId = $id;
        return view('pleaf-common::groupUser.editMember')
            ->with('groupUserId',$groupUserId);
    }

    public function getDataForEditMemberGroupUser($id){
        $getMembers = App::make('getMembersByGroupUserId');

        $input = [
            'groupUserId'=>$id
        ];

        $members = $getMembers->execute($input);

        $getUsers = App::make('getUsersDetailFromGroupMember');

        $inputForUsers = [

        ];

        $users = $getUsers->execute($inputForUsers);

        $getGroupUser = App::make('findGroupUserById');

        $inputForGroupUser = [
            'groupUserId'=>$id
        ];

        $groupUser = $getGroupUser->execute($inputForGroupUser);

        return response()
            ->json([
                'members'=>$members['members'],
                'users'=> $users['users'],
                'groupUser'=>$groupUser['groupUser']
            ]);
    }

    public function deleteMemberFromGroupUser(Request $request){


        $getData = App::make('removeMemberFromGroupUser');

        $input = [
            'groupUserId'=>$request->groupUserId,
            'userId'=>$request->userId
        ];


        try {
            $output = $getData->execute($input);

            $response = [ "message" =>"Data successfully added"];

            return response()->json(Response::ok($response));

        } catch(CoreException $ex){

            return response()->json(Response::fail($ex));
        }


        // return redirect('admin/groupuser/viewMember/'+$request->groupUserId);

    }

    public function joinMember(Request $request){
        $loggedUserId= SessionUtil::getUserLoginId();
        $getData = App::make('addJoinMember');
        $time = DateUtil::datetimeNow();

        $input = [
            'groupUserId'=>$request->groupUserId,
            'userId'=>$request->userId,
            'userLoginId'=>$loggedUserId,
            'datetime'=>$time
        ];


        try {

        $output = $getData->execute($input);
        $response = [ "message" =>"Data successfully added"];

        return response()->json(Response::ok($response));

        }catch(CoreException $ex){
          return response()->json(Response::fail($ex));
        }

    }

}
