<?php

namespace Sts\PleafCommon\Controllers;

use App;
use Illuminate\Http\Request;
use App\Http\Requests;
use Sts\PleafCore\DateUtil;
use Sts\PleafCore\MessageHelper;
use Sts\PleafCore\CoreException;
use Sts\PleafCore\SessionUtil;

class ComboConstantController extends \App\Http\Controllers\Controller {

    public function index() {

        $getComboList = App::make('getComboConstantList');

        $input = [];

        $output = $getComboList->execute($input);
        $combo_constant_list = $output['combo_constant_list'];

        return view('pleaf-common::comboconstant.index')->with('combo_constant_list',$combo_constant_list);



    }

    public function create(){

        return view('pleaf-common::comboconstant.insert');
    }


    public function store(Request $request){

        $addComboConstant = App::make('addComboConstant');

        $input = [
            "tenantId" => SessionUtil::getTenantId(),
            "combo_id" => $request->combo_id,
            "code" => $request->code,
            "prop_key" => $request->prop_key,
            "code_group" => $request->code_group,
            "sort_no" => $request->sort_no,
            "userLoginId" => SessionUtil::getUserLoginId(),
            "datetime"=>DateUtil::dateTimeNow()

        ];

        try {
            $output = $addComboConstant->execute($input);

            MessageHelper::displaySuccess(_ADD_DATA);

        } catch(CoreException $ex){

          MessageHelper::displayError($ex);
          return redirect('admin/comboconstant/create');
        }

        return redirect('admin/comboconstant');
    }


    public function show($id){

    }

    public function search(Request $request){


    }


    public function edit($id, $code){
        $findComboConstantById = App::make('findComboConstantById');

        $input = [
           "combo_id" => $id,
           "code" => $code
        ];

        $output = $findComboConstantById->execute($input);


        $data_combo_constant = $output['edit_combo_constant'];

        return view('pleaf-common::comboconstant.update',compact('data_combo_constant'));
    }



    public function update(Request $request){

             $editComboConstant = App::make('editComboConstant');
             $userLoginId = SessionUtil::getUserLoginId();
             $time = DateUtil::dateTimeNow();

             $input = [
                 "combo_id" => $request->input('combo_id'),
                 "code" => $request->input('code'),
                 "userLoginId" => $userLoginId,
                 "prop_key" => $request->input('prop_key'),
                 "code_group" => $request->input('code_group'),
                 "sort_no" => $request->input('sort_no'),
                 "datetime" => $time

             ];

            try {

                 $output = $editComboConstant->execute($input);
                 MessageHelper::displaySuccess(_EDIT_DATA);
                 return redirect('admin/comboconstant');

             } catch(CoreException $ex){

                 MessageHelper::displayError($ex);
                 return redirect('admin/comboconstant/edit/'.$request->input('combo_id').'/'.$request->input('code'));

             }

    }

    public function destroy($id, $code){

        $deleteComboConstant = App::make('removeComboConstant');

        $input = [
           "combo_id" => $id,
           "code" => $code
        ];


        $output = $deleteComboConstant->execute($input);
        MessageHelper::displaySuccess(_DELETE_DATA);
        return redirect('admin/comboconstant');

    }


    public function getComboConstantList(){

        $getComboConstantList = App::make('getComboConstantList');

        $input = [];

        try {
            $output = $getComboConstantList->execute($input);
            $list = $output['combo_constant_list'];
            $count = count($list);

            return response()->json([
                "list" => $list,
                "total" => $count
            ]);

        }catch(CoreException $ex){
            Log:error($ex->getMessage());

        }
    }

}
