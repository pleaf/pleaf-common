<?php

namespace Sts\PleafCommon\Controllers;

use App;
use Illuminate\Http\Request;
use Sts\PleafCore\DateUtil;
use Sts\PleafCore\SessionUtil;
use Sts\PleafCore\CoreException;
use Sts\PleafCore\MessageHelper;

class OUController extends \App\Http\Controllers\Controller {
	
	public function index(){
       return view('pleaf-common::OU.index');
    }
    
    public function getOUList(){
    	$getOUList = App::make('getOUList');
    	$input = [
    			'tenant_id'=>SessionUtil::getTenantId()
    	];
    	
    	$output = $getOUList->execute($input);
    	$ou_list = $output['ou_list'];
    	
    	return response()->json($ou_list);
    }

    public function create(){
        $getOUTypeList = App::make('getOUTypeList');
        $getOUList = App::make('getOUList');
        $input = [
        		'tenant_id'=>SessionUtil::getTenantId()
        ];
        $outputoutype = $getOUTypeList->execute($input);
        $outputou = $getOUList->execute($input);
        $ou_type_list = $outputoutype['outype_list'];
        $ou_name_list = $outputou['ou_list'];

        return view('pleaf-common::OU.insert')->with('ou_type_list',$ou_type_list)->with('ou_name_list',$ou_name_list);
    }

    public function store(Request $request){
        $addOU = App::make('addOU');
        $input = [
        	"tenant_id"=>SessionUtil::getTenantId(),
            "ou_parent_id"=>$request->ou_parent_id,
            "ou_code"=>$request->ou_code,
            "ou_name"=>$request->ou_name,
            "ou_type_id"=>$request->ou_type_id,
        	'userLoginId' => SessionUtil::getUserLoginId(),
            "datetime"=>DateUtil::dateTimeNow()

        ];
        try {   
            $output = $addOU->execute($input);
            $newOUCode = $output['OU'];
            // View Success Message
            MessageHelper::displaySuccess(_ADD_DATA);
            return redirect('admin/ou');
        } catch(CoreException $ex){
			// View Error Message        	
            MessageHelper::displayError($ex);
            return redirect('admin/ou/create');
        }

        return redirect('admin/ou');
    }


    public function search(Request $request){
       $getOUList = App::make('getOUList');

        $input = [
            "keysearch"=>$request->keysearch
        ];

        $output = $getOUList->execute($input);
        $ou_list = $output['ou_list'];
        $keysearch = $output['keysearch'];
        return view('pleaf-common::OU.index')->with('ou_list',$ou_list)->with('keysearch',$keysearch);
        
    }

    public function edit($id){
        $editOU = App::make('findOUById');
        $getOUTypeList = App::make('getOUTypeList');
        $getOUList = App::make('getOUList');
        $input = [
           "tenant_id"=>SessionUtil::getTenantId(),
           "ou_id" => $id
           
        ];
        
        $output = $editOU->execute($input);
        $outputoutype = $getOUTypeList->execute($input);
        $outputou = $getOUList->execute($input);
        
        $data_ou = $output['edit_ou'];
        $ou_type_list = $outputoutype['outype_list'];
        $ou_list = $outputou['ou_list'];
        
        return view('pleaf-common::OU.update',[
        		"ou_type_list"=>$ou_type_list,
        		"ou_list"=>$ou_list,
        		"data_ou"=>$data_ou
        ]);

    }

    public function update(Request $request){
            $updateOU = App::make('editOU');
            $input = [
            "ou_id"=>$request->input('ou_id'),
            "ou_type_id"=>$request->input('ou_type_id'),
            "ou_name"=>$request->input('ou_name'),
            "ou_parent_id"=>$request->input('parent_ou_id'),
            "userLoginId" => SessionUtil::getUserLoginId(),
            "datetime"=>DateUtil::dateTimeNow()
        ];
        try { 
         	$output = $updateOU->execute($input);
        	MessageHelper::displaySuccess(_EDIT_DATA);
        	return redirect('admin/ou');
        } catch(CoreException $ex){
        	MessageHelper::displayError($ex);
        	return redirect('admin/ou/edit/'.$request->ou_id);
        }
            
    }

    public function destroy($id){
        $deleteOU = App::make('removeOU');
        $input = [
           "tenant_id"=>SessionUtil::getTenantId(),
           "ou_id" => $id
           
        ];

        try { 
            $output = $deleteOU->execute($input);
            MessageHelper::displaySuccess(_DELETE_DATA);
        } catch(Exception $ex){
            MessageHelper::displayError($ex);
        }
        
       
        return redirect('admin/ou');
    }
	 
}