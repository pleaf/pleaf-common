<?php

namespace Sts\PleafCommon\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App;
use Sts\PleafCore\SessionUtil;
use Sts\PleafCore\CoreException;
use Sts\PleafCore\MessageHelper;
use Sts\PleafCore\DateUtil;

class PolicyController extends \App\Http\Controllers\Controller {
	
	public function index(){
        return view('pleaf-common::policy.index');
    }
    
    public function getPolicyList(){
    	$getPolicyList = App::make('getPolicyList');
    	
    	$input = [
    			"tenant_id"=>SessionUtil::getTenantId()
    	
    	];
    	
    	$output = $getPolicyList->execute($input);
    	$policy_list = $output['policy_list'];
    	
    	return response()->json($policy_list);
    	
    }

    public function create(){

        return view('pleaf-common::policy.insert');
    }

    public function store(Request $request){
        $addPolicy = App::make('addPolicy');

        $input = [
            "tenant_id" => SessionUtil::getTenantId(),
            "policy_code" => $request->policy_code,
            "policy_name" => $request->policy_name,
        	"userLoginId" => SessionUtil::getUserLoginId(),
        	"datetime"=> DateUtil::dateTimeNow()
        ];

        try {   
            $output = $addPolicy->execute($input);
            
            //View Succes Message
            MessageHelper::displaySuccess(_ADD_DATA);
            return redirect('admin/policy');

        } catch(CoreException $ex){
            //View Error Message 
            MessageHelper::displayError($ex);
            return redirect('admin/policy/create');
        }
            
    }

    public function show($id){

    }

    public function search(Request $request){
       
        
    }

    public function edit($id){
        $editPolicy = App::make('findPolicyById');

        $input = [
        		 "tenant_id"=>SessionUtil::getTenantId(),
                 "policy_id" => $id
        ];
       
        $output = $editPolicy->execute($input);
        $data_policy = $output['edit_policy'];
        return view('pleaf-common::policy.update',compact('data_policy'));

    }

    public function update(Request $request){
            
             $updatePolicy = App::make('editPolicy');
             $userTenant = SessionUtil::getTenantId();
             $input = [
             
                 "policy_id" => $request->policy_id,
                 "tenant_id" => $userTenant,
                 "policy_name" => $request->policy_name,
        		 "userLoginId" => SessionUtil::getUserLoginId(),
        		 "datetime"=> DateUtil::dateTimeNow()
             		
             ];

            try {
                
                 $output = $updatePolicy->execute($input);
                 //View Succes Message
                 MessageHelper::displaySuccess(_EDIT_DATA);
                 return redirect('admin/policy');

             } catch(CoreException $ex){
             	 //View Error Message
                 MessageHelper::displayError($ex);
                 return redirect('admin/policy/edit/'.$request->policy_id);

             }

    }

    public function destroy($id){
        $deletePolicy = App::make('removePolicy');
     
        $input = [
           "tenant_id"=>SessionUtil::getTenantId(),	
           "policy_id" => $id
        ];
		
        try {
        
        	$output = $deletePolicy->execute($input);
        	//View Succes Message
        	MessageHelper::displaySuccess(_DELETE_DATA);
        	return redirect('admin/policy');
        
        } catch(CoreException $ex){
        	//View Error Message
        	MessageHelper::displayError($ex);
        	return redirect('admin/policy');
        
        }

    }
	
}