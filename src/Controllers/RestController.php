<?php
namespace Sts\PleafCommon\Controllers;

use App;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Sts\PleafCore\CoreException;
use Sts\PleafCore\DateUtil;
use Sts\PleafCore\Response;
use Sts\PleafCore\SessionUtil;

class RestController extends Controller {


    public function restService(Request $request) {

        $service = App::make($request->get('service'));


        try {

            $object = $request->all();
            $inputDto = $object["payload"];

            $inputDto["user_login_id"] = SessionUtil::getUserLoginId();
            $inputDto["role_login_id"] = SessionUtil::getRoleLoginId();
            $inputDto["tenant_login_id"] = SessionUtil::getTenantId();
            $inputDto["record_owner_id"] = SessionUtil::getTenantId();
            $inputDto["username"] = SessionUtil::getUsername();
            $inputDto["datetime"] = DateUtil::dateTimeNow();
            $inputDto["date_now"] = DateUtil::dateNow();

            $outputDto = $service->execute($inputDto);

            $response = Response::isOk($outputDto);

        } catch(CoreException $ex) {
            $response = Response::isFail($ex);

        }

        return $response;

    }

}
