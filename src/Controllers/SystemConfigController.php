<?php
namespace Sts\PleafCommon\Controllers;

use App;
use Illuminate\Http\Request;
use App\Http\Requests;
use Sts\PleafCore\DateUtil;
use Sts\PleafCore\SessionUtil;
use Sts\PleafCore\MessageHelper;
use Sts\PleafCore\CoreException;
use Sts\PleafCore\Response;

class SystemConfigController extends \App\Http\Controllers\Controller {

    public function index(){

        return view("pleaf-common::systemconfig.index");
    }

    public function getSystemConfigList(){

        $getSystemConfigList = App::make('getSystemConfigList');
        $tenantId = SessionUtil::getTenantId();

        $input = [
            'tenantId'=>$tenantId
        ];

        try {

            $output = $getSystemConfigList->execute($input);
            $systemConfig_list = $output['systemConfig_list'];

            return response()->json(["list" => $systemConfig_list,
                                    "total" => count($systemConfig_list)]);

        } catch (CoreException $ex) {
            return response()->json();
        }

    }


    public function getSystemConfigListRest(){

        $getSystemConfigList = App::make('getSystemConfigList');
        $tenantId = SessionUtil::getTenantId();

        $input = [
            'tenantId'=>$tenantId
        ];

        $output = $getSystemConfigList->execute($input);
        $systemConfig_list = $output['systemConfig_list'];

        return Response::ok(["list" => $systemConfig_list]);
    }


    public function editSystemConfig($id){

        $getSystemConfigDetail = App::make('getSystemConfigDetail');

        $input = [
            'parameterId'=>$id
        ];

        $output = $getSystemConfigDetail->execute($input);
        $systemConfigDetail = $output['parameterDetail'];

        return view("pleaf-common::systemconfig.update")
            ->with('parameter',$systemConfigDetail);

    }

    public function updateSystemConfig(Request $request){

        $editSystemConfig = App::make('editSystemConfig');
        $userLoginId = SessionUtil::getUserLoginId();
        $time=DateUtil::dateTimeNow();

        $input = [
            'parameterId' => $request->input('parameterId'),
            'systemConfigId' => $request->input('systemConfigId'),
            'parameterValue' => $request->input('parameterValue'),
            'userLoginId' => $userLoginId,
            'datetime'  => $time
        ];

        try {
            $output = $editSystemConfig->execute($input);
            $systemConfig = $output['systemConfig'];
            MessageHelper::displaySuccess(_EDIT_DATA);
            return redirect('admin/systemconfig');

        } catch(CoreException $ex){
            MessageHelper::displayError($ex);
            return redirect('/admin/systemconfig/edit/'.$request->input('parameterId'));
        }


    }

}
