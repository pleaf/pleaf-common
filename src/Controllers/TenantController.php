<?php

namespace Sts\PleafCommon\Controllers;

use App;
use Session;
use Illuminate\Http\Request;
use App\Http\Requests;
use Sts\PleafCore\DateUtil;
use Sts\PleafCore\SessionUtil;
use Sts\PleafCore\CoreException;
use Sts\PleafCore\Response;
use Sts\PleafCore\MessageHelper;

class TenantController extends \App\Http\Controllers\Controller {

	public function index(){

        $getTenantList = App::make('getTenantList');

        $input = [

        ];

        $output = $getTenantList->execute($input);
        $tenant_list = $output['tenant_list'];

        return view('pleaf-common::tenant.index')->with('tenant_list',$tenant_list);

    }

    public function create(){
        return view('pleaf-common::tenant.insert');
    }

    public function store(Request $request){
        $addTenant = App::make('addTenant');
        $findUserById = App::make("findUserById");
        $addCreateInitialTenant = App::make('addCreateInitialTenant');
		$datetime = DateUtil::dateTimeNow();
		$userLoginId = SessionUtil::getUserLoginId();

        $input = [
            "userTenantId"=> SessionUtil::getTenantId(),
            "checked" => $request->input("check"),
            "tenant_code" => $request->input("tenant_code"),
            "tenant_name" => $request->input("tenant_name"),
            "description" => $request->input("description"),
            "tenant_key" => md5($request->input("tenant_key")),
            "email" => $request->input("email"),
            "host" => $request->input("host"),
			"datetime" => $datetime,
			"user_id" =>$userLoginId
        ];

        try {
            $addTenant->execute($input);
            MessageHelper::displaySuccess(_ADD_DATA);
            return redirect('admin/tenant');

        } catch(CoreException $ex){
            MessageHelper::displayError($ex);
            return redirect('admin/tenant/create');
        }

    }

    public function addTenantRest(Request $request){

        $addTenant = App::make('addTenant');
        $response = $request->input("payload");

        $input = [
            "userTenantId"=> $response["tenant_id"],
            "tenant_code" => $response["tenant_code"],
            "tenant_name" => $response["tenant_name"],
            "description" => $response["description"],
            "tenant_key" => md5($response["tenant_key"]),
            "email" => $response["email"],
            "host" => $response["host"],
            "datetime"=> DateUtil::dateTimeNow(),
            "userLoginId"=>SessionUtil::getUserLoginId()
        ];

        try {
            $addTenant->execute($input);
            $data =  response()->json(Response::ok(["list" => $input]));

        } catch(CoreException $ex){

            $data =  response()->json(Response::fail($ex));

        }

        return $data;

    }

    public function deleteTenantRest(Request $request){
        $removeTenantRest = App::make("removeTenant");
        $payload = $request["payload"];

        $input = [
            "tenant_id" => $payload["tenant_id"]
        ];


        try {

            $tenant = $removeTenantRest->execute($input);
            $response = response()->json(Response::ok([]));

        } catch(CoreException $ex){
            $response = response()->json(Response::fail($ex));
        }

        return $response;

    }

    public function updateTenantRest(Request $request){
        $editTenant = App::make('editTenant');
        $payload = $request["payload"];

        $input = [
            "tenant_id" => $payload["tenant_id"],
            "description" => $payload["description"],
            "tenant_name" => $payload["tenant_name"],
            "email" => $payload["email"],
            "host" => $payload["host"],
            "userLoginId" => SessionUtil::getUserLoginId(),
            "datetime" => DateUtil::dateTimeNow()
        ];

        try {

            $editTenant->execute($input);
            $response = response()->json(Response::ok(["list" => $input]));

        } catch (CoreException $ex){
            $response = response()->json(Response::fail($ex));
        }

        return $response;
    }

    public function show($id){

    }

    public function search(Request $request){

        $getTenantList = App::make('getTenantList');

        $input = [
           "keyword" => $request->keysearch
        ];
        $output = $getTenantList->execute($input);
        $data = $output['list'];

        return view('pleaf-common::tenant.index')->with('data',$data);

    }

    public function edit($id){

//    \Log::debug($id);

        $findTenantById = App::make('findTenantById');
//       \Log::debug($findTenantById==null);


        $input = [
                 "tenant_id" => $id
        ];

        try {
            $output = $findTenantById->execute($input);
            $data_tenant = $output['edit_tenant'];
//            \Log::debug($output['edit_tenant']->tenant_id);
            return view('pleaf-common::tenant.update',compact('data_tenant'));
        } catch(CoreException $ex){
            MessageHelper::displayError($ex);
            return back();
        }

    }

    public function update(Request $request){

        $editTenant = App::make('editTenant');
        $userLoginId = SessionUtil::getUserLoginId();
        $datetime = DateUtil::dateTimeNow();

        $input = [

             "tenant_id" => $request->tenant_id,
             "description" => $request->description,
             "tenant_name" => $request->tenant_name,
             "email" => $request->email,
             "host" => $request->host,
             "userLoginId" => $userLoginId,
             "datetime" => $datetime

        ];

        try {

            $output = $editTenant->execute($input);
            MessageHelper::displaySuccess(_EDIT_DATA);
            return redirect('admin/tenant');

        } catch(CoreException $ex){
            MessageHelper::displayError($ex);
            return back();
        }


    }

    public function refresh(){

        MessageHelper::displaySuccess(_LOAD_DATA);
        return redirect('admin/tenant');
    }


    public function getTenantList(){
        $getTenantList = App::make('getTenantList');

        $input = [];
        try {
            $output = $getTenantList->execute($input);
            $list = $output['tenant_list'];
            $count = count($list);

            return response()->json(Response::ok([
                "list" => $list,
                "total" => $count
            ]));

        }catch(CoreException $ex){
            return response()->json(Response::fail($ex));
        }
    }

    public function changeTenantForUser() {

        try {

            $user_id = SessionUtil::getUserLoginId();

            $findUserById = App::make("findUserByIdCommon");
            $getPolicyTenantUserList = App::make("getPolicyTenantUserList");

            $input_dto = [
                "user_id" => $user_id
            ];

            $output_user_dto = $findUserById->execute($input_dto);
            $output_policy_user_list_dto = $getPolicyTenantUserList->execute($input_dto);

            return view("pleaf-common::tenant.viewChangeTenant", [
                "user" => $output_user_dto["edit_user"],
                "policy_tenant_list" => $output_policy_user_list_dto["policy_tenant_list"]
            ]);

        } catch(CoreException $ex) {

            \Log::debug(json_encode($ex->getErrorList()));

        }

    }

    public function kickOutSessionTenant() {

        Session::put(_PLEAF_SESS_USERS, [
            "username"          => SessionUtil::getUsername(),
            "fullname"          => SessionUtil::getFullname(),
            "user_id"           => SessionUtil::getUserLoginId(),
            "role_default_name" => SessionUtil::getRoleDefaultName(),
            "role_default_id"   => SessionUtil::getRoleLoginId(),
            "tenant_id"         => _NULL_LONG,
            "tenant_name"       => _EMPTY_VALUE,
            "tenant_key"        => _EMPTY_VALUE
        ]);

        return redirect("/admin/tenant/change-tenant");

    }

    public function viewManageTenantUser($id) {

        try {

            $findUserById = App::make("findUserByIdCommon");

            $input_dto = [
                "user_id" => $id
            ];

            $output_dto = $findUserById->execute($input_dto);

            return view("pleaf-common::tenant.viewManageTenantUser", [
                "user" => $output_dto["edit_user"]
            ]);
        } catch(CoreException $ex) {

            \Log::debug(json_encode($ex->getErrorList()));

        }

    }

    public function getPolicyManageUserTenantList($id) {

        try {

            $getManageUserTenantList = App::make("getManageUserTenantList");
            $getPolicyTenantUserList = App::make("getPolicyTenantUserList");

            $input_dto = [
                "user_id" => $id
            ];

            $manage_user_tenant_list = $getManageUserTenantList->execute($input_dto);
            $policy_tenant_list = $getPolicyTenantUserList->execute($input_dto);

            $merge = array_merge($manage_user_tenant_list, $policy_tenant_list);

            return Response::isOk([
                "list" => $merge,
            ]);

        } catch(CoreException $ex) {

            return Response::isFail($ex);

        }

    }

    public function  doChangeTenant(Request $request) {

        try {

            $findTenantById = App::make("findTenantById");

            $input_dto = [
                "tenant_id" => $request->get("tenant_id")
            ];

            $output_tenant_dto = $findTenantById->execute($input_dto);

            $tenant_dto = $output_tenant_dto["edit_tenant"];

            Session::put(_PLEAF_SESS_USERS, [
                "username"          => SessionUtil::getUsername(),
                "fullname"          => SessionUtil::getFullname(),
                "user_id"           => SessionUtil::getUserLoginId(),
                "role_default_name" => SessionUtil::getRoleDefaultName(),
                "role_default_id"   => SessionUtil::getRoleLoginId(),
                "tenant_id"         => $tenant_dto->tenant_id,
                "tenant_name"       => $tenant_dto->tenant_name,
                "tenant_key"        => $tenant_dto->tenant_key
            ]);

            return redirect(config("pleaf_config.app_redirect_success_change_tenant"));

        } catch(CoreException $ex) {
            return redirect()->back();
        }

    }

}
