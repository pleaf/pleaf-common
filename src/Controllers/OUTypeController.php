<?php

namespace Sts\PleafCommon\Controllers;

use App;
use Illuminate\Http\Request;
use App\Http\Requests;
use Sts\PleafCore\DateUtil;
use Sts\PleafCore\SessionUtil;
use Sts\PleafCore\MessageHelper;
use Sts\PleafCore\CoreException;

class OUTypeController extends \App\Http\Controllers\Controller {
	
	public function index(){
		
		 return view('pleaf-common::OUType.index');
		
	}

	public function getOUTypeList(){
		$getOUTypeList = App::make('getOUTypeList');
				
		$input = [
			'tenant_id'=>SessionUtil::getTenantId()
		];
		
		$output = $getOUTypeList->execute($input);
		$outype_list = $output['outype_list'];

		return response()->json($outype_list);
	}
	
	public function create(){

		return view('pleaf-common::OUType.insert');
	}

	public function store(Request $request){
		$addOUType = App::make('addOUType');

		$input = [
			"tenant_id"=>SessionUtil::getTenantId(),	
			"ou_type_code"=>$request->ou_type_code,
			"ou_type_name"=>$request->ou_type_name,
			"flg_bu"=>$request->chkBU,
			"flg_accounting"=>$request->chkAccounting,
			"flg_legal"=>$request->chkLegal,
			"flg_sub_bu"=>$request->chkSBU,
			"flg_branch"=>$request->chkBranch,
			"userLoginId" => SessionUtil::getUserLoginId(),
			"datetime"=>DateUtil::dateTimeNow()

		];

		try {   
            $output = $addOUType->execute($input);
            $newOUTypeCode = $output['OUType'];
            // View Success Message 
            MessageHelper::displaySuccess(_ADD_DATA);
            return redirect('admin/outype');
        } catch(CoreException $ex){
        	// View Error Message
            MessageHelper::displayError($ex);
            return redirect('admin/outype/create');
        }

        return redirect('admin/outype');
	}

	public function search(Request $request){
		$getOUTypeList = App::make('getOUTypeList');
		
		$input = [
			"keysearch"=>$request->keysearch
		];

		$output = $getOUTypeList->execute($input);
		$outype_list = $output['outype_list'];
		$keysearch = $output['keysearch'];
		return view('pleaf-common::OUType.index')->with('outype_list',$outype_list)->with('keysearch',$keysearch);
	}

	public function edit($id){
		$editOUType = App::make('findOUTypeById');
		
		$input= [
			'tenant_id'=>SessionUtil::getTenantId(),	
			'ou_type_id'=>$id	
		];
		
		$output = $editOUType->execute($input);
		$ouType = $output['edit_ou_type'];
		
        return view('pleaf-common::OUType.update')
        ->with('ouType',$ouType);
        
	}

	public function update(Request $request){
		$updateOUType = App::make('editOUType');

		$input = [
			"tenant_id"=>SessionUtil::getTenantId(),
			"ou_type_id"=>$request->txtID,
			"version"=>$request->version+1,
			"ou_type_code"=>$request->ou_type_code,
			"ou_type_name"=>$request->ou_type_name,
			"userLoginId" => SessionUtil::getUserLoginId(),
			"datetime"=>DateUtil::dateTimeNow()
				
		];

		try {
              $output = $updateOUType->execute($input);
              MessageHelper::displaySuccess(_EDIT_DATA);
              return redirect('admin/outype');
        } catch(CoreException $ex){
        	MessageHelper::displayError($ex);
        	return redirect('admin/outype/edit/'.$request->txtID);
        }
 
	}

	public function destroy($id){
		$deleteOUType = App::make('removeOUType');
		
        $input = [
        	"tenant_id"=>SessionUtil::getTenantId(),
        	"ou_type_id" => $id
           
        ];

        try{
        	$output = $deleteOUType->execute($input);
        	
        	// View Success Message
        	MessageHelper::displaySuccess(_DELETE_DATA);
        	return redirect('admin/outype');
        } catch (CoreException $ex){
        	MessageHelper::displayError($ex);
        	return redirect('admin/outype');
        }

	}
}