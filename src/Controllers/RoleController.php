<?php

namespace Sts\PleafCommon\Controllers;;

use App;
use Illuminate\Http\Request;
use App\Http\Requests;
use Sts\PleafCore\DateUtil,
    Sts\PleafCore\SessionUtil,
    Sts\PleafCore\MessageHelper,
    Sts\PleafCore\CoreException,
    Session;
use Sts\PleafCore\Response;

class RoleController extends \App\Http\Controllers\Controller {

    public function index(){

        return view('pleaf-common::Role.index');
    }

    public function getRoleList(){

        $getRoleList = App::make('getRoleListCommon');

        $input = [
            "tenant_id" => SessionUtil::getTenantId()
        ];

        $output = $getRoleList->execute($input);

        $role_list = $output['role_list'];

        return response()->json([
            'list' => $role_list
        ]);

    }

    public function create(){
        $getRoleType = App::make('getRoleType');

        $input = [
            "tenant_id" => SessionUtil::getTenantId(),
            "role_name" => Session::get('sessUser')['role_default_name']

        ];

        $output = $getRoleType->execute($input)['role_list'];

        $cmbRoleType = [(object)[
            "key"   => "ADMIN",
            "value" => "Administrator",
        ],(object)[
            "key"   => "USER",
            "value" => "User"
        ]];

        return view('pleaf-common::Role.insert',[
            "role_type" => $cmbRoleType,
            "type"      => $output[0]
        ]);
    }

    public function store(Request $request){
        $addRole = App::make('addRole');
        $datetime = DateUtil::dateTimeNow();
        $userLoginId = SessionUtil::getUserLoginId();

        $input = [
            "role_name"=> $request->get("role_name"),
            "role_type"=> $request->get("role_type"),
            "tenant_id"=> SessionUtil::getTenantId(),
            "datetime"=> $datetime,
            "userLoginId"=>$userLoginId
        ];

        try {
            $output = $addRole->execute($input);

            // View Success Messages
            MessageHelper::displaySuccess(_ADD_DATA);
            $response =  redirect('admin/role');

        } catch(CoreException $ex){
            // View Error Messages
            MessageHelper::displayError($ex);
            $response = redirect('/admin/role/create');
        }

        return $response;

    }

    public function search(Request $request){
        $getRoleList = App::make('getRoleList');

        $input = [
            "keysearch"=>$request->keysearch
        ];

        $output = $getRoleList->execute($input);
        $role_list = $output['role_list'];
        $keysearch = $output['keysearch'];

        return view('pleaf-common::Role.index')
            ->with('role_list',$role_list)
            ->with('keysearch',$keysearch);

    }

    public function getEditRole($id){

        $editRole = App::make('findRoleByIdForEdit');

        $input = [
            "role_id"   => $id,
            "tenant_id" => SessionUtil::getTenantId()
        ];

        $cmbRoleType = [(object)[
            "role_type" => "ADMIN",
        ],(object)[
            "role_type" => "USER"
        ]];

        $output = $editRole->execute($input);
        $data_role = $output['role'];

        return view('pleaf-common::Role.update',[
            'role' => $data_role[0],
            'role_type' => $cmbRoleType
        ]);

    }

    public function doUpdateRole(Request $request){
        $updateRole = App::make('editRole');
        $tenantId = SessionUtil::getTenantId();
        $userLoginId =\Session::get('sessUser')['user_id'];
        $datetime = DateUtil::dateTimeNow();

        $input = [
            "tenant_id" => $tenantId,
            "userLoginId" => $userLoginId,
            "role_id"   =>  $request->get("role_id"),
            "role_name" =>  $request->get("role_name"),
            "role_type" =>  $request->get("cmbRole"),
            "datetime" => $datetime
        ];

        try
        {

            $updateRole->execute($input);
            MessageHelper::displaySuccess(_EDIT_DATA);
            $response = redirect('/admin/role');

        }
        catch(CoreException $ex){

            MessageHelper::displayError($ex);
            $response = redirect('/admin/role/edit/'.$request->get("role_id"));

        }

        return $response;

    }

    public function doDeleteRole($id){
        $deleteRole = App::make('removeRole');

        $input = [
            "role_id" => $id

        ];

        $deleteRole->execute($input);

        MessageHelper::displaySuccess(_DELETE_DATA);
        return redirect('admin/role');
    }
    public function role($id){
        $findRoleById = App::make("findRoleByIdCommon");

        $input = [
            "id" => $id
        ];

        $output = $findRoleById->execute($input);

        return view('pleaf-common::Role.managerRoleTask',[
            "data" => $output
        ]);
    }

}
