<?php
/**
 * Created by PhpStorm.
 * User: Ali Irawan (boylevantz@gmail.com)
 * Date: 4/1/16
 * Time: 2:43 PM
 */

namespace Sts\PleafCommon\Controllers;


use App;
use Illuminate\Http\Request;
use App\Http\Requests;
use Sts\PleafCore\DateUtil,
    Sts\PleafCore\SessionUtil,
    Sts\PleafCore\MessageHelper,
    Sts\PleafCore\CoreException,
    Session;
use Sts\PleafCore\Response;

class RoleTaskController extends \App\Http\Controllers\Controller
{
    public function getRoleTask(Request $request){

        $getRoleTaskList = App::make('getRoleTaskList');

        $input = [
            "role_id" => $request->get('role_id'),
            "task_group" => $request->get('task_group')
        ];
        try {
            $output = $getRoleTaskList->execute($input);
            return Response::ok([
                "list" => $output['role_list']
            ]);
        }catch(CoreException $ex){
            return Response::fail($ex->getMessage());
        }
    }

    public function updateRoleTask(Request $request){

        $input = [
            'role_id' => $request->input('role_id'),
            'task_id' => $request->input('task_id'),
            'checked' => $request->input('checked'),
            'userLoginId' => SessionUtil::getUserLoginId(),
            'datetime' => DateUtil::dateTimeNow()
        ];

        if($input['checked']==true){
            // Add role task
            $addRoleTask = App::make("addRoleTask");

            try {
                $output = $addRoleTask->execute($input);
                return Response::ok($output);
            }catch(CoreException $ex){
                return Response::fail($ex);
            }
        }else{
            // Remove role task
            $removeRoleTask = App::make("removeRoleTask");

            try {
                $output = $removeRoleTask->execute($input);
                return Response::ok($output);
            }catch(CoreException $ex){
                return Response::fail($ex);
            }
        }
    }
}