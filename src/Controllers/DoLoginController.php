<?php

namespace Sts\PleafCommon\Controllers;
use App;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Sts\PleafCore\DateUtil;
use Sts\PleafCore\SessionUtil;
use Sts\PleafCore\CoreException;
use Sts\PleafCore\MessageHelper; 
use Session;
use Cookie;
use Log;

class DoLoginController extends \App\Http\Controllers\Controller {

    public function index(){
        
        $cookie = Cookie::get(_PLEAF_COOKIE_USERS);
        return view(config('pleaf_config.app_login_view'));
    }

    public function getLogin(Request $request){

        $getSessionLogin      = App::make('getSessionLogin');
        $findUserLoginByIndex = App::make("findUserLoginByIndex");
        $addUserLogin         = App::make("addUserLogin");

        \Artisan::call('view:clear');

            $input = [
                "username" => $request->input("txtUser"),
                "password" => $request->input("txtPass"),
                "datetime" => DateUtil::dateTimeNow()
            ];

        try {

            $output = $getSessionLogin->execute($input);

            if($output["session_status"]=="error")
            {
                return redirect('/')
                        ->with('errormsg','Username or Password is incorrect!');
            }

            else
            {

                $addUserLogin->execute($input);

                $user_id          = $output["session_data"][0]->user_id;

                $this->setSessionLoginUser($output["session_data"]);
                $this->setCurrentRole(SessionUtil::getRoleLoginId());
                $this->currentRole($user_id);

//                Log::debug("ISI SESSION");
//                Log::debug($output["session_data"]);

                $session_data      = $output["session_data"];
                $random            = str_random(40);
                $tokenrnd          = str_random(40);
                $username          = $session_data[0]->username;
                $fullname          = $session_data[0]->fullname;
                $user_id           = $session_data[0]->user_id;
                $role_default_name = $session_data[0]->role_name;
                $role_default_id   = $session_data[0]->role_default_id;
                $ou_default_id   = $session_data[0]->ou_default_id;
                $ou_default_code   = $session_data[0]->ou_code;
                $ou_default_name   = $session_data[0]->ou_name;
                $tenant_name       = $session_data[0]->tenant_name;
                $tenant_key        = md5($session_data[0]->tenant_code);
                $tenant_id         = $session_data[0]->tenant_id;

                Session::put(_PLEAF_SESS_USERS,[
                    "username"          => $username,
                    "user_id"           => $user_id,
                    "role_default_name" => $role_default_name,
                    "role_default_id"   => $role_default_id,
                    "ou_default_id"   => $ou_default_id,
                    "ou_default_code"   => $ou_default_code,
                    "ou_default_name"   => $ou_default_name,
                    "tenant_id"         => $tenant_id,
                    "tenant_name"       => $tenant_name,
                    "tenant_key"        => $tenant_key,
                    "fullname"          => $fullname
                ]);

                if($request->chkRem=="Y")
                {
                    $datetime      = DateUtil::dateTimeNow();
                    $addCookieUser = App::make('addCookieUser');

                    $inputCookie   = [
                        "datetime"          => $datetime,
                        "user_id"           => $user_id,
                        "token"             => $tokenrnd,
                        "series_identifier" => $random
                    ];

                    $outputCookie  = $addCookieUser->execute($inputCookie);

                    $cookie = [
                        "user_id"           => $user_id,
                        "token"             => $tokenrnd,
                        "series_identifier" => $random
                    ];

                    $cookiee = Cookie::forever(_PLEAF_COOKIE_USERS,serialize($cookie));

                    $role      = SessionUtil::getRoleDefaultName();
                    $availableRouteLoginSuccessByRole = config('pleaf_config.routes_redirect_login_success_by_role');

                    if(array_key_exists($role, $availableRouteLoginSuccessByRole)){
                        $idx = SessionUtil::getRoleDefaultName();
                        return redirect($availableRouteLoginSuccessByRole[$idx])->withCookie($cookiee);
                    }
                    else{
                        return redirect(config('pleaf_config.routes_redirect_login_success'))->withCookie($cookiee);
                    }


                }
                else
                {
                    $role      = SessionUtil::getRoleDefaultName();
                    $availableRouteLoginSuccessByRole = config('pleaf_config.routes_redirect_login_success_by_role');

                    if(array_key_exists($role, $availableRouteLoginSuccessByRole)){
                        $idx = SessionUtil::getRoleDefaultName();
                        return redirect($availableRouteLoginSuccessByRole[$idx]);
                    }
                    else{
                        return redirect(config('pleaf_config.routes_redirect_login_success'));
                    }

                }

            }

        } catch (CoreException $ex) {

            MessageHelper::displayError($ex);
            return redirect('pleaf_config.routes_redirect_login_success');
        }


    }


    private function currentRole($user_id){

        $findUserRoleByIndex = App::make("findUserRoleByIndex");

        $input = [
            "user_id" => $user_id
        ];

        $user_role = $findUserRoleByIndex->execute($input)["list"];

        $role = [];
        foreach($user_role as $key=>$value){

            $role[] = [
                "role_id" => $value->role_id,
                "role_name" => $value->role_name
            ];

        }

        Session::put(_PLEAF_SET_CURRENT_ROLE,$role);
    }


    private function setSessionLoginUser($session_data){

        $username          = $session_data[0]->username;
        $user_id           = $session_data[0]->user_id;
        $role_default_name = $session_data[0]->role_name;
        $role_default_id   = $session_data[0]->role_default_id;
        $tenant_name       = $session_data[0]->tenant_name;
        $tenant_key        = md5($session_data[0]->tenant_code);
        $tenant_id         = $session_data[0]->tenant_id;

        Session::put(_PLEAF_SESS_USERS,[
            "username"          => $username,
            "user_id"           => $user_id,
            "role_default_name" => $role_default_name,
            "role_default_id"   => $role_default_id,
            "tenant_id"         => $tenant_id,
            "tenant_name"       => $tenant_name,
            "tenant_key"        => $tenant_key
        ]);
    }

    public function setCurrentRole($role_id){

        $getUserRoleTask = App::make("getUserRoleTaskList");

        $input = [
            "role_id" => $role_id
        ];

        $role_task = $getUserRoleTask->execute($input)["role_task"];

        $setCurrentRole = [];
        foreach($role_task as $value){

            $setCurrentRole[] = $value->task_name;

        }

        Session::put(_PLEAF_CURRENT_ROLE,$setCurrentRole);

    }

}