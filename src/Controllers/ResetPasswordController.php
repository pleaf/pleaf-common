<?php

namespace Sts\PleafCommon\Controllers;;
use App;
use Illuminate\Http\Request;
use App\Http\Requests;
use Sts\PleafCore\DateUtil;
class ResetPasswordController extends \App\Http\Controllers\Controller {
	
	public function resetpassword($id){
        $resetUserPassword = App::make('resetUserPassword');

        $input = [
            "user_id"=>$id
        ];

        try{
            $output = $resetUserPassword->execute($input);

        }
        catch(Exception $ex){
            Log::error($ex->getMessage());
        }

        echo "Reset password ".$id." success!";
    }

    
	
}