<?php

namespace Sts\PleafCommon\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Sts\PleafCore\CoreException;
use Sts\PleafCore\Response;

use App;

class DBQueryController extends \App\Http\Controllers\Controller {
	
	public function index(){

        return view('pleaf-common::DBQuery.index');

    }

    public function query(Request $request){
        $dbQuery = App::make('dBQuery');
        $input = [
            "query" => $request['query']
        ];

        try {   
            $output = $dbQuery->execute($input);

            return response()->json(Response::ok(["list"=>$output['query_list']]));

        } catch(CoreException $ex){
            return response()->json(Response::fail($ex));
        }

    }
	
}