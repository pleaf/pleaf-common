<?php

namespace Sts\PleafCommon\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App;
use Session,
    Sts\PleafCore\MessageHelper,
    Sts\PleafCore\CoreException,
    Sts\PleafCore\SessionUtil,
    Sts\PleafCore\Response,
    Sts\PleafCore\DateUtil;


class UserController extends \App\Http\Controllers\Controller {

    public function index(){

        $getUserList = App::make('getUserListCommon');
        $tenantId = SessionUtil::getTenantId();
        $input = [
            'tenantId'=>$tenantId
        ];

        $output = $getUserList->execute($input);
        $user_list = $output['user_list'];

        return view('pleaf-common::user.index')
            ->with('user_list',$user_list);

    }

    public function getUsersList(){
        $getUserList = App::make('getUserListCommon');
        $tenantId = SessionUtil::getTenantId();
        $input = [
            'tenantId'=>$tenantId
        ];

        $output = $getUserList->execute($input);
        $user_list = $output['user_list'];

        return response()->json([
            "list" => $user_list
        ]);
    }

    public function getManageUserRoleList(Request $request){
        $getManageUserRoleList = App::make('getManageUserRoleList');

        $input = [
            "tenant_id" => SessionUtil::getTenantId(),
            "user_id" => $request->input('user_id'),
            "role_name" => $request->input('role_name')
        ];

        $output = $getManageUserRoleList->execute($input);
        $manageUserRoleList = $output['manage_user_list'];

        return response()->json([
            "list" => $manageUserRoleList
        ]);
    }

    public function create(){
        $getRoleList = App::make("getRoleListCommon");
        $getOuList = App::make("getOuCreateList");
        $getPolicyList = App::make("getPolicyList");

        $input = [
            "tenant_id" => SessionUtil::getTenantId()
        ];

        $output_role = $getRoleList->execute($input)["role_list"];
        $output_ou = $getOuList->execute($input)["ou_list"];
        $output_policy = $getPolicyList->execute($input)["policy_list"];

        return view('pleaf-common::user.insert',[
            "role" => $output_role,
            "ou"   => $output_ou,
            "policy" => $output_policy
        ]);
    }

    public function store(Request $request){
        $datetime = DateUtil::dateTimeNow();
        $userLoginId = SessionUtil::getUserLoginId();
        $addUser = App::make('addUserCommon');
        $userLoginTenant = SessionUtil::getTenantId();

        $input = [
            "username" => $request->input('username'),
            "email" => $request->input('email'),
            "fullname" => $request->input('fullname'),
            "password" => $request->input('password'),
            "phone" => $request->input('phone'),
            "role_default_id" => $request->input('role_default_id'),
            "ou_default_id" => $request->input('ou_default_id'),
            "policy_default_id" => $request->input('policy_default_id'),
            "datetime" => $datetime,
            "userLoginId" => $userLoginId,
            "tenant_id" =>$userLoginTenant
        ];

        try {

            $addUser->execute($input);
            MessageHelper::displaySuccess(_ADD_DATA);
            $response = redirect("/admin/user");

        } catch(CoreException $ex){

            MessageHelper::displayError($ex);
            $response = redirect("/admin/user/create");

        }

        return $response;

    }

    public function show($id){

    }

    public function search(Request $request){

    }

    public function edit($user_id){
        $user = App::make("findUserById");
        $getRoleList = App::make("getRoleListCommon");
        $getOuList = App::make("getOUList");
        $getPolicyList = App::make("getPolicyList");

        $input = [
            "user_id" => $user_id,
            "tenant_id" => SessionUtil::getTenantId()
        ];

        $output_user = $user->execute($input)["edit_user"];
        $output_role = $getRoleList->execute($input)["role_list"];
        $output_ou = $getOuList->execute($input)["ou_list"];
        $output_policy = $getPolicyList->execute($input)["policy_list"];

        return view('pleaf-common::user.update')
            ->with('user',$output_user)
            ->with('role',$output_role)
            ->with('policy',$output_policy)
            ->with('ou',$output_ou);

    }

    public function update(Request $request){

        $userLoginId = SessionUtil::getUserLoginId();
        $datetime = DateUtil::dateTimeNow();



        $updateUser = App::make('editUser');

        $input = [
            "user_id" => $request->user_id,
            "username" => $request->username,
            "email" => $request->email,
            "fullname" => $request->fullname,
            "password" => $request->password,
            "phone" => $request->phone,
            "role_default_id" => $request->role_default_id,
            "ou_default_id" => $request->ou_default_id,
            "policy_default_id" => $request->policy_default_id,
            "userLoginId"=> $userLoginId,
            "datetime"=>$datetime
        ];

        try {

            $updateUser->execute($input);
            MessageHelper::displaySuccess(_EDIT_DATA);
            return redirect('/admin/user');

        } catch(CoreException $ex){

            MessageHelper::displayError($ex);
            return redirect("/admin/user/edit/".$request->get("user_id"));

        }

    }

    public function manageUser($user_id){
        $findUserById = App::make("findUserByIdCommon");
        $getRoleList = App::make("getRoleListForManagerRole");
        $getPolicyList = App::make("getPolicyList");
        $getManageUserRoleList = App::make('getManageUserRoleList');

        $input = [
            "tenant_id" => SessionUtil::getTenantId(),
            "user_id" => $user_id
        ];

        $output_user = $findUserById->execute($input)["edit_user"];
        $output_role = $getRoleList->execute($input)["role_list"];
        $output_policy = $getPolicyList->execute($input)["policy_list"];
        $output_manage_user_role = $getManageUserRoleList->execute($input)["manage_user_list"];

        return view("pleaf-common::user.manageUserRole",[
            "user"             => $output_user,
            "role_list"        => $output_role,
            "policy_list"      => $output_policy,
            "manage_user_role_list" =>$output_manage_user_role

        ]);
    }

    public function doAddManageUser(Request $request){

        $addManageUserRole = App::make("addManageUserRole");

        $user_id = $request->input("user_id");
        $cmb_role = $request->input("cmb_role");
        $cmb_policy = $request->input("cmb_policy");

        $input = [
            "user_id" => $user_id,
            "role_id" => $cmb_role,
            "policy_id" => $cmb_policy,
            "userLoginId" => SessionUtil::getUserLoginId(),
            "datetime" => DateUtil::dateTimeNow()
        ];

        try {

            $addManageUserRole->execute($input);
            MessageHelper::displaySuccess(_ADD_DATA);
            $response = redirect("/admin/user/manage-user/".$user_id);

        } catch (CoreException $ex) {

            MessageHelper::displayError($ex);
            $response = redirect("/admin/user/manage-user/".$user_id);

        }

        return $response;



    }

    public function destroy($id){

        $deleteUser = App::make('removeUser');

        $input = [
            "user_id" => $id
        ];


        $deleteUser->execute($input);
        MessageHelper::displaySuccess(_DELETE_DATA);
        return redirect('admin/user');
    }

    public function changerole()
    {
        $getListRole = App::make('getListRole');
        $data_session = Session::get(_PLEAF_SESS_USERS);
        $input = [
            "tenant_id" => SessionUtil::getTenantId()

        ];

        $output = $getListRole->execute($input);
        $list_role = $output['list_role'];
        return view('pleaf-common::user.changerole')->with('list_role',$list_role)
            ->with('data_session',$data_session);

    }

//     public function gantirole(Request $request)
//     {
//         \Artisan::call('view:clear');

//         $findRoleById = App::make('findRoleByIdCommon');
//         $data = Session::get(_PLEAF_SESS_USERS);
//         $input = [
//             "id"=>$request->input("u_role")
//         ];

//         $output = $findRoleById->execute($input);

//         Session::put(_PLEAF_SESS_USERS,['username'=>$data['username'] ,
//             'user_id'=>$data['user_id'],
//             'fullname'=>$data['fullname'],
//             'role_default_name'=>$output->role_name,
//             'role_default_id'=>$request->u_role,
//             'tenant_id'=>$data['tenant_id'],
//             'tenant_name'=>$data['tenant_name'],
//             'tenant_key'=>$data['tenant_key']
//         ]);

//         Session::put(_PLEAF_SESS_USERS, $data);        
        
//         $this->setChangeRoleUser($request->input("u_role"));
        
//         return redirect(\Config::get("pleaf_config.app_redirect_success_change_role"));
        
//     }

    public function gantirole(Request $request)
    {
    	\Artisan::call('view:clear');
    
    	$findRoleById = App::make('findRoleByIdCommon');
    	$data = Session::get(_PLEAF_SESS_USERS);
    	$input = [
    			"id"=>$request->input("u_role")
    	];
    
    	$output = $findRoleById->execute($input);
    
    	$data["role_default_name"] = $output->role_name;
    	$data["role_default_id"] = $request->u_role;
    
    	Session::put(_PLEAF_SESS_USERS, $data);
    	$this->setChangeRoleUser($request->input("u_role"));
    
    	return redirect(\Config::get("pleaf_config.app_redirect_success_change_role"));
    
    }    
    
    private function setChangeRoleUser($role_id){
        $getUserRoleTask = App::make("getUserRoleTaskList");

        $input = [
            "role_id" => $role_id
        ];

        $role_task = $getUserRoleTask->execute($input)["role_task"];

        $setCurrentRole = [];
        foreach($role_task as $value){

            $setCurrentRole[] = $value->task_name;

        }

        Session::put(_PLEAF_CURRENT_ROLE,$setCurrentRole);

    }

    public function changetenant()
    {
        $getTenantList = App::make('getTenantList');
        $data_session = Session::get(_PLEAF_SESS_USERS);
        $input = [

        ];

        $output = $getTenantList->execute($input);

        $tenant_list=$output["tenant_list"];

        return view('pleaf-common::user.changetenant',[
            "tenant_list" => $tenant_list,
            "data_session" => $data_session
        ]);
    }

    public function gantitenant(Request $request)
    {
        $getTenant = App::make('findTenantById');
        $data = Session::get(_PLEAF_SESS_USERS);
        $input = [
            "tenant_id"=>$request->u_tenant_id
        ];
        $output = $getTenant->execute($input);

        $new_tenant_name = $output['edit_tenant'];

        $nama = $new_tenant_name['tenant_name'];
        $new_tenant_key = md5($new_tenant_name['tenant_code']);
        Session::put('sessUser',['username'=>$data['username'] ,
            'user_id'=>$data['user_id'],
            'role_default_name'=>$data['role_default_name'],
            'role_default_id'=>$data['role_default_id'],
            'tenant_id'=>$request->u_tenant_id,
            'tenant_name'=>$nama,
            'tenant_key'=>$new_tenant_key
        ]);
        var_dump(Session::get('sessUser'));
        return redirect('admin');
    }

    public function removeUserRole($id,$userId){
        $removeUserRole = App::make('removeUserRole');

        $input = [
            "user_role_id" => $id
        ];

        $removeUserRole->execute($input);
        MessageHelper::displaySuccess(_DELETE_DATA);
        return redirect('admin/user/manage-user/'.$userId);

    }

}