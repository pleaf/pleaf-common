<?php

namespace Sts\PleafCommon\Controllers;;
use App;
use Illuminate\Http\Request;
use App\Http\Requests;
use Sts\PleafCore\DateUtil;
class ChangePasswordController extends \App\Http\Controllers\Controller {
	
	public function index(){
      $loggedUser = \Session::get('sessUser');
      $getLoggedUserData = App::make('getLoggedUserData');
      $input = [
        "user_id"=>$loggedUser['user_id']
      ];
      $output = $getLoggedUserData->execute($input);
      $dataUser = $output["dataUser"];

      return view('pleaf-common::Login.changePassword')
            ->with('dataUser',$dataUser);
    }

    public function doChangePassword(Request $request)
    {
        $loggedUser = \Session::get('sessUser');
        $setNewPassword = App::make('setNewPassword');
        
        $input = [
            "user_id"=>$loggedUser['user_id'],
            "password"=>$request->password,
            "new_password"=>$request->newPassword,
            "confirm_password"=>$request->confirmPassword
        ];

        try{
            $output = $setNewPassword->execute($input);

        } catch(Exception $ex){
            Log::error($ex->getMessage());

        }

        return redirect('admin/changePasswords');


    }
	
}