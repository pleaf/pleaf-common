<?php

namespace Sts\PleafCommon\Controllers;

use Response;
use Session;
use Cookie;
use App;
use Sts\PleafCore\CoreException;
use Sts\PleafCore\DateUtil;
use Sts\PleafCore\MessageHelper;
use Sts\PleafCore\SessionUtil;

class DoLogoutController extends \App\Http\Controllers\Controller {
	
	public function index(){
        $removeCookieUser = App::make('removeCookieUser');
        $updateUserLogin = App::make("updateUserLogin");

        $input = [
            "username" => SessionUtil::getUsername(),
            "datetime" => DateUtil::dateTimeNow()
        ];

        try {

            $updateUserLogin->execute($input);

            if(Session::has(_PLEAF_SESS_USERS)) {
                Session::forget('sessUser');

                if (Cookie::has('user_cookie')) {
                    # code...
                    $cookie = Cookie::get(_PLEAF_COOKIE_USERS);
                    $data_cookie = unserialize($cookie);
                    $user_id = $data_cookie['user_id'];
                    $token = $data_cookie['token'];
                    $series_identifier = $data_cookie['series_identifier'];

                    $input = [
                        "user_id" => $user_id,
                        "token" => $token,
                        "series_identifier" => $series_identifier,
                    ];

                    $removeCookieUser->execute($input);

                    $cookie = Cookie::forget(_PLEAF_COOKIE_USERS);
                    return redirect('/')->withCookie($cookie);
                } else {


                    return redirect('/');
                }
            }
            else{
                return redirect('/');
            }

            } catch (CoreException $ex){

            MessageHelper::displayError($ex);
            return redirect("/admin");

        }

    }

   
	
}