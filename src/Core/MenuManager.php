<?php

namespace Sts\PleafCommon\Core;



class MenuManager {

    private $menu;

    function __construct($menu = []) {

        $this->menu = [
            "root" => [ ]
        ];

    }

    public function getMenu(){
        return $this->menu;
    }

    public function addMenu($array,$index,$index_key){
    	$this->menu[$index][$index_key] = $array ;
    }

}
