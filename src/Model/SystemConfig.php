<?php

namespace Sts\PleafCommon\Model;

use Illuminate\Database\Eloquent\Model;

class SystemConfig extends Model
{
   protected $primaryKey = "system_config_id";
   public $table = "t_system_config";
   public $timestamps = false;
}
