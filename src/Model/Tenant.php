<?php

namespace Sts\PleafCommon\Model;

use Illuminate\Database\Eloquent\Model;

class Tenant extends Model
{
   protected $primaryKey = "tenant_id";
   public $table = "t_tenant";
   public $timestamps = false;
}
