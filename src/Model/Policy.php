<?php

namespace Sts\PleafCommon\Model;

use Illuminate\Database\Eloquent\Model;

class Policy extends Model
{
   protected $primaryKey = "policy_id";
   public $table = "t_policy";
   public $timestamps = false;
}
