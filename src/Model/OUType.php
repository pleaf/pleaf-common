<?php

namespace Sts\PleafCommon\Model;

use Illuminate\Database\Eloquent\Model;

class OUType extends Model
{
   protected $primaryKey = "ou_type_id";
   public $table = "t_ou_type";
   public $timestamps = false;
}
