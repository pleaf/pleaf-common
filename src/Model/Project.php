<?php

namespace Sts\PleafCommon\Model;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
   protected $primaryKey = "";
   public $table = "";
   public $timestamps = false;
}
