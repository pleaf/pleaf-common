<?php

namespace Sts\PleafCommon\Model;

use Illuminate\Database\Eloquent\Model;

class CookieUser extends Model
{
   protected $primaryKey = "cookie_id";
   public $table = "t_user_cookie";
   public $timestamps = false;
}
