<?php

namespace Sts\PleafCommon\Model;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
   protected $primaryKey = "user_role_id";
   public $table = "t_user_role";
   public $timestamps = false;
}
