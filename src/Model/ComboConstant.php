<?php

namespace Sts\PleafCommon\Model;

use Illuminate\Database\Eloquent\Model;

class ComboConstant extends Model
{
   protected $primaryKey = ['combo_id','code'];
   public $table = "t_combo_value";
   public $timestamps = false;
   public $incrementing = false;
}
