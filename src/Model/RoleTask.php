<?php

namespace Sts\PleafCommon\Model;

use Illuminate\Database\Eloquent\Model;

class RoleTask extends Model
{
   protected $primaryKey = "role_task_id";
   public $table = "t_role_task";
   public $timestamps = false;

   public $active = _YES;
}
