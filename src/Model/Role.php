<?php

namespace Sts\PleafCommon\Model;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
   protected $primaryKey = "role_id";
   public $table = "t_role";
   public $timestamps = false;
}
