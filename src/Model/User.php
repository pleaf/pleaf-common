<?php

namespace Sts\PleafCommon\Model;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
   protected $primaryKey = "user_id";
   public $table = "t_user";
   public $timestamps = false;
}
