<?php

namespace Sts\PleafCommon\Model;

use Illuminate\Database\Eloquent\Model;

class Parameter extends Model
{
   protected $primaryKey = "parameter_id";
   public $table = "t_parameter";
   public $timestamps = false;
}
