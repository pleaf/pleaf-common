<?php

namespace Sts\PleafCommon\Model;

use Illuminate\Database\Eloquent\Model;

class OU extends Model
{
   protected $primaryKey = "ou_id";
   public $table = "t_ou";
   public $timestamps = false;
}
