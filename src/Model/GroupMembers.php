<?php

namespace Sts\PleafCommon\Model;

use Illuminate\Database\Eloquent\Model;

class GroupMembers extends Model
{
   protected $primaryKey = "group_members_id";
   public $table = "t_group_members";
   public $timestamps = false;
}
