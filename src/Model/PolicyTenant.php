<?php

namespace Sts\PleafCommon\Model;

use Illuminate\Database\Eloquent\Model;

class PolicyTenant extends Model
{
    protected $primaryKey = "policy_tenant_id";
    public $table = "t_policy_tenant";
    public $timestamps = false;
}
