<?php

namespace Sts\PleafCommon\Model;

use Illuminate\Database\Eloquent\Model;

/**
* @author  Widana
* @in
*
* @out
*/

class UserLogin extends Model
{
   protected $table      = "t_user_login";
   protected $primaryKey = "user_login_id";
   public $timestamps    = false;
   public $incrementing  = true;

}
