<?php

namespace Sts\PleafCommon\Model;

use Illuminate\Database\Eloquent\Model;

class GroupUser extends Model
{
   protected $primaryKey = "group_user_id";
   public $table = "t_group_user";
   public $timestamps = false;
}
