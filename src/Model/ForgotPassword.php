<?php

namespace Sts\PleafCommon\Model;

use Illuminate\Database\Eloquent\Model;

class ForgotPassword extends Model
{
   protected $primaryKey = "forgot_password_id";
   public $table = "t_forgot_password";
   public $timestamps = false;
}
