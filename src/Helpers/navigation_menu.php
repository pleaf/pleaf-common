<?php
  if(!function_exists('navigation_menu')){
  		function navigation_menu()
		{
			$menuManager = App::make('menuManager');
			$output = $menuManager->getMenu();
			foreach ($output['root'] as $row) {
				# code...
				if ($row['url']=="-") {
						# code...					
	    ?>
						<li class="dropdown">
	                        <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"> <?php echo $row['label']; ?> <span class="caret"></span></a>
	                        <ul role="menu" class="dropdown-menu">
	                        <?php 
	                        	foreach ($row['submenu'] as $key) {
									# code...	
	                         ?>
	                            	<li><a onclick="addTab('<?php echo $key['url']; ?>','<?php echo $key['label']; ?>')"><?php echo $key['label']; ?></a></li>
	                        <?php 
	                        	}
	                         ?>
	                        </ul>
	                    </li>
	    <?php  
					}
				else
				{	
	    ?>		              
			            <li class="dropdown">
			                <a aria-expanded="false" role="button" onclick="addTab('<?php echo $row['url']; ?>','<?php echo $row['label']; ?>')"><?php echo $row['label']; ?></a>
			            </li>       				
	    <?php
				}
			}
		}
  }
  ?>