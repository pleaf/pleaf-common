<?php
namespace Sts\PleafCommon\Middleware;

use App;
use Session;
use Closure;
use Exception;
use ReflectionException;

class ValidRequest {

    public function handle($request, Closure $next) {

        try {

            $object = $request->all();

            $this->checkKeyExists($object, "service");
            $this->checkKeyExists($object, "taskName");
            $this->checkKeyExists($object, "payload");

            $this->checkAuthorizedTaskUser($object["taskName"]);

            $this->checkClassExists($object["service"]);

            return $next($request);

        } catch (Exception $ex) {
            return response()->json([
                "error_message" => $ex->getMessage(),
                "status" => _RESPONSE_FAIL,
                "status_code" => $ex->getCode()
            ]);
        }

    }

    private function checkKeyExists($object, $key) {

        if(!array_key_exists($key, $object)){
            throw new Exception("Key $key not found");
        }
    }

    private function checkAuthorizedTaskUser($taskName) {
        if($taskName != 'system') {
            if(!in_array($taskName, Session::get(_PLEAF_CURRENT_ROLE))) {
                throw new Exception("You are not eligible to access");
            }
        }

    }

    private function checkClassExists($service) {

        try {

            App::make($service);

        } catch (ReflectionException $ex) {
            throw new Exception("Cannot find $service not found");
        }

    }
}