<?php

namespace Sts\PleafCommon\Middleware;

use Closure;
use Session;
use Cookie;
use Sts\PleafCommon\Model\CookieUser;
use App;
class LoginUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Session::has('sessUser')){
            return $next($request);
        }
        else
        {
            if(Cookie::has('user_cookie'))
            {
                $cookie = Cookie::get('user_cookie');
                $data_cookie = unserialize($cookie);
                $user_id = $data_cookie['user_id'];
                $token = $data_cookie['token'];
                $series_identifier = $data_cookie['series_identifier'];
                $getCookieUser = App::make('getCookieUser');
                $input = [
                    "user_id"=>$user_id,
                    "token"=>$token,
                    "series_identifier"=>$series_identifier
                ];
                $output = $getCookieUser->execute($input);

                $session_data = $output['data_session'];
                $username = $session_data[0]->username;
                $user_id = $session_data[0]->user_id;
                $role_default_name =  $session_data[0]->role_name;
                $role_default_id = $session_data[0]->role_default_id;
                $tenant_name = $session_data[0]->tenant_name;
                $tenant_key = md5($session_data[0]->tenant_code);
                $tenant_id = $session_data[0]->tenant_id;
                Session::put(_PLEAF_SESS_USERS,["username"=>$username,
                                                "user_id"=>$user_id,
                                                "role_default_name"=>$role_default_name,
                                                "role_default_id"=>$role_default_id,
                                                "tenant_id"=>$tenant_id,
                                                "tenant_name"=>$tenant_name,
                                                "tenant_key"=>$tenant_key
                    ]);
                return $next($request);
            }
            else
            {
                return redirect('/');
            }

        }

    }



}
