<?php

namespace Sts\PleafCommon\Middleware;

use Closure;
use Session;
use Cookie;
use Sts\PleafCommon\Model\CookieUser;
use App;
use Sts\PleafCore\SessionUtil;

class UserHasTenant
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(SessionUtil::getTenantId() == _NULL_LONG) {

            return redirect("/admin/tenant/change-tenant");
        }

        return $next($request);
    }

}
