<?php
Route::group(['middleware' => 'valid.request'], function () {
    Route::post('/api/service', '\Sts\PleafCommon\Controllers\RestController@restService');
});