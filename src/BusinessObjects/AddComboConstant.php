<?php

namespace Sts\PleafCommon\BO;
use App;
use Sts\PleafCommon\Model\ComboConstant;
use Sts\PleafCore as PleafCore;
use Sts\PleafCore\BusinessTransaction;
use Sts\PleafCore\CoreException;
use Sts\PleafCore\DefaultBusinessTransaction;
use Log;

/**
 * @Class AddComboConstant
 * @package Sts\PleafCommon\BO
 */

class AddComboConstant extends DefaultBusinessTransaction implements BusinessTransaction {

    public function getDescription(){
    	return "Add Combo Constant";
    }

    public function prepare ($dto, $originalDto){
        $this->valUniqueKey($dto);

    }

    private function valUniqueKey($dto){

      $findComboConstantByIndex = App::make('findComboConstantByIndex');
        $comboId = $dto['combo_id'];
        $code = $dto['code'];

          $input = [
              "combo_id"=>$comboId,
              "code" => $code
          ];
          $output = $findComboConstantByIndex->execute($input);
          $data = $output['comboConstant'];

          //validasi business

          if($data!=null)throw new CoreException(ERROR_BUSINESS_VALIDATION,[],
              ["code"=> "combo id ".$comboId." with code ".$code." already exists"]);

    }


	  public function process ($dto, $originalDto){


        $userId = $dto['userLoginId'];
        $datetime = $dto['datetime'];

        $combo_constant = new ComboConstant;
        $combo_constant->combo_id = $dto['combo_id'];
        $combo_constant->code = strtoupper($dto['code']);
        $combo_constant->prop_key = $dto['prop_key'];
        $combo_constant->code_group = $dto['code_group'];
        $combo_constant->sort_no = $dto['sort_no'];
        self::auditInsert($combo_constant, $userId, $datetime);
        $combo_constant->save();


	  }

    protected function rules(){
      return [
        'combo_id' => 'required',
        'code' => 'required',
        'prop_key' => 'required',
        'code_group' => 'required',
        'sort_no' => 'required',
        'tenantId' => 'required|val_tenant'
      ];
  }

}
