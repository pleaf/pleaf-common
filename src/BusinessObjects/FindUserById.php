<?php
namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use Sts\PleafCommon\Model\User;
use DB;

/**
 * @in
 * user_id
 *
 * @out
 * username
 * email
 * fulllname
 * phone
 * role_default_id
 * policy_default_id
 * ou_default_id
 */
class FindUserById implements BusinessFunction {

    public function getDescription(){
        return "Mengambil data user";
    }

    public function execute($dto){

        $user_id = $dto['user_id'];

        $user = User::where('user_id','=',$user_id)->first();
        return[

            "edit_user" => $user

        ];
    }
}