<?php

namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Log;

/**
 * 
 * @in
 *
 * @out
 */
class GetSessionLogin implements BusinessFunction {

    public function getDescription(){
    	return "change here";
    }

    public function execute($dto){
        $username = $dto['username'];
        $password = $dto['password'];

        if($this->validate_password($username,$password)){
            $dataSessionUser  = DB::select("SELECT A.*, B.role_name,C.tenant_code,C.tenant_name,D.ou_code,D.ou_name FROM t_user A
                             JOIN t_role B on A.role_default_id = B.role_id
                             LEFT JOIN t_tenant C on A.tenant_id = C.tenant_id
                             LEFT JOIN t_ou D on A.ou_default_id = D.ou_id
                             WHERE username ='".$username."'");
            if (empty($dataSessionUser)) {
                # code...
                return [
                    "session_status"=>"error"
                ];
            }
            else {
                return [
                    "session_status"=>"success",
                    "session_data"=>$dataSessionUser
                ];
            }
        }else{
            return [
                "session_status"=>"error"
            ];
        }

    }





    private function validate_password($username,$password)
    {
        $encryptMode= env("ENCRYPT_TYPE","BCRYPT");

        $hashpass = DB::table('t_user')
            ->select('password')
            ->where('username', $username)
            ->first();
        if($hashpass == NULL){
            return false;
        }

        $db_password = $hashpass->password;

        if($encryptMode=="BCRYPT"){
            $test = password_verify($password, $db_password);
        } else {
            $test = md5($password) == $db_password;
        }

        return $test;

    }
}

           