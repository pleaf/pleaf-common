<?php
namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use DB;
/**
 * @in
 *   - keyword
 */
class GetUsersDetailFromGroupMember implements BusinessFunction {

    public function getDescription(){
        return "Get Users Detail From Group Member";
    }

    public function execute($dto){

        $users = \DB::select("SELECT A.user_id,A.fullname,A.username
                 FROM t_user A
                 LEFT JOIN t_group_members B
                 ON A.user_id = B.user_id
                 WHERE A.user_id <> -1 AND NOT EXISTS (Select*from t_group_members where A.user_id = B.user_id )");

        return [
            "users" => $users
        ];
    }
}
