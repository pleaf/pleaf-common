<?php

namespace Sts\PleafCommon\BO;
use Sts\PleafCommon\Model\GroupMembers;
use Sts\PleafCore as PleafCore;
use Sts\PleafCore\BusinessTransaction;
use Sts\PleafCore\DefaultBusinessTransaction;
use Sts\PleafCore\DateUtil;
use Log;
use Validator;

class AddJoinMember extends DefaultBusinessTransaction implements BusinessTransaction {

    public function getDescription(){
    	return "Add Member to Selected Group User";
    }

    public function prepare ($dto, $originalDto){
        // Add validation here

    }

	public function process ($dto, $originalDto){
        $groupUserId = $dto['groupUserId'];
        $userId = $dto['userId'];
        $userLoginId = $dto['userLoginId'];
        $datetime = $dto['datetime'];


        if(count($userId)>0){
            foreach($userId as $data){
                $joinMember = new GroupMembers();
                $joinMember->group_user_id = $groupUserId;
                $joinMember->user_id = $data;
                
                self::auditInsert($joinMember, $userLoginId, $datetime);

                $joinMember->save();
            }
        }



	}

    protected function rules() {
        return [
            'userId' => 'required'
        ];
    }
}
