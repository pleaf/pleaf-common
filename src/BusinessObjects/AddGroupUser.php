<?php

namespace Sts\PleafCommon\BO;

use App;
use Sts\PleafCore\DefaultBusinessTransaction;
use Sts\PleafCore\BusinessTransaction;
use DB;
use Log;
use Sts\PleafCommon\Model\GroupUser;
use Sts\PleafCore\CoreException;




/**
 *
 * @in
 *
 * @out
 */
class AddGroupUser extends DefaultBusinessTransaction implements BusinessTransaction {

    public function getDescription(){
    	return "Add New Group User";
    }

    public function prepare ($dto, $originalDto){
        $this->valUniqueKey($dto);
    }

    private function valUniqueKey($dto){
        $findGroupUserByCode = App::make('findGroupUserByIndex');
            $groupUserCode = $dto['groupUserCode'];
            $tenantId = $dto['tenantId'];

            $input = [
              "groupUserCode"=>$groupUserCode,
              "tenantId"=> $tenantId
            ];

            $output = $findGroupUserByCode->execute($input);

            $groupUser = $output['groupUser'];

            //validasi business
            if($groupUser!=null)throw new CoreException(ERROR_BUSINESS_VALIDATION,[],
                    ["groupUserCode"=> "Group user with code ".$groupUserCode." already exists"]);

    }

    public function process ($dto, $originalDto){
        $tenantId = $dto['tenantId'];
        $groupUserCode = strtoupper($dto['groupUserCode']);
        $groupUserName = $dto['groupUserName'];
        $groupUserDesc = $dto['groupUserDesc'];
        $userId = $dto['userLoginId'];
        $datetime= $dto['datetime'];

        $data = new GroupUser();
        $data->tenant_id = $tenantId;
        $data->group_user_code = $groupUserCode;
        $data->group_user_name = $groupUserName;
        $data->group_user_desc = $groupUserDesc;

        self::auditActive($data, $datetime);
        self::auditInsert($data, $userId, $datetime);
        $data->save();

    }

    protected function rules() {
        return [
            'groupUserCode' => 'required',
            'groupUserName' =>  'required',
            'groupUserDesc' => 'required',
            'tenantId'=> 'required|val_tenant'

        ];
    }

}
