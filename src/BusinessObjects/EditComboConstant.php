<?php
namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessTransaction;
use Sts\PleafCore\DefaultBusinessTransaction;

use Sts\PleafCommon\Model\ComboConstant;

use Log;

class EditComboConstant extends DefaultBusinessTransaction implements BusinessTransaction {

    public function getDescription(){
        return "Edit Combo Constant";
    }

    public function prepare ($dto, $originalDto){

    }
    public function process ($dto, $originalDto){

        $time=$dto['datetime'];
        $userLoginId = $dto['userLoginId'];
		$combo_id = $dto['combo_id'];
        $code = $dto['code'];
        $param = ComboConstant::whereRaw("combo_id = '$combo_id' AND code = '$code'")
                            ->first();

        $comboconstant = ComboConstant::where('combo_id',$combo_id)
        ->where('code',$code)
       ->update([
            'prop_key'=>$dto['prop_key'],
            'code_group' => $dto['code_group'],
            'sort_no' => $dto['sort_no'],
            'update_datetime'=>$time,
            'update_user_id'=> $userLoginId,
            'version' => $param->version+1
       ]);

    }

    protected function rules(){
      return [
        'prop_key' => 'required',
        'code_group' => 'required',
        'sort_no' => 'required'
      ];
    }

}

//