<?php

namespace Sts\PleafCommon\BO;

use Sts\PleafCore as PleafCore;
use Sts\PleafCore\BusinessTransaction;
use Sts\PleafCore\DefaultBusinessTransaction;
use Sts\PleafCommon\Model\OU;


class RemoveOU extends DefaultBusinessTransaction implements BusinessTransaction {

    public function getDescription(){
    	return "Remove OU";
    }

    public function prepare ($dto, $originalDto){
        // Add validation here

    }
    
	public function process ($dto, $originalDto){
        $ou_id = $dto['ou_id'];

        $ou = OU::find($ou_id);
        if($ou!=null)$ou->delete();
        
	}
}