<?php

namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use Sts\PleafCore\DefaultBusinessFunction;
use Sts\PleafCore\CoreException;
use DB;
use Log;


/**
* @author  
* @in 
* @in('tenant_id', 'numeric', true, 'tenant id')
* @in('param_code', 'string', true, 'param code')
* @out
*/

class GetValueSystemConfigByParamCode extends DefaultBusinessFunction implements BusinessFunction {

    public function getDescription(){
        return "Mengambil system config";
    }

    public function process($dto){

        $recordOwnerId = $dto['record_owner_id'];
        $paramCode = $dto['param_code'];

        $result = DB::select("SELECT f_get_value_system_config_by_param_code(:recordOwnerId, :paramCode) as value", [
            "recordOwnerId"  => $recordOwnerId,
            "paramCode" => $paramCode
        ]);
       
        return $result[0]->value;
    }

}
