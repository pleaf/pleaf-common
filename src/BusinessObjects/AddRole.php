<?php

namespace Sts\PleafCommon\BO;

use Sts\PleafCore as PleafCore;
use Sts\PleafCore\BusinessTransaction;
use Sts\PleafCore\DefaultBusinessTransaction;
use Sts\PleafCommon\Model\Role;
use Sts\PleafCore\CoreException;
use App;

class AddRole extends DefaultBusinessTransaction implements BusinessTransaction {

    public function getDescription(){
    	return "Add Role";
    }

    public function prepare ($dto, $originalDto){
     	$this->valUniqKey($dto);
     }

    private function valUniqKey($dto){
        $findRoleByIndex = App::make("findRoleByIndex");
        $role_name = $dto["role_name"];
        $userTenant = $dto["tenant_id"];
        
        $input = [
            "tenant_id" => $userTenant,
            "role_name" => $role_name
        ];
     		$output = $findRoleByIndex->execute($input);
            if($output!=null)throw new CoreException(ERROR_BUSINESS_VALIDATION,[],
                    ["role_name"=> "Role name ".$role_name." is already exist"]);
        
    }

	public function process ($dto, $originalDto){
      $datetime = $dto['datetime'];
      $userLoginId = $dto['userLoginId'];
	  $roleName = strtoupper($dto["role_name"]);
      
      $role = new Role;
      $role->role_name = $roleName;
      $role->role_type = $dto['role_type'];
      $role->tenant_id = $dto['tenant_id'];

      self::auditActive($role, $datetime);
      self::auditInsert($role, $userLoginId, $datetime);

      $role->save();

	}
	
	protected function rules() {
		return [
				'role_name' => 'required',
                'tenant_id' => 'required|val_tenant'
		];
	}
	


}
