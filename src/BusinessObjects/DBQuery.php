<?php
namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use DB;
use Sts\PleafCore as PleafCore;
use Validator;
use Sts\PleafCore\CoreException;
use Sts\PleafCore\DefaultBusinessFunction;

/**
 * @author Congky 22, Maret 2016
 * @in
 *   - query
 * @out
 *   - query_list
 */
class DBQuery extends DefaultBusinessFunction implements BusinessFunction {
	
	public function getDescription(){
    	return "DB query";
    }

    public function process($dto){

    	$query = $dto['query'];
        
    	$query_list  = DB::select($query);
    	 
        return [
    		"query_list" => $query_list
    	];
    }

    protected function rules() {
        return [
            'query' => 'required'
        ];
    }
}