<?php

namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use Sts\PleafCore\DefaultBusinessFunction;
use Sts\PleafCommon\Model\Role;


/**
 *
 * @in
 *
 * @out
 */

class FindRoleByIndex extends DefaultBusinessFunction implements BusinessFunction {

    public function getDescription(){
        return "Find Role By Index";
    }

    public function process($dto){

        $tenant_id = $dto["tenant_id"];
        $role_name = strtoupper($dto["role_name"]);

        $role =  Role::where('tenant_id','=',$tenant_id)
                            ->where('role_name','=',$role_name)
                            ->first();
        
        return  $role;
		
    }

    public function rules(){

        return [
            "tenant_id" => "required|Integer",
            "role_name" => "required"
        ];

    }
}
