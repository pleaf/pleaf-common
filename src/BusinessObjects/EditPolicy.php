<?php
namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessTransaction;
use Sts\PleafCore\DefaultBusinessTransaction;

use Sts\PleafCommon\Model\Policy;


use Log;

class EditPolicy extends DefaultBusinessTransaction implements BusinessTransaction {

    public function getDescription(){
        return "Edit Policy";
    }

    public function prepare ($dto, $originalDto){

    }
    
    protected function rules(){
    	return [
    			"policy_name"=>"required"
    	];
    }
    
    public function process ($dto, $originalDto){
    	$userLoginId = $dto['userLoginId'];
    	$datetime = $dto['datetime'];    	 
		$policy_id = $dto['policy_id'];
        
        $policy = Policy::Find($policy_id);
        $policy->policy_name = $dto['policy_name'];
        
        self::auditUpdate($policy,$userLoginId,$datetime);        

        $policy->save();
    }
}