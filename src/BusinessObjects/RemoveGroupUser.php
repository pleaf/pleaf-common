<?php
namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessTransaction;
use Sts\PleafCore\DefaultBusinessTransaction;
use Sts\PleafCommon\Model\GroupUser;
use Log;
use DB;

class RemoveGroupUser extends DefaultBusinessTransaction implements BusinessTransaction {

    public function getDescription(){
        return "Delete Group User";
    }

    public function prepare ($dto, $originalDto){

    }
    public function process ($dto, $originalDto){

        $groupUserId = $dto['groupUserId'];

        $groupUser = GroupUser::where('group_user_id', '=', $groupUserId);

        if($groupUser !=null) $groupUser->delete();



    }
}
