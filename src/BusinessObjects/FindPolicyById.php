<?php
namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use Sts\PleafCore\DefaultBusinessFunction;
use DB;
use Sts\PleafCommon\Model\Policy;

/**
 * @in
 *   - keyword
 */
class FindPolicyById  extends DefaultBusinessFunction implements BusinessFunction {
	
	public function getDescription(){
    	return "Find Policy By Id";
    }

    protected function rules() {
    	return [
    			"tenant_id"=>"required|Integer",
    			"policy_id"=>"required|Integer"
    	];
        
    }
    
    public function process($dto){
    	 
        $policy_id = $dto['policy_id'];


        $policy = Policy::where('policy_id',$policy_id)->first();
        return[
            "edit_policy" => $policy

        ];
    }
}