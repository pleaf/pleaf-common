<?php

namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use DB;
use Log;

/**
 *
 * @in
 *
 * @out
 */
class GetOuCreateList implements BusinessFunction {

    public function getDescription(){
        return "Get Role List";
    }

    public function execute($dto){
        $tenantId = $dto['tenant_id'];

        $ou_list  = \DB::select("SELECT ou_id, tenant_id, ou_code, ou_name, ou_parent_id, ou_type_id,
                                       create_datetime, create_user_id, update_datetime, update_user_id,
                                       version, active, active_datetime, non_active_datetime
                                  FROM t_ou
                                  WHERE tenant_id = $tenantId;

         		");
        return [
            "ou_list" => $ou_list
        ];
    }
}