<?php

namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use DB;
use Sts\PleafCommon\Model\Role;
use Log;

/**
 * 
 * @in
 *
 * @out
 */
class FindRoleByIdForEdit implements BusinessFunction {

    public function getDescription(){
    	return "Find Role By Id For Edit";
    }

    public function execute($dto){
    	 $role_id   = $dto["role_id"];
         $tenant_id = $dto["tenant_id"];

    	 $role_list  = DB::SELECT(
             "
                    SELECT role_id, role_name, tenant_id, role_type, create_datetime, create_user_id,
                           update_datetime, update_user_id, version, active, active_datetime,
                           non_active_datetime
                    FROM t_role WHERE tenant_id = $tenant_id AND role_id = $role_id;

             "
         );
    	 
         return [
    	 	"role" => $role_list
    	 ];
    }
}