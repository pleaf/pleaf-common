<?php
namespace Sts\PleafCommon\BO;

use Sts\PleafCore as PleafCore;
use Sts\PleafCore\BusinessTransaction;
use Sts\PleafCore\DefaultBusinessTransaction;
use Sts\PleafCommon\Model\Tenant;
use Sts\PleafCommon\Model\User;
use App;
use Validator;
use Log;

class AddTenant extends DefaultBusinessTransaction implements BusinessTransaction {

    public function getDescription(){
    	return "Add Tenant";
    }

    protected function rules() {
        return [
            "tenant_code" => "required",
            "tenant_name" => "required",
            "email" => "email",
            "userTenantId"=> "required|Integer"
        ];
    }

    public function prepare ($dto, $originalDto){
      // Add validation here
    }

	  public function process ($dto, $originalDto){
           $findUserById = App::make("findUserById");
           $users = $findUserById->execute(["user_id" => $dto["user_id"]])["edit_user"];

            $pTenant_id = $dto["userTenantId"];
            $tenant_code = $dto['tenant_code'];
            $description = $dto['description'];
            $tenant_key = $dto['tenant_key'];
            $tenant_name = $dto['tenant_name'];
            $email = $dto['email'];
            $host = $dto['host'];
            $userLoginId = $dto['user_id'];
            $datetime = $dto['datetime'];

            $tenant = new Tenant();
            $tenant->tenant_code = $tenant_code;
            $tenant->description = $description;
            $tenant->tenant_key = $tenant_key;
            $tenant->tenant_name = $tenant_name;
            $tenant->email = $email;
            $tenant->host = $host;
            self::auditActive($tenant, $datetime);
            self::auditInsert($tenant, $userLoginId, $datetime);
            $tenant->save();

            if($users["tenant_id"] == -1){

                $users = User::find($dto["user_id"]);
                $users->tenant_id = $tenant->tenant_id;
                $users->update();
            }

            if(!is_null($dto["checked"])){
          \DB::Select("SELECT f_create_initial_tenant_data($pTenant_id,$userLoginId,'$datetime')");
//            \DB::Select("SELECT f_create_initial_tenant_data($tenant->tenant_id,$userLoginId,'$datetime')");

            }


	}


}
