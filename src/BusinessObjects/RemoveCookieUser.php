<?php

namespace Sts\PleafCommon\BO;

use Sts\PleafCore as PleafCore;
use Sts\PleafCore\BusinessTransaction;
use Sts\PleafCore\DefaultBusinessTransaction;
use Sts\PleafCommon\Model\CookieUser;
use Log;

class RemoveCookieUser extends DefaultBusinessTransaction implements BusinessTransaction {

    public function getDescription(){
    	return "changes here";
    }

    public function prepare ($dto, $originalDto){
        // Add Validation Business

    }
    
	public function process ($dto, $originalDto){
		
        // Put your code here
        $user_id = $dto['user_id'];
        $token = $dto['token'];
        $series_identifier=$dto['series_identifier'];

        $cookie = CookieUser::where('user_id',$user_id)
                                ->where('token',$token)
                                ->where('series_identifier',$series_identifier);

        if($cookie!=null) $cookie->delete();		

	}
}