<?php

namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use DB;
use Sts\PleafCommon\Model\ComboConstant;
use Log;

/**
 *
 * @in
 *
 * @out
 */
class FindComboConstantById implements BusinessFunction {

    public function getDescription(){
		return "Find Combo Constant By Id";
    }

    public function execute($dto){

      $combo_id  = $dto['combo_id'];
      $code  = $dto['code'];

      $comboconstant = ComboConstant::where('combo_id', '=', $combo_id)
                                     ->where('code', '=', $code)
                                     ->first();
         return [
    	 	"edit_combo_constant" => $comboconstant
    	 ];
    }
}
