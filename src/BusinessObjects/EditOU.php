<?php

namespace Sts\PleafCommon\BO;

use Sts\PleafCore as PleafCore;
use Sts\PleafCore\BusinessTransaction;
use Sts\PleafCore\DefaultBusinessTransaction;
use Sts\PleafCommon\Model\OU;

class EditOU extends DefaultBusinessTransaction implements BusinessTransaction {

    public function getDescription(){
    	return "Edit OU";
    }

    public function prepare ($dto, $originalDto){
        // Add validation here

    }
    
    protected function rules(){
    	return[
    			"ou_type_id"=>"required",
    			"ou_name"=>"required"
    	];
    }
    
	public function process ($dto, $originalDto){
		$ou_id = $dto['ou_id'];
		$userLoginId = $dto['userLoginId'];
		$datetime = $dto['datetime'];
		
        $ou = OU::Find($ou_id);
        $ou->ou_name = $dto['ou_name'];
        $ou->ou_type_id = $dto['ou_type_id'];
        $ou->ou_parent_id = $dto['ou_parent_id'];
        self::auditUpdate($ou,$userLoginId,$datetime);
        $ou->save();
	}
}