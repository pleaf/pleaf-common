<?php

namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use DB;
use Log;

/**
 * 
 * @in
 *
 * @out
 */
class GetOUTypeList implements BusinessFunction {

    public function getDescription(){
    	return "Get OU Type List";
    }

    public function execute($dto){

         $tenantId = $dto['tenant_id'];

    	 $outype_list  = DB::select("SELECT ou_type_code, ou_type_name, ou_type_id, CASE WHEN flg_bu = 'Y' THEN 'Yes' ELSE 'No' END AS flg_bu,  
                                     CASE WHEN flg_accounting = 'Y' THEN 'Yes' ELSE 'No' END AS flg_accounting, 
                                     CASE WHEN flg_legal = 'Y' THEN 'Yes' ELSE 'No' END AS flg_legal,
                                     CASE WHEN flg_sub_bu = 'Y' THEN 'Yes' ELSE 'No' END AS flg_sub_bu,
                                     CASE WHEN flg_branch = 'Y' THEN 'Yes' ELSE 'No' END AS flg_branch
                                     FROM t_ou_type
                                     WHERE tenant_id = $tenantId
                                     ORDER BY ou_type_name,ou_type_code asc");
    	 
         return [
    	 	"outype_list" => $outype_list
    	 ];
    }
}