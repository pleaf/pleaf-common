<?php

namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use Sts\PleafCore\QueryBuilder;
use Sts\PleafCore\CreateNativeQuery;
use DB;
use Log;
/**
 * @in 
 *  - parameter_id
 * @out
    - system_configuration(
            parameter_id
            parameter_code,
            parameter_value,
            parameter_group,
            parameter_description,
            active)
 */
class FindSystemConfigurationByIndex implements BusinessFunction {

    public function getDescription(){
        return "Find System Configuration By Index";
    }

    public function execute($dto){

        $parameter_code = $dto["parameter_code"];
        $tenant_id = $dto["tenant_id"];
        
        $queryBuilder = new QueryBuilder();
        $queryBuilder
            ->add(" SELECT ")
            ->add(" sc.tenant_id, ")
            ->add(" p.parameter_id, ")
            ->add(" p.parameter_code, ")
            ->add(" sc.parameter_value, ")
            ->add(" p.parameter_group, ")
            ->add(" p.parameter_desc, ")
            ->add(" p.active, ")
            ->add(" sc.version ")
            ->add(" FROM t_system_config sc ")
            ->add(" JOIN t_parameter p ON sc.parameter_id = p.parameter_id")
            ->add(" WHERE p.parameter_code = :parameter_code ")
            ->add(" AND sc.tenant_id = :tenant_id ");
        
        $query = new CreateNativeQuery($queryBuilder->toString());
        $query->setParameter("parameter_code", $parameter_code);
        $query->setParameter("tenant_id", $tenant_id);

        $result = $query->getResultList();

        if(is_null($result)) {
            $this->errorBusinessValidation(["error_system_configuration" => "Parameter Code ".$parameter_code." with tenant id ".$tenant_id." not found"]);
        }

        return [
            "system_configuration" => $result
        ];
    }

}
