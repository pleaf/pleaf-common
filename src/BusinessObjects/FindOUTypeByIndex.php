<?php
namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use Sts\PleafCore\DefaultBusinessFunction;
use DB;
use Sts\PleafCommon\Model\OUType;

/**
 * @in
 *   - keyword
 */
class FindOUTypeByIndex extends DefaultBusinessFunction implements BusinessFunction {
	
	public function getDescription(){
    	return "Find OU Type By Index";
    }
    
    public function process($dto){

        $OUTypeCode = strtoupper($dto['ou_type_code']);
        $tenant_id  = $dto['tenant_id'];


        $OUType = OUType::where('ou_type_code','=',$OUTypeCode)
                            ->where('tenant_id','=',$tenant_id)
                            ->first();
        return[
            "OUType" => $OUType
        ];
    }
    
    protected function rules() {
        return [
            "ou_type_code" => "required"
        ];
    }
}