<?php
namespace Sts\PleafCommon\BO;

use Sts\PleafCore as PleafCore;
use Sts\PleafCore\BusinessTransaction;
use Sts\PleafCore\DefaultBusinessTransaction;
use Log;
use App;
use Sts\PleafCommon\Model\PolicyTenant;

class RemovePolicyTenantUser extends DefaultBusinessTransaction implements BusinessTransaction {

    public function getDescription(){
        return "Remove Policy Tenant User";
    }

    public function prepare ($dto, $originalDto){


    }

    public function process ($dto, $originalDto){
        foreach($dto["policy_tenant_list"] as $value) {
            $policy_tenant = PolicyTenant::find($value["policy_tenant_id"]);
            $policy_tenant->delete();

        }

        return [];

    }

    protected function rules() {
        return [];
    }
}