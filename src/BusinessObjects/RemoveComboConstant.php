<?php
namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessTransaction;
use Sts\PleafCore\DefaultBusinessTransaction;
use Sts\PleafCommon\Model\ComboConstant;
use Log;
use DB;

class RemoveComboConstant extends DefaultBusinessTransaction implements BusinessTransaction {

    public function getDescription(){
        return "Remove Combo Constant";
    }

    public function prepare ($dto, $originalDto){
        // Add validation here
    }
    public function process ($dto, $originalDto){

        $combo_id = $dto['combo_id'];
        $code = $dto['code'];

        $comboconstant = ComboConstant::where('combo_id', '=', $combo_id)
                                      ->where('code', '=', $code);
        if($comboconstant!=null) $comboconstant->delete();

    }
}
