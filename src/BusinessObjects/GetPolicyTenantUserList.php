<?php

namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use DB;
use Log;
use Sts\PleafCore\DefaultBusinessFunction;

/**
 *
 * @in
 *
 * @out
 */
class GetPolicyTenantUserList extends DefaultBusinessFunction implements BusinessFunction {

    public function getDescription(){
        return "Get Policy Tenant User List";
    }

    public function process($dto){

        $user_id = $dto["user_id"];


        if($user_id == -1) {
            $policy_tenant_list  = DB::select("
                    SELECT *
                    FROM t_tenant A
            ");

        } else {
            $policy_tenant_list  = DB::select("
                    SELECT *
                    FROM t_policy_tenant A
                    INNER JOIN t_tenant B ON A.tenant_id = B.tenant_id
                    WHERE A.user_id = $user_id
                    ORDER BY B.tenant_name ASC
            ");

        }

        return [
            "policy_tenant_list" => $policy_tenant_list
        ];
    }
}
