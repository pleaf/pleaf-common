<?php
namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessTransaction;
use Sts\PleafCore\DefaultBusinessTransaction;
use Sts\PleafCommon\Model\User;
use Log;
use DB;

class RemoveUser extends DefaultBusinessTransaction implements BusinessTransaction {

    public function getDescription(){
        return "remove user";
    }

    public function prepare ($dto, $originalDto){

    }
    public function process ($dto, $originalDto){
        
        $user_id = $dto['user_id'];

        $user = User::find($user_id);
        if($user!=null)$user->delete();

            
    }
}