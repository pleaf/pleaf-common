<?php

namespace Sts\PleafCommon\BO;

use Sts\PleafCore as PleafCore;
use Sts\PleafCore\BusinessTransaction;
use Sts\PleafCore\DefaultBusinessTransaction;
use Sts\PleafCommon\Model\OUType;

class RemoveOUType extends DefaultBusinessTransaction implements BusinessTransaction {

    public function getDescription(){
    	return "Remove OU Type";
    }

    public function prepare ($dto, $originalDto){
        // Add validation here

    }
    
	public function process ($dto, $originalDto){
        $ou_type_id = $dto['ou_type_id'];

        $ou_type = OUType::find($ou_type_id);
        if($ou_type!=null)$ou_type->delete();
	}
}