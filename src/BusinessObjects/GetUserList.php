<?php
namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use DB;
/**
 * @in
 *   - keyword
 */
class GetUserList implements BusinessFunction {
    
    public function getDescription(){
        return "Get User List";
    }

    public function execute($dto){
        $tenantId = $dto['tenantId'];

         $user_list  = DB::select("
            SELECT A.user_id,A.username,A.fullname,A.email,A.role_default_id,A.ou_default_id,B.ou_name,
                    A.role_default_id,C.role_id,C.role_name,A.active,A.policy_default_id,D.policy_name,phone,
                    B.ou_id,D.policy_id,COUNT(E.user_login_id) AS total_login
            FROM t_user A
            LEFT JOIN t_ou B ON A.ou_default_id = B.ou_id
            LEFT JOIN t_role C ON A.role_default_id = C.role_id
            LEFT JOIN t_policy D ON A.policy_default_id = D.policy_id
            LEFT JOIN t_user_login E ON A.username = E.username
            WHERE A.user_id <> -1 AND A.tenant_id = $tenantId
            GROUP BY A.user_id,A.username,A.fullname,A.email,A.role_default_id,A.ou_default_id,B.ou_name,
                     A.role_default_id,C.role_id,C.role_name,A.active,A.policy_default_id,D.policy_name,phone,
                     B.ou_id,D.policy_id
            ORDER BY A.username ASC,A.fullname ASC
         ");
         
         return [
            "user_list" => $user_list
         ];
    }
}