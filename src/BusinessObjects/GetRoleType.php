<?php

namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use DB;
use Log;

/**
 *
 * @in
 *
 * @out
 */
class GetRoleType implements BusinessFunction {

    public function getDescription(){
        return "Get Role Type";
    }

    public function execute($dto){

        $tenant_id = $dto["tenant_id"];
        $role_name = $dto["role_name"];

        $role_list = \DB::Select("
                    SELECT role_id, role_name, tenant_id, role_type, create_datetime, create_user_id,
                           update_datetime, update_user_id, version, active, active_datetime,
                           non_active_datetime
                    FROM t_role WHERE role_name = '$role_name'
                    ORDER BY role_name ASC,role_type ASC;
        ");

        return [
            "role_list" => $role_list
        ];

    }
}
