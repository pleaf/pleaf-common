<?php
namespace Sts\PleafCommon\BO;

use Sts\PleafCore as PleafCore;
use Sts\PleafCore\BusinessTransaction;
use Sts\PleafCore\DefaultBusinessTransaction;
use Sts\PleafCommon\Model\UserRole;

/**
 * Class AddManageUserRole
 * @in
 *
 * @out
 * @package Sts\PleafCommon\BO
 */

class AddManageUserRole extends DefaultBusinessTransaction implements BusinessTransaction {

    public function getDescription(){
        return "Add Manage User";
    }

    public function prepare ($dto, $originalDto){

    }

    // validasi untuk Add input pada Policy
    protected function rules(){
        return[
            "user_id"      => "Integer|required",
            "role_id"      => "Integer|required",
            "policy_id"    => "Integer|required",
        ];

    }

    public function process ($dto, $originalDto){

        $user_id = $dto["user_id"];
        $role_id = $dto["role_id"];
        $policy_id = $dto["policy_id"];
        $userLoginId = $dto["userLoginId"];
        $datetime = $dto["datetime"];

        $userRole = new UserRole();
        $userRole->user_id = $user_id;
        $userRole->role_id = $role_id;
        $userRole->policy_id = $policy_id;
        self::auditActive($userRole,$datetime);
        self::auditInsert($userRole,$userLoginId,$datetime);
        $userRole->save();

    }
}