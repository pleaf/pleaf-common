<?php
namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessTransaction;
use Sts\PleafCore\DefaultBusinessTransaction;
use Sts\PleafCommon\Model\Tenant;
use Log;
use DB;

class RemoveTenant extends DefaultBusinessTransaction implements BusinessTransaction {

    public function getDescription(){
        return "Remove Tenant";
    }

    public function prepare ($dto, $originalDto){

    }
    public function process ($dto, $originalDto){

        $tenant_id = $dto['tenant_id'];

        $tenant = Tenant::find($tenant_id);
        if($tenant!=null)$tenant->delete();


    }
}
