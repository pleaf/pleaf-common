<?php

namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use DB;
use Log;

/**
 * 
 * @in
 *
 * @out
 */
class GetOUList implements BusinessFunction {

    public function getDescription(){
    	return "Get OU List";
    }

    public function execute($dto){

        $tenantId = $dto['tenant_id'];
        
         $ou_list  = \DB::select("SELECT A.ou_id,A.ou_parent_id, A.tenant_id, A.ou_code, A.ou_name, B.ou_type_code, B.ou_type_name,
                                    COALESCE((SELECT ou_name FROM t_ou where ou_id = A.ou_parent_id) ,'-') as ou_parent_name
                                    FROM t_ou a
                                    INNER JOIN t_ou_type b ON a.ou_type_id = b.ou_type_id
                                    WHERE a.tenant_id = $tenantId
                                    ORDER BY ou_name,ou_code asc
         		");
         return [	
            "ou_list" => $ou_list
         ];
    }
}