<?php

namespace Sts\PleafCommon\BO;


use Sts\PleafCore as PleafCore;
use Sts\PleafCore\BusinessTransaction;
use Sts\PleafCore\DefaultBusinessTransaction;
use Sts\PleafCommon\Model\UserLogin;
use Log;

class AddUserLogin extends DefaultBusinessTransaction implements BusinessTransaction {

    public function getDescription(){
        return "changes here";
    }

    public function prepare ($dto, $originalDto){
        // Add Validation Business

    }

    public function process ($dto, $originalDto){

        $userLogin = new UserLogin();
        $userLogin->username = $dto["username"];
        $userLogin->datetime_login = $dto["datetime"];
        $userLogin->status_login = _YES;
        $userLogin->error_login = ' ';
        $userLogin->session_id = md5(time());
        $userLogin->datetime_logout = ' ';
        $userLogin->save();

    }

    protected function rules(){

        return [
            "username" => "required",
            "datetime" => "required"
        ];
    }

}