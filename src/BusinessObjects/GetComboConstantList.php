<?php
namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use DB;
/**
 * @in
 *   - keyword
 */
class GetComboConstantList implements BusinessFunction {

    public function getDescription(){
        return "Get Combo Constant List";
    }

    public function execute($dto){

         //$keyword => $dto['keyword'];

         $combo_constant_list  = DB::select("SELECT combo_id, code, prop_key, code_group,
                                                    sort_no, create_datetime, create_user_id,
                                                    update_datetime, update_user_id, version
                                             FROM t_combo_value
                                             ORDER BY combo_id, sort_no;");

         return [
            "combo_constant_list" => $combo_constant_list
         ];
    }
}
