<?php

namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use DB;
/**
 * @in
 *   - keyword
 */
class GetPolicyList implements BusinessFunction {
	
	public function getDescription(){
    	return "Get Policy List";
    }

    public function execute($dto){

         $tenantId = $dto['tenant_id'];
    	 $policy_list  = DB::select("SELECT policy_id, tenant_id, policy_code, policy_name
    	 							FROM t_policy
    	 							WHERE tenant_id = $tenantId
    	 							ORDER BY policy_name ASC");
    	 
         return [
    	 	"policy_list" => $policy_list
    	 ];
    }
}