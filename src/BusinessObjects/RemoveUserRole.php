<?php
/**
 * Created by PhpStorm.
 * User: sts
 * Date: 11/04/16
 * Time: 10:31
 */
namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessTransaction;
use Sts\PleafCore\DefaultBusinessTransaction;
use Sts\PleafCommon\Model\UserRole;
use Log;
use DB;

class RemoveUserRole extends DefaultBusinessTransaction implements BusinessTransaction {

    public function getDescription(){
        return "remove user";
    }

    public function prepare ($dto, $originalDto){

    }
    public function process ($dto, $originalDto){

        $user_role_id = $dto['user_role_id'];

        $userRole = UserRole::find($user_role_id);
        if($userRole!=null)$userRole->delete();


    }
}