<?php

namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use Sts\PleafCore\DefaultBusinessFunction;
use Sts\PleafCommon\Model\UserLogin;
use Sts\PleafCore\CoreException;
use DB;
use Log;

/**
 * @author  Widana, Thursday, 12 May 2016 08:11:58 AM
 * @in
 *
 * @out
 */
class FindUserLoginByIndex extends DefaultBusinessFunction implements BusinessFunction {

    public function getDescription(){
    	return "write description here";
    }

    public function process($dto){

        $username = $dto["username"];

        $userLogin  = UserLogin::where("username", $username)->first();

        return $userLogin;

    }

    protected function rules(){

        return  [];

    }
}