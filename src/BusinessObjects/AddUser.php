<?php
namespace Sts\PleafCommon\BO;

use Illuminate\Support\Facades\Hash;
use Sts\PleafCore as PleafCore;
use Sts\PleafCore\BusinessTransaction;
use Sts\PleafCore\DefaultBusinessTransaction;
use Sts\PleafCommon\Model\User;
use Sts\PleafCommon\Model\UserRole;
use Validator;
use Log;

class AddUser extends DefaultBusinessTransaction implements BusinessTransaction {

    public function getDescription(){
    	return "Add User";
    }

    public function prepare ($dto, $originalDto){


    }
    
	public function process ($dto, $originalDto){
        $env=[
          'bcrypt'=>env('ENCRYPT_TYPE','BCRYPT')
        ];

        if($env['bcrypt']=='BCRYPT'){
            $this->bcrypt($dto, $originalDto);

        }else{
            $datetime = $dto['datetime'];
            $userLoginId = $dto['userLoginId'];
            $tenant_id = $dto['tenant_id'];

            $user = new User();
            $user->tenant_id = $tenant_id;
            $user->username = $dto['username'];
            $user->email = $dto['email'];
            $user->fullname = $dto['fullname'];
            $user->password = md5($dto['password']);
            $user->phone = $dto['phone'];
            $user->role_default_id = $dto['role_default_id'];
            $user->ou_default_id = $dto['ou_default_id'];
            $user->policy_default_id = $dto['policy_default_id'];
            self::auditActive($user,$datetime);
            self::auditInsert($user,$userLoginId,$datetime);
            $user->save();

            $userRole = new UserRole();
            $userRole->user_id = $user->user_id;
            $userRole->role_id = $dto["role_default_id"];
            $userRole->policy_id = $dto["policy_default_id"];
            self::auditActive($userRole,$datetime);
            self::auditInsert($userRole,$userLoginId,$datetime);
            $userRole->save();
        }





    }

    private function bcrypt($dto , $originalDto){
        $datetime = $dto['datetime'];
        $userLoginId = $dto['userLoginId'];
        $tenant_id = $dto['tenant_id'];
        $password = Hash::make($dto['password']);

        $user = new User();
        $user->tenant_id = $tenant_id;
        $user->username = $dto['username'];
        $user->email = $dto['email'];
        $user->fullname = $dto['fullname'];
        $user->password = $password;
        $user->phone = $dto['phone'];
        $user->role_default_id = $dto['role_default_id'];
        $user->ou_default_id = $dto['ou_default_id'];
        $user->policy_default_id = $dto['policy_default_id'];
        self::auditActive($user,$datetime);
        self::auditInsert($user,$userLoginId,$datetime);
        $user->save();

        $userRole = new UserRole();
        $userRole->user_id = $user->user_id;
        $userRole->role_id = $dto["role_default_id"];
        $userRole->policy_id = $dto["policy_default_id"];
        self::auditActive($userRole,$datetime);
        self::auditInsert($userRole,$userLoginId,$datetime);
        $userRole->save();
    }

    protected function rules(){
        return [
        	"tenant_id"=>"required|val_tenant",
            "username" => "required",
            "email" => "required|email",
            "fullname" => "required",
            "password" => "required",
            "role_default_id" => "required|numeric",
            "ou_default_id" => "required|numeric",
            "policy_default_id" => "required|numeric",
        ];
    }
}