<?php

namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use DB;
use Sts\PleafCommon\Model\ComboConstant;


class FindComboConstantByIndex implements BusinessFunction {

  public function getDescription(){
    return "Find Combo Constant By Index";
  }

  public function execute($dto){

    $combo_id = $dto['combo_id'];
    $code = strtoupper($dto['code']);

    $comboConstant = ComboConstant::where("combo_id", "=", $combo_id)
                                  ->where("code", "=", $code)
                                   ->first();

    return [
      "comboConstant" => $comboConstant
    ];
  }

}
