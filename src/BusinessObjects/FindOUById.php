<?php

namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use Sts\PleafCore\DefaultBusinessFunction;
use DB;
use Sts\PleafCommon\Model\OU;
use Log;

/**
 * 
 * @in
 *
 * @out
 */
class FindOUById extends DefaultBusinessFunction implements BusinessFunction {

    public function getDescription(){
    	return "Find OU By Id";
    }

    protected function rules() {
    	return [
    			"tenant_id"=>"required|Integer",
    			"ou_id"=>"required|Integer"
    	];
    }
    
    public function process($dto){
    	 
    	 $ou_id = $dto['ou_id'];
         $ou = OU::where('ou_id',$ou_id)->first();
    	 
         return [
    	 	"edit_ou" => $ou
    	 ];
    }
}