<?php
namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use DB;
/**
 * @in
 *   - keyword
 */
class GetMembersByGroupUserId implements BusinessFunction {

    public function getDescription(){
        return "Get Members By Group User Id";
    }

    public function execute($dto){

         $groupUserId = $dto['groupUserId'];

         $members = \DB::select("SELECT A.user_id,A.fullname,A.username
                 FROM t_user A
                 INNER JOIN t_group_members B
                 ON A.user_id = B.user_id
                 WHERE A.user_id <> -1 AND B.group_user_id = $groupUserId");

         return [
            "members" => $members
         ];
    }
}
