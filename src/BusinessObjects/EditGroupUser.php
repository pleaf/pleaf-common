<?php
namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessTransaction;
use Sts\PleafCore\DefaultBusinessTransaction;

use Sts\PleafCommon\Model\GroupUser;

use Log;

class EditGroupUser extends DefaultBusinessTransaction implements BusinessTransaction {

    public function getDescription(){
        return "Edit Group User";
    }

    public function prepare ($dto, $originalDto){

    }
    public function process ($dto, $originalDto){
        $time=$dto['datetime'];
        $userLoginId = $dto['userLoginId'];
        $groupUserId = $dto['groupUserId'];
		$groupUserName = $dto['groupUserName'];
        $groupUserDesc = $dto['groupUserDesc'];

        $groupUser = GroupUser::where('group_user_id',$groupUserId)
                        ->first();

        $groupUser->group_user_name = $groupUserName;
        $groupUser->group_user_desc = $groupUserDesc;

        self::auditUpdate($groupUser,$userLoginId,$time);

        $groupUser->update();

    }

    protected function rules() {
        return [
            'groupUserName' =>  'required',
            'groupUserDesc' => 'required'

        ];
    }
}
