<?php

namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use DB;
use Log;
use Sts\PleafCore\DefaultBusinessFunction;

/**
 *
 * @in
 *
 * @out
 */
class GetManageUserTenantList extends DefaultBusinessFunction implements BusinessFunction {

    public function getDescription(){
        return "Get Manage User Tenant List";
    }

    public function process($dto){

        $user_id = $dto["user_id"];

        $manage_user_tenant_list  = DB::select("
                SELECT *
                FROM t_tenant A
                WHERE NOT EXISTS (SELECT 1
                                  FROM t_policy_tenant B
                                  WHERE A.tenant_id = B.tenant_id
                                        AND B.user_id = $user_id)
      ");

        return [
            "manage_user_tenant_list" => $manage_user_tenant_list
        ];
    }
}