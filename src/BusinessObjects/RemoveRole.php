<?php

namespace Sts\PleafCommon\BO;

use Sts\PleafCore as PleafCore;
use Sts\PleafCore\BusinessTransaction;
use Sts\PleafCore\DefaultBusinessTransaction;
use Sts\PleafCore\DateUtil;
use Sts\PleafCommon\Model\Role,
    Sts\PleafCommon\Model\RoleTask;
use Log;
use Validator;

class RemoveRole extends DefaultBusinessTransaction implements BusinessTransaction {

    public function getDescription(){
    	return "Remove Role";
    }

    public function prepare ($dto, $originalDto){
        // Add validation here

    }

	public function process ($dto, $originalDto){

        // Put your code here
        $role_id  = $dto['role_id'];
//        RoleTask::FindOrFail($role_id)->delete();
        $role = Role::FindOrFail($role_id);
        if($role!=null)$role->delete();

	}
}
