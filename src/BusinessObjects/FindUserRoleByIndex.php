<?php
namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use DB;

/**
 * Class FindUserByIndex
 *
 * @In([])
 * @In(["user_id","bigint","false","find user role by user id"])
 *
 * @Out([])
 * @Out(["role_name","string","false","Role Name"])
 * @Out(["role_id","bigint","false","role_id"])
 * @Out(["user_role_id","bigint","false","user_role_id"])
 * @package Sts\PleafCommon\BO
 */

class FindUserRoleByIndex implements BusinessFunction {

    public function getDescription(){
        return "Find User Role By Index";
    }

    public function execute($dto){

        $user_id = $dto['user_id'];

        $user_role = DB::Select("SELECT * FROM t_user_role A
                                 INNER JOIN t_role B ON A.role_id = B.role_id
                                 WHERE user_id = $user_id
                            ");

        return [
            "list" => $user_role
        ];
    }
}