<?php

namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use DB;
use Sts\PleafCommon\Model\Role;
use Log;
use Sts\PleafCore\DefaultBusinessFunction;

/**
 *
 * @in
 * - role_id
 * - task_group
 * @out
 * - role_list
 */
class GetRoleTaskList extends DefaultBusinessFunction implements BusinessFunction {

    public function getDescription(){
    	return "Get role task list by role ID";
    }

    public function process($dto){

        $role_id = $dto['role_id'];

        $role_list = DB::select("SELECT B.role_id, A.task_id, A.task_code, A.task_name, A.description FROM t_task A
               LEFT OUTER JOIN t_role_task B ON A.task_id = B.task_id AND B.role_id = $role_id
               ORDER BY task_code");

        return [
            "role_list" => $role_list
        ];
    }
}
