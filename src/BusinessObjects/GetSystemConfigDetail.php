<?php
namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use DB;
/**
 * @in
 *   - keyword
 */
class GetSystemConfigDetail implements BusinessFunction {

    public function getDescription(){
        return "Get System Config Detail";
    }

    public function execute($dto){

        $parameterId = $dto['parameterId'];


        $getParameter = \DB::table('t_parameter as A')
            ->join('t_system_config as B','A.parameter_id','=','B.parameter_id')
            ->where('A.parameter_id',$parameterId)
            ->first();

        return [
            "parameterDetail" => $getParameter
        ];
    }
}
