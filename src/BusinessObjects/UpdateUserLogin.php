<?php

namespace Sts\PleafCommon\BO;

use Sts\PleafCore as PleafCore;
use Sts\PleafCore\BusinessTransaction;
use Sts\PleafCore\DefaultBusinessTransaction;
use Sts\PleafCommon\Model\UserLogin;
use Log;

class UpdateUserLogin extends DefaultBusinessTransaction implements BusinessTransaction {

    public function getDescription(){
        return "changes here";
    }

    public function prepare ($dto, $originalDto){
        // Add Validation Business

    }

    public function process ($dto, $originalDto){

        $username = $dto["username"];
        $status_login = _YES;

        $userLogin = UserLogin::whereRaw("username = '$username' AND status_login = '$status_login'")
            ->first();

        if($userLogin != null){
        	$userLogin->status_login = _NO;
        	$userLogin->datetime_logout = $dto["datetime"];
        	$userLogin->update();
        }

    }

    protected function rules(){

        return [
            "username" => "required",
            "datetime" => "required"
        ];
    }
}