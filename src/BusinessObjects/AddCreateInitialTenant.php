<?php

namespace Sts\PleafCommon\BO;

use Sts\PleafCore as PleafCore;
use Sts\PleafCore\BusinessTransaction;
use Sts\PleafCore\DefaultBusinessTransaction;
use Log;

/**
* @author
* @in([])
* @in(["tenant_id","bigint","false","tenant id Integer"])
* @in(["user_id","bigint","false","user id Integer"])
* @in(["datetime","string","false","datetime untuk create di dalam table"])
*
* @out([])
*/

class AddCreateInitialTenant extends DefaultBusinessTransaction implements BusinessTransaction {

    public function getDescription(){
    	return "changes here";
    }

    public function prepare ($dto, $originalDto){
        // Add Validation Business

    }
    
	public function process ($dto, $originalDto){

        $pTenantId  = $dto["userTenantId"];
        $pUserId    = $dto["user_id"];
        $pDatetime  = $dto["datetime"];

        return \DB::Select("f_create_initial_tenant_data($pTenantId,$pUserId,$pDatetime)");
//        return \DB::selectRaw("f_create_initial_tenant_data($pTenantId,$pUserId,$pDatetime)");

	}

    protected function rules(){
        //  Add Validation Data
        return [
                "tenant_id" => "required|Integer|val_tenant",
                "user_id" => "required|Integer",
                "datetime" => "required"
            ];
    }
}