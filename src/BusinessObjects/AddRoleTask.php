<?php

namespace Sts\PleafCommon\BO;

use Sts\PleafCore as PleafCore;
use Sts\PleafCore\BusinessTransaction;
use Sts\PleafCore\DefaultBusinessTransaction;
use Sts\PleafCommon\Model\RoleTask;
use Sts\PleafCore\Annotation as Annotation;

/**
 * Class AddRoleTask
 *
 * @In
 *
 * @Out
 * @package Sts\PleafCommon\BO
 */
class AddRoleTask extends DefaultBusinessTransaction implements BusinessTransaction {

    public function getDescription(){
    	return "Add role task";
    }

    protected function rules(){
        return [
            "role_id" => "required|numeric",
            "task_id" => "required|numeric",
        ];
    }

    public function prepare ($dto, $originalDto){
        // Add validation here

    }

	public function process ($dto, $originalDto){

         $role_id = $dto["role_id"];
         $task_id = $dto["task_id"];
         $userLoginId = $dto["userLoginId"];
         $datetime = $dto["datetime"];

         $roleTask = new RoleTask();
        
         $roleTask->role_id = $role_id;
         $roleTask->task_id = $task_id;

         self::auditActive($roleTask, $datetime);
         self::auditInsert($roleTask, $userLoginId, $datetime);

         $roleTask->save();

	}
}
