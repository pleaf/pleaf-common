<?php

namespace Sts\PleafCommon\Model;

use Sts\PleafCore\BusinessFunction;
use Sts\PleafCore\DefaultBusinessFunction;
use DB;
use Log;

/**
 * @author  Widana, Thursday, 12 May 2016 08:11:58 AM
 * @in
 *
 * @out
 */
class IsUserLoginByIndex extends DefaultBusinessFunction implements BusinessFunction {

    public function getDescription(){
        return "write description here";
    }

    public function process($dto){

        $user_id = $dto["user_id"]; 
		

        $result = [];
         
        $userlogin  = UserLogin::where("user_id", $user_id)->first();
        
        $isExists = false;
        if (!is_null($userlogin)) {
            $isExists = true;
            $result = ["userlogin_list" => $userlogin];
        }

        $result = ["exists"=>$isExists];

        return $result;
    }

    protected function rules() {

        return [];

    }

}