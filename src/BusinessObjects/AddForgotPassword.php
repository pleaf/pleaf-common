<?php

namespace Sts\PleafCommon\BO;
use Sts\PleafCommon\Model\ForgotPassword;
use Sts\PleafCore as PleafCore;
use Sts\PleafCore\BusinessTransaction;
use Sts\PleafCore\DefaultBusinessTransaction;
use Sts\PleafCore\DateUtil;
use Mail;
use URL;
use Log;
use Validator;

class AddForgotPassword extends DefaultBusinessTransaction implements BusinessTransaction {

    public function getDescription(){
    	return "changes here";
    }

    public function prepare ($dto, $originalDto){
        // Add validation here

    }
    
	public function process ($dto, $originalDto){
		
        // Put your code here		
        /*
        $tenant = new Tenant;
        $tenant->tenant_code = $dto['tenant_code'];
        $tenant->description = $dto['description'];
        $tenant->tenant_key = $dto['tenant_key'];
        $tenant->tenant_name = $dto['tenant_name'];
        $tenant->email = $dto['email'];
        $tenant->host = $dto['host'];
        $tenant->version = '0';
        $tenant->create_datetime= $times;
        $tenant->create_user_id='-1';
        $tenant->update_datetime= $times;
        $tenant->update_user_id='-1';
        $tenant->version='0';
        $tenant->active='Y';
        $tenant->active_datetime=$times;
        $tenant->non_active_datetime=' ';
        $tenant->save();
        */
        $reset = new ForgotPassword();
        $reset->user_email = $dto['email'];
        $reset->random_key = $dto['random_key'];
        $reset->version = 0;
        $reset->create_date_time = DateUtil::dateTimeNow();
        $reset->save();

        Mail::send('pleaf-common::Forgot.mail', ['email'=> $dto['email'] , 'link'=>URL::route('reset-password',[$dto['email'], $dto['random_key']])],function($message) use ($dto)
        {
            $message->from('shinosuke14@gmail.com');
            $message->to($dto['email'])->subject('Welcome!');
        });
	}
}