<?php
namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use DB;
/**
 * @in
 *   - keyword
 */
class GetSystemConfigList implements BusinessFunction {

    public function getDescription(){
        return "Get System Config List";
    }

    public function execute($dto){

        $tenantLoggedUser = $dto['tenantId'];


        $systemConfig_list  = DB::select("SELECT A.parameter_id, B.system_config_id, A.parameter_code, A.parameter_desc, A.parameter_group, A.parameter_data_type,
                                A.parameter_length, A.parameter_precision, A.parameter_editable, A.is_multiple, A.from_combo,
                                A.validation_rule, B.parameter_value, B.version
                                FROM t_parameter A
                                INNER JOIN t_system_config B ON A.parameter_id = B.parameter_id
                                WHERE B.tenant_id = $tenantLoggedUser
                                ORDER BY A.parameter_code, A.sort_no");

        return [
            "systemConfig_list" => $systemConfig_list
        ];
    }
}
