<?php

namespace Sts\PleafCommon\Model;

use Sts\PleafCore\BusinessFunction;
use Sts\PleafCore\DefaultBusinessFunction;
use DB;
use Log;

/**
 * @author  Widana, Thursday, 12 May 2016 08:11:59 AM
 * @in
 *
 * @out
 */
class ValUserLoginByIndex extends DefaultBusinessFunction implements BusinessFunction {

    public function getDescription(){
        return "write description here";
    }

    public function process($dto){

        $user_id = $dto["user_id"]; 
		
         
        $userlogin  = UserLogin::where("user_id", $user_id)->first();
        
        if (count($userlogin) == 0) {
            $errorList = [
               "data_userlogin" => "User login does not exists with Index ".$user_id
            ];
            $this->errorBusinessValidation($errorList);
        }

        return $userlogin;
    }

    protected function rules() {

        return [];

    }

}