<?php

namespace Sts\PleafCommon\BO;

use Sts\PleafCore as PleafCore;
use Sts\PleafCore\BusinessTransaction;
use Sts\PleafCore\DefaultBusinessTransaction;
use Sts\PleafCommon\Model\Role;
use Sts\PleafCore\CoreException;
use App;

class EditRole extends DefaultBusinessTransaction implements BusinessTransaction {

    public function getDescription(){
    	return "Edit Role";
    }

    public function prepare ($dto, $originalDto){

    }

    protected function rules(){
    	return[
    			"role_name"=>"required",
    			"ou_name"=>"role_type"
    	];
    }
    
	public function process ($dto, $originalDto){
        $datetime = $dto['datetime'];
        $userLoginId = $dto['userLoginId'];
        $role_id = $dto['role_id'];
        $roleName = strtoupper($dto["role_name"]);
        
        $role = Role::Find($role_id);
        $role->role_name = $roleName;
        $role->role_type = $dto["role_type"];

        self::auditUpdate($role,$userLoginId,$datetime);

        $role->update();

	}
}
