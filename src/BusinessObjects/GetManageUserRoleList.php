<?php
namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use DB;
use Sts\PleafCore\DefaultBusinessFunction;

/**
 * @in
 *   - keyword
 */
class GetManageUserRoleList extends DefaultBusinessFunction implements BusinessFunction {
    
    public function getDescription(){
        return "Get Manage User Role List";
    }

    public function process($dto){

      $tenantId = $dto['tenant_id'];
      $userId = $dto["user_id"];

      $manage_user_role_list  = \DB::select("SELECT A.user_role_id,C.tenant_id,B.policy_id, C.role_id, C.role_name, B.policy_name ,A.user_id
                                             FROM t_user_role A
                                             INNER JOIN t_policy B on A.policy_id = B.policy_id 
                                             INNER JOIN t_role C on A.role_id = C.role_id
                                             WHERE C.tenant_id = $tenantId
                                                    AND A.role_id <> -1
                                                    AND A.user_id = $userId
                                             ORDER BY C.role_name,B.policy_name ASC 
                                           ");
     return [
        "manage_user_list" => $manage_user_role_list
     ];
    }
}