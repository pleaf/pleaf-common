<?php

namespace Sts\PleafCommon\BO;

use Sts\PleafCore as PleafCore;
use Sts\PleafCore\BusinessTransaction;
use Sts\PleafCore\DefaultBusinessTransaction;
use Sts\PleafCommon\Model\OUType;


class EditOUType extends DefaultBusinessTransaction implements BusinessTransaction {

    public function getDescription(){
    	return "Edit OU Type";
    }

    public function prepare ($dto, $originalDto){
        // Add validation here

    }
    
    protected function rules(){
    	return [
    			"ou_type_name"=>"required",
    			"ou_type_id"=>"required"
    	];
    }
    
	public function process ($dto, $originalDto){
        $ou_type_id = $dto['ou_type_id'];
        $userLoginId = $dto['userLoginId'];
        $datetime = $dto['datetime'];
                
        $outype = OUType::Find($ou_type_id);
        $outype -> ou_type_name = $dto['ou_type_name'];
        
        self::auditUpdate($outype,$userLoginId,$datetime);

        $outype->save();
	}
}