<?php
namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use DB;
use Sts\PleafCommon\Model\User;

/**
 * @in
 * user_id

 * @out
 * username
 * role
 * policy
 *
 */
class FindUserByIndex implements BusinessFunction {

	public function getDescription(){
    	return "Find User By Index";
    }

    public function execute($dto){

        $user_id = $dto['user_id'];

        $user = DB::Select("
                SELECT A.username,A.email,A.phone,A.fullname,C.policy_code,C.policy_code,
                        D.role_name,D.role_type FROM t_user A
                INNER JOIN t_user_role B ON A.user_id = b.user_id
                LEFT JOIN t_policy C ON C.policy_id = B.policy_id
                LEFT JOIN t_role D ON D.role_id = B.role_id
                WHERE A.user_id = $user_id
        ");
        return[

            "manage_user_list" => $user

        ];
    }
}