<?php

namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use DB;
use Log;

/**
 * 
 * @in
 *
 * @out
 */
class GetLoggedUserData implements BusinessFunction {

    public function getDescription(){
    	return "Get Logged User Data";
    }

    public function execute($dto){
    	 
    	 $dataUser  = DB::table('t_user')->where('user_id',$dto['user_id'])->first();
    	 
         return [
    	 	"dataUser" => $dataUser
    	 ];
    }
}