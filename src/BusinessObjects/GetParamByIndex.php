<?php

namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use DB;
use Log;
/**
 * @in param_code
 *   
 */
class GetParamByIndex implements BusinessFunction {

    public function getDescription(){
        return "Get Config Param";
    }

    public function execute($dto){
       
        $param  = collect(DB::select("SELECT * FROM t_parameter 
            WHERE parameter_code LIKE '".$dto['param_code']."'"))->first();
        

        return ["id" => $param->parameter_id, "code" => 'parameter_code'];
    }
}
