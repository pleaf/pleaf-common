<?php
namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessTransaction;
use Sts\PleafCore\DefaultBusinessTransaction;
use Sts\PleafCommon\Model\User;

class EditUser extends DefaultBusinessTransaction implements BusinessTransaction {

    public function getDescription(){
        return "Edit User";
    }

    public function prepare ($dto, $originalDto){

    }
    public function process ($dto, $originalDto){


        $userLoginId = $dto["userLoginId"];
        $datetime = $dto["datetime"];

        $user = User::Find($dto["user_id"]);

        $user->username = $dto["username"];
        $user->email    = $dto["email"];
        $user->fullname = $dto["fullname"];
        $user->phone    = $dto["phone"];
        $user->role_default_id  = $dto["role_default_id"];
        $user->ou_default_id    = $dto["ou_default_id"];
        $user->policy_default_id = $dto["policy_default_id"];

        self::auditUpdate($user, $userLoginId, $datetime);

        $user->update();

        return $user;

    }

    protected function rules(){
        return [
            "username" => "required",
            "email" => "required|email",
            "fullname" => "required",
            "role_default_id" => "required|numeric",
            "ou_default_id" => "required|numeric",
            "policy_default_id" => "required|numeric",
        ];
    }

}