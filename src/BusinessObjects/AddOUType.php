<?php
namespace Sts\PleafCommon\BO;

use Sts\PleafCore as PleafCore;
use Sts\PleafCore\BusinessTransaction;
use Sts\PleafCore\DefaultBusinessTransaction;
use Sts\PleafCommon\Model\OUType;
use Sts\PleafCore\CoreException;
use App;

class AddOUType extends DefaultBusinessTransaction implements BusinessTransaction {

    public function getDescription(){
    	return "Add OU Type";
    }

    public function prepare ($dto, $originalDto){
     	$this->valUniqKey($dto);
     }
    
     private function valUniqKey($dto){
     	$findOUTypeByIndex = App::make('findOUTypeByIndex');
     	$userTenant = $dto['tenant_id'];
    	$OUTypeCode = strtoupper($dto['ou_type_code']);
    	
     	$input = [
     			"tenant_id"=>$userTenant,
     			"ou_type_code"=>$OUTypeCode
   
     	];
    	 
     	
     		$output = $findOUTypeByIndex->execute($input);
     		$OUType = $output['OUType'];
            if($OUType!=null)throw new CoreException(ERROR_BUSINESS_VALIDATION,[],
                    ["ou_type_code"=> "OU Type code ".$OUTypeCode." is already exist"]);
     		
     }
        
	public function process ($dto, $originalDto){
		$userLoginId = $dto["userLoginId"];
		$datetime = $dto["datetime"];
		$OUTypeCode = strtoupper($dto['ou_type_code']);
		
        $ou_type = new OUType;
        $ou_type->tenant_id = $dto['tenant_id'];
        $ou_type->ou_type_code = $OUTypeCode;
        $ou_type->ou_type_name = $dto['ou_type_name'];

        if($dto['flg_bu']=='')
            $ou_type->flg_bu='N';
        else
            $ou_type->flg_bu=$dto['flg_bu'];

        if($dto['flg_legal']=='')
            $ou_type->flg_legal='N';
        else
            $ou_type->flg_legal=$dto['flg_legal'];       
        
        if($dto['flg_accounting']=='')
            $ou_type->flg_accounting='N';
        else
            $ou_type->flg_accounting=$dto['flg_accounting'];
        
        if($dto['flg_sub_bu']=='')
            $ou_type->flg_sub_bu='N';
        else
            $ou_type->flg_sub_bu=$dto['flg_sub_bu'];
        
        if($dto['flg_branch']=='')
            $ou_type->flg_branch='N';
        else
            $ou_type->flg_branch=$dto['flg_branch'];
        		
        self::auditActive($ou_type, $datetime);
        self::auditInsert($ou_type, $userLoginId, $datetime);
        
        $ou_type->save();
	}
	
	protected function rules(){
		return[
				"tenant_id"=>"required|val_tenant",
				"ou_type_code"=>"required",
				"ou_type_name"=>"required"
		];
		 
	}
	
}