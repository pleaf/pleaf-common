<?php

namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use Sts\PleafCore\DefaultBusinessFunction;
use DB;
use Sts\PleafCommon\Model\OUType;
use Log;

/**
 * 
 * @in
 *
 * @out
 */
class FindOUTypeById extends DefaultBusinessFunction implements BusinessFunction {

    public function getDescription(){
    	return "Find OU Type By Id";
    }
    
    protected function rules() {
    	return [
    			"tenant_id"=>"required|Integer",
    			"ou_type_id"=>"required|Integer"
    	];
    }
    

    public function process($dto){
    	 
    	 $ou_type_id  = $dto['ou_type_id'];
    	 $outype = OUType::where('ou_type_id',$ou_type_id)->first();
         return [
    	 	"edit_ou_type" => $outype
    	 ];
    }
}