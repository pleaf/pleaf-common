<?php

namespace Sts\PleafCommon\BO;

use Sts\PleafCore as PleafCore;
use Sts\PleafCore\BusinessTransaction;
use Sts\PleafCore\DefaultBusinessTransaction;
use Sts\PleafCore\DateUtil;
use Sts\PleafCommon\Model\RoleTask;
use Log;
use Validator;

class RemoveRoleTask extends DefaultBusinessTransaction implements BusinessTransaction {

    public function getDescription(){
    	return "Remove role task";
    }

    protected function rules(){
        return [
            "role_id" => "required|numeric",
            "task_id" => "required|numeric",
        ];
    }

    public function prepare ($dto, $originalDto){
        // Add validation here

    }
    
	public function process ($dto, $originalDto){
        $role_id = $dto["role_id"];
        $task_id = $dto["task_id"];

        $role_task = RoleTask::where("role_id","=", $role_id)
                        ->where("task_id","=", $task_id);
        if($role_task!=null) $role_task->delete();

        return [];
	}
}