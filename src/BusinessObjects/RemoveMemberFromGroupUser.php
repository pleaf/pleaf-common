<?php
namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessTransaction;
use Sts\PleafCore\DefaultBusinessTransaction;
use Sts\PleafCommon\Model\GroupMembers;
use Log;
use DB;

class RemoveMemberFromGroupUser extends DefaultBusinessTransaction implements BusinessTransaction {

    public function getDescription(){
        return "Delete Member From Group User";
    }

    public function prepare ($dto, $originalDto){

    }
    public function process ($dto, $originalDto){

        $groupUserId = $dto['groupUserId'];
        $userId = $dto['userId'];

        if(count($userId)>0){
            foreach($userId as $data){
                $member = GroupMembers::where('user_id','=',$data);
                if($member!=null) $member->delete();

            }
        }




    }

    protected function rules() {
        return [
            'userId' => 'required'
        ];
    }
}
