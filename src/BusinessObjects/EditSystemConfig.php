<?php

namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessTransaction;
use Sts\PleafCore\DefaultBusinessTransaction;
use DB;
use Sts\PleafCommon\Model\Parameter;
use Sts\PleafCommon\Model\SystemConfig;
use Log;
use Sts\PleafCommon\BO\SystemConfig AS SystemConfigModel;


/**
 * Class EditSystemConfig
 * @package Sts\PleafCommon\BO
 */
class EditSystemConfig extends DefaultBusinessTransaction implements BusinessTransaction {

    public function getDescription(){
    	return "Edit System Config";
    }

    public function prepare ($dto, $originalDto){

    }

    public function process ($dto, $originalDto){
        $parameterId = $dto['parameterId'];
        $userLoginId = $dto['userLoginId'];
        $systemConfigId = $dto['systemConfigId'];
        $datetime = $dto['datetime'];

        $parameter = Parameter::find($parameterId);
        $parameter->parameter_def_value = $dto['parameterValue'];
        self::auditUpdate($parameter,$userLoginId,$datetime);

        $systemConfig = SystemConfig::find($systemConfigId);
        $systemConfig->parameter_value = $parameter->parameter_def_value;
        self::auditUpdate($systemConfig,$userLoginId,$datetime);

        $parameter->update();
        $systemConfig->update();

        return [
            "systemConfig" => $parameter
        ];
    }

    protected function rules() {
        return [
            'parameterValue' => 'required'

        ];
    }
}
