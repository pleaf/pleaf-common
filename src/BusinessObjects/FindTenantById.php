<?php
namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use Sts\PleafCore\DefaultBusinessFunction;
use DB;
use Sts\PleafCommon\Model\Tenant;

/**
 * @author Unknown
 * Modify By Congky
 * @in
 *   - tenant_id
 * @out
 *   - List : edit_tenant
 */
class FindTenantById extends DefaultBusinessFunction implements BusinessFunction {
	
	public function getDescription(){
    	return "Find Tenant By Id";
    }

    public function process($dto){
    	 
        $tenant_id = $dto['tenant_id'];

        $tenant = Tenant::where('tenant_id',$tenant_id)->first();
        return[
            "edit_tenant" => $tenant
        ];
    }

    protected function rules() {
        return [
            "tenant_id" => "required|Integer"
        ];
    }
}