<?php

namespace Sts\PleafCommon\BO;

use Sts\PleafCommon\Model\OU;
use Sts\PleafCore as PleafCore;
use Sts\PleafCore\BusinessTransaction;
use Sts\PleafCore\DefaultBusinessTransaction;
use Sts\PleafCore\CoreException;
use App;

class AddOU extends DefaultBusinessTransaction implements BusinessTransaction {

    public function getDescription(){
    	return "Add OU";
    }

    public function prepare ($dto, $originalDto){
        $this->valUniqueKey($dto);

    }
    
    private function valUniqueKey($dto){
    	$findOUByIndex = App::make('findOUByIndex');
    	$userTenant = $dto["tenant_id"];
		$OUcode = $dto["ou_code"];
		
    	$input = [
    			"tenant_id"=>$userTenant,
    			"ou_code"=>$OUcode
    			 
    	];

    	$output = $findOUByIndex->execute($input);
    	$OU = $output['OU'];
    	if($OU!=null)throw new CoreException(ERROR_BUSINESS_VALIDATION,[],
    			["ou_code"=> "OU code ".$OUcode." is already exist"]);
    	 
    }
    
	public function process ($dto, $originalDto){
		$userLoginId = $dto["userLoginId"];
		$datetime = $dto["datetime"];
		$OUcode = strtoupper($dto["ou_code"]);	
				
        $ou= new OU;
        $ou->ou_parent_id=$dto['ou_parent_id'];
        $ou->tenant_id=$dto['tenant_id'];
        $ou->ou_code=$OUcode;
        $ou->ou_name=$dto['ou_name'];
        $ou->ou_type_id=$dto['ou_type_id'];        
        
        self::auditActive($ou, $datetime);
        self::auditInsert($ou, $userLoginId, $datetime);
                
        $ou->save();
	}
	
	protected function rules(){
		return[
				"tenant_id"=>"required|val_tenant",
				"ou_code"=>"required",
				"ou_type_id"=>"required",
				"ou_name"=>"required"
		];
	}
	
}