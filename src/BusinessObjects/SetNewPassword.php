<?php

namespace Sts\PleafCommon\BO;

use Illuminate\Support\Facades\Hash;
use Sts\PleafCore as PleafCore;
use Sts\PleafCore\BusinessTransaction;
use Sts\PleafCore\DefaultBusinessTransaction;
use Sts\PleafCore\DateUtil;
use Illuminate\Support\Facades\DB;
use Log;
use Validator;

class SetNewPassword extends DefaultBusinessTransaction implements BusinessTransaction {

    public function getDescription(){
    	return "changes here";
    }

    public function prepare ($dto, $originalDto){
        // Add validation here

    }
    
	public function process ($dto, $originalDto){
		
        // Put your code here		
        /*
        $tenant = new Tenant;
        $tenant->tenant_code = $dto['tenant_code'];
        $tenant->description = $dto['description'];
        $tenant->tenant_key = $dto['tenant_key'];
        $tenant->tenant_name = $dto['tenant_name'];
        $tenant->email = $dto['email'];
        $tenant->host = $dto['host'];
        $tenant->version = '0';
        $tenant->create_datetime= $times;
        $tenant->create_user_id='-1';
        $tenant->update_datetime= $times;
        $tenant->update_user_id='-1';
        $tenant->version='0';
        $tenant->active='Y';
        $tenant->active_datetime=$times;
        $tenant->non_active_datetime=' ';
        $tenant->save();
        */

        $env = [
          "bcrypt"=>env("ENCRYPT_TYPE","BCRYPT")
        ];
        if($env["bcrypt"]=="BCRYPT"){

            $check_user = DB::table('t_user')->where('user_id',$dto['user_id'])
                ->get();


            $countData = count($check_user);


            if($countData==0)
            {
                Log::debug("Old Pass Wrong");
                return redirect('admin/changePasswords')->with("message","Old Password Wrong");

            }
            else {
                $user = DB::table('t_user')->where('user_id', $dto['user_id'])
                    ->update([
                        "password" => Hash::make($dto['new_password'])

                    ]);

                Log::debug("Success Update");
                return redirect('admin/changePasswords')->with("successMessage", "Change Password Success!");
            }
        }else{
            $check_user = DB::table('t_user')->where('user_id',$dto['user_id'])
                ->where('password',md5($dto['password']))->get();
            $countData = count($check_user);

            if($countData==0)
            {
                Log::debug("Old Pass Wrong");
                return redirect('admin/changePasswords')->with("message","Old Password Wrong");

            }
            else
            {
                $user = DB::table('t_user')->where('user_id',$dto['user_id'])
                    ->where('password',md5($dto['password']))
                    ->update([
                        "password"=>md5($dto['new_password'])

                    ]);

                Log::debug("Success Update");
                return redirect('admin/changePasswords')->with("successMessage","Change Password Success!");


            }

        }

	}
}