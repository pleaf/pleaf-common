<?php
namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use DB;
use Sts\PleafCommon\Model\Tenant;
use Log;

/**
 * Class GetTenantList
 * @In([])
 * @In(["tenant_code","string","false","tenant code"])
 * @In(["tenant_name","string","false","tenant name"])
 * @In(["email","string","false","email unique"])
 * @In(["description","string","false","description"])
 * @In(["host","string","true","host"])

 * @Out([])
 * @Out(["tenant_code","string","false","tenant code"])
 * @Out(["tenant_name","string","false","tenant name"])
 * @Out(["email","string","false","email unique"])
 * @Out(["description","string","false","description"])
 * @Out(["host","string","true","host"])

 * @package Sts\PleafCommon\BO
 */

class GetTenantList implements BusinessFunction {

	public function getDescription(){
    	return "Get Tenant List";
    }

    public function execute($dto){

        $tenant_list  = DB::select("
          SELECT tenant_id,tenant_code,tenant_name,description,email,host
          FROM t_tenant
          ORDER BY tenant_code ASC,tenant_name ASC");

         return [
    	 	"tenant_list" => $tenant_list
    	 ];
    }
}
