<?php
namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use DB;
use Sts\PleafCommon\Model\Policy;

/**
 * @in
 *   - keyword
 */
class FindPolicyByIndex implements BusinessFunction {
	
	public function getDescription(){
    	return "Find Policy By Index";
    }

    public function execute($dto){
        $policy_code = strtoupper($dto['policy_code']);
        $tenantId = $dto['tenant_id'];

        $policy = Policy::where('policy_code','=',$policy_code)
                        ->where('tenant_id','=',$tenantId)
                        ->first();
        return[
            "edit_policy" => $policy

        ];
    }
}