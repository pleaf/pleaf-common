<?php

namespace Sts\PleafCommon\BO;

use Illuminate\Support\Facades\Hash;
use Sts\PleafCore as PleafCore;
use Sts\PleafCore\BusinessTransaction;
use Sts\PleafCore\DefaultBusinessTransaction;
use Sts\PleafCore\DateUtil;
use DB;
use Log;
use Validator;

class UpdateUserForgot extends DefaultBusinessTransaction implements BusinessTransaction {

    public function getDescription(){
    	return "changes here";
    }

    public function prepare ($dto, $originalDto){
        // Add validation here

    }
    
	public function process ($dto, $originalDto){
		
        // Put your code here		
        /*
        $tenant = new Tenant;
        $tenant->tenant_code = $dto['tenant_code'];
        $tenant->description = $dto['description'];
        $tenant->tenant_key = $dto['tenant_key'];
        $tenant->tenant_name = $dto['tenant_name'];
        $tenant->email = $dto['email'];
        $tenant->host = $dto['host'];
        $tenant->version = '0';
        $tenant->create_datetime= $times;
        $tenant->create_user_id='-1';
        $tenant->update_datetime= $times;
        $tenant->update_user_id='-1';
        $tenant->version='0';
        $tenant->active='Y';
        $tenant->active_datetime=$times;
        $tenant->non_active_datetime=' ';
        $tenant->save();
        */
       $env = [
         "bcrypt"=>env("ENCRYPT_TYPE","BCRYPT")
       ];
        if($env["bcrypt"]=="BCRYPT"){
            $email = $dto['email'];

            Log::debug($email);

            $user = DB::table('t_user')->where('email',$email)
                ->update(["password"=>Hash::make($dto['password'])]);

            Log::debug($user);
        }else{
            $email = $dto['email'];

            Log::debug($email);

            $user = DB::table('t_user')->where('email',$email)
                ->update(["password"=>md5($dto['password'])]);

            Log::debug($user);
        }



	}
}