<?php

namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use DB;
use Log;

/**
 *
 * @in
 *
 * @out
 */
class GetRoleListForManagerRole implements BusinessFunction {

    public function getDescription(){
        return "Get Role List";
    }

    public function execute($dto){

        $tenant_id = $dto["tenant_id"];
        $user_id = $dto["user_id"];

        $role_list = \DB::Select("
                SELECT *
                FROM t_role A
                WHERE NOT EXISTS (SELECT 1
                                  FROM t_user_role B
                                  WHERE A.role_id = B.role_id
                                    AND B.user_id = $user_id)
                    AND A.tenant_id <> -1;
        ");

        return [
            "role_list" => $role_list
        ];

    }

}