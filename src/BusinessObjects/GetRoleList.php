<?php

namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use DB;
use Log;

/**
 * 
 * @in
 *
 * @out
 */
class GetRoleList implements BusinessFunction {

    public function getDescription(){
    	return "Get Role List";
    }

    public function execute($dto){

        $tenant_id = $dto["tenant_id"];
        $role_list = \DB::Select("
                        SELECT A.role_id, A.role_name, A.tenant_id, A.role_type,COALESCE(COUNT(B.role_id),0) AS detail,
                               A.create_datetime, A.create_user_id,A.update_datetime, A.update_user_id, A.version,
                               A.active, A.active_datetime,A.non_active_datetime
                        FROM t_role A LEFT JOIN t_user_role B ON A.role_id = B.role_id
                        WHERE A.tenant_id = $tenant_id AND A.role_id <> -1
                        GROUP BY A.role_id, A.role_name, A.tenant_id, A.role_type, A.create_datetime, A.create_user_id,
                               A.update_datetime, A.update_user_id, A.version, A.active, A.active_datetime,
                               A.non_active_datetime
                        ORDER BY A.role_name ASC , A.role_type ASC
        ");

        return [
            "role_list" => $role_list
        ];

    }

}