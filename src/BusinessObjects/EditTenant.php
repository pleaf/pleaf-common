<?php
namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessTransaction;
use Sts\PleafCore\DefaultBusinessTransaction;

use Sts\PleafCommon\Model\Tenant;


use Log;

class EditTenant extends DefaultBusinessTransaction implements BusinessTransaction {

    public function getDescription(){
        return "Edit Tenant";
    }

    public function prepare ($dto, $originalDto){

    }
    public function process ($dto, $originalDto){
        
        $tenant_id = $dto['tenant_id'];
        $userLoginId = $dto['userLoginId'];
        $datetime = $dto['datetime'];
        
        $tenant = Tenant::where('tenant_id',$tenant_id)
                        ->first();
        $tenant->description = $dto['description'];
        $tenant->tenant_name = $dto['tenant_name'];
        $tenant->email = $dto['email'];
        $tenant->host = $dto['host'];
        self::auditUpdate($tenant,$userLoginId,$datetime);
        $tenant->update();

    }

    protected function rules() {
        return [
          "tenant_id" => "required|integer|val_tenant",
          "tenant_name" => "required",
          "email" => "email"
        ];
    }
}