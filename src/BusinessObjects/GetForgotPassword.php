<?php

namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use DB;
use Log;

/**
 * 
 * @in
 *
 * @out
 */
class GetForgotPassword implements BusinessFunction {

    public function getDescription(){
    	return "get data forgot password";
    }

    public function execute($dto){
    	 
    	 $forgot_password_list  = DB::select("SELECT * FROM t_forgot_password 
                                WHERE user_email LIKE '%".$dto['email']."%' 
                                AND random_key LIKE '%".$dto['random_key']."%' ");
    	 
         if(!empty($forgot_password_list))
         {
            
            return [
                "status" => "OK"
            ];
         }
         else
         {
            return [
                "status" => "NOPE"
            ];
         }
         
    }
}