<?php

namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use DB;
use Log;

/**
 * 
 * @in
 *
 * @out
 */
class GetListRole implements BusinessFunction {

    public function getDescription(){
    	return "Get Role List";
    }

    public function execute($dto){

         $list_role  = DB::select("
                      SELECT role_id,role_name,tenant_id,role_type
                      FROM t_role                      ");

         return [
    	 	"list_role" => $list_role
    	 ];
    }
}