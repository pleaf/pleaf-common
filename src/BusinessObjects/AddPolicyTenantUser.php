<?php
namespace Sts\PleafCommon\BO;

use Sts\PleafCore as PleafCore;
use Sts\PleafCore\BusinessTransaction;
use Sts\PleafCore\DefaultBusinessTransaction;
use Log;
use App;
use Sts\PleafCommon\Model\PolicyTenant;

class AddPolicyTenantUser extends DefaultBusinessTransaction implements BusinessTransaction {

    public function getDescription(){
        return "Add Policy Tenant User";
    }

    public function prepare ($dto, $originalDto){

        $findUserById = App::make("findUserByIdCommon");
        $findTenantById = App::make("findTenantById");

        $user_dto = [
            "user_id" => $dto["user_id"]
        ];

        $findUserById->execute($user_dto);

        foreach($dto["tenant_list"] as $value) {

            $tenant_dto = [
                "tenant_id" => $value["tenant_id"]
            ];

            $findTenantById->execute($tenant_dto);

        }

    }

    public function process ($dto, $originalDto){
        foreach($dto["tenant_list"] as $value) {
            $policy_tenant = new PolicyTenant();
            $policy_tenant->user_id = $dto["user_id"];
            $policy_tenant->tenant_id = $value["tenant_id"];
            self::auditActive($policy_tenant, $dto["datetime"]);
            self::auditInsert($policy_tenant, $dto["user_login_id"], $dto["datetime"]);
            $policy_tenant->save();

        }

        return [];

    }

    protected function rules() {
        return [];
    }
}