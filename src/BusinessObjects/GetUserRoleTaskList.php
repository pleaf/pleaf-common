<?php

namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use DB;
use Log;

/**
 * 
 * @in
 *
 * @out
 */
class GetUserRoleTaskList implements BusinessFunction {

    public function getDescription(){
    	return "change here";
    }

    public function execute($dto){

        $role_id = $dto["role_id"];

        $role_task = \DB::Select("
            SELECT B.task_code,B.task_name FROM t_role_task A
            INNER JOIN t_task B ON A.task_id = B.task_id
            WHERE role_id = $role_id
        ");

        return [
    	 	"role_task" => $role_task
    	 ];
    }
}