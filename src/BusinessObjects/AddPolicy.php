<?php
namespace Sts\PleafCommon\BO;

use Sts\PleafCore as PleafCore;
use Sts\PleafCore\BusinessTransaction;
use Sts\PleafCore\DefaultBusinessTransaction;
use Sts\PleafCommon\Model\Policy;
use Validator;
use Log;
use App;
use Sts\PleafCore\CoreException;


class AddPolicy extends DefaultBusinessTransaction implements BusinessTransaction {

    public function getDescription(){
    	return "Add Policy";
    }

    public function prepare ($dto, $originalDto){
 		$this->valUniqKey($dto);
      
    }
    
	private function valUniqKey($dto){
       $findPolicyByIndex = App::make('findPolicyByIndex');
	   $userTenant = $dto['tenant_id'];
	   $policy_code = $dto['policy_code'];

	   $input = [
		   "tenant_id"=>$userTenant,
		   "policy_code"=>$policy_code

		];

		$output = $findPolicyByIndex->execute($input);

		$policy = $output['edit_policy'];
		//validasi business
		if($policy!=null) throw new CoreException(ERROR_BUSINESS_VALIDATION, [],
			["policy_code"=>"Policy with code ".$policy_code." Already Exists"]);

	 }
    
    // validasi untuk Add input pada Policy 
    protected function rules(){
    	return[
    			'tenant_id'=>'required|val_tenant',
    			'policy_code'=> 'required',
    			'policy_name'=> 'required'
    	];
    	
    }
    
	public function process ($dto, $originalDto){
		$userLoginId = $dto["userLoginId"];
		$datetime = $dto["datetime"];
		
		$policy = new Policy;
		$policy->tenant_id = $dto['tenant_id'];
        $policy->policy_code = strtoupper($dto['policy_code']);
        $policy->policy_name = $dto['policy_name'];
       
        self::auditActive($policy, $datetime);
        self::auditInsert($policy, $userLoginId, $datetime);
        
        
		$policy->save();
		
    		
	}
}