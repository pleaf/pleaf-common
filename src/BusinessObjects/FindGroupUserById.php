<?php
namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use Sts\PleafCore\DefaultBusinessFunction;
use DB;
use Sts\PleafCommon\Model\GroupUser;
/**
 * @in
 *   - keyword
 */
class FindGroupUserById extends DefaultBusinessFunction implements BusinessFunction {

    public function getDescription(){
        return "Find Group User By Id";
    }

    public function process($dto){

        $groupUserId = $dto['groupUserId'];


        $groupUser = GroupUser::where('group_user_id','=',$groupUserId)
                        ->first();
        return [
            "groupUser" => $groupUser
        ];
    }
}
