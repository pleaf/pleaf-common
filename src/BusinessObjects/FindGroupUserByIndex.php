<?php
namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use Sts\PleafCore\DefaultBusinessFunction;
use DB;
use Sts\PleafCommon\Model\GroupUser;

class FindGroupUserByIndex extends DefaultBusinessFunction implements BusinessFunction {

	public function getDescription(){
    	return "Find Group User By Code";
    }

    public function process($dto){

        $groupUserCode = strtoupper($dto['groupUserCode']);
        $tenant_id  = $dto['tenantId'];


        $groupUser = GroupUser::where('group_user_code','=',$groupUserCode)
                            ->where('tenant_id','=',$tenant_id)
                            ->first();
        return[
            "groupUser" => $groupUser
        ];
    }

    protected function rules() {
        return [
            "groupUserCode" => "required|min:3|max:3"
        ];
    }
}
