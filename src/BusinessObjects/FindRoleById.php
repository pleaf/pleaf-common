<?php

namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use DB;
use Sts\PleafCommon\Model\Role;
use Log;
use Sts\PleafCore\DefaultBusinessFunction;

/**
 *
 * @in
 *
 * @out
 */

class FindRoleById extends DefaultBusinessFunction implements BusinessFunction {

    public function getDescription(){
        return "Find Role By Id";
    }

    public function process($dto){

        $role_id = $dto["id"];

        $role =  Role::find($role_id);

        return $role;
    }

    public function rules(){

        return [
            "id" => "required"
        ];

    }
}
