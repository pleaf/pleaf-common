<?php
namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use Sts\PleafCore\DefaultBusinessFunction;
use DB;
use Sts\PleafCommon\Model\OU;

class FindOUByIndex extends DefaultBusinessFunction implements BusinessFunction {

	public function getDescription(){
    	return "Find OU By Index";
    }

    public function process($dto){

        $OUCode = strtoupper($dto['ou_code']);
        $tenant_id  = $dto['tenant_id'];


        $OU = OU::where('ou_code','=',$OUCode)
                            ->where('tenant_id','=',$tenant_id)
                            ->first();
        return[
            "OU" => $OU
        ];
    }

    protected function rules() {
        return [
            "ou_code" => "required"
        ];
    }
}