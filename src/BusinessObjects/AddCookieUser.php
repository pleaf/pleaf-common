<?php

namespace Sts\PleafCommon\BO;

use Sts\PleafCore as PleafCore;
use Sts\PleafCore\BusinessTransaction;
use Sts\PleafCore\DefaultBusinessTransaction;
use Sts\PleafCommon\Model\CookieUser;
use Log;

class AddCookieUser extends DefaultBusinessTransaction implements BusinessTransaction {

    public function getDescription(){
    	return "changes here";
    }

    public function prepare ($dto, $originalDto){
        // Add Validation Business

    }
    
	public function process ($dto, $originalDto){
		
        $datetime=$dto['datetime'];
        $user_id=$dto['user_id'];
        $token = $dto['token'];
        $series_identifier=$dto['series_identifier'];

        $cookie = new CookieUser;

        $cookie->user_id=$user_id;
        $cookie->token=$token;
        $cookie->series_identifier=$series_identifier;
        $cookie->create_datetime=$datetime;
        $cookie->save();

	}

   
}