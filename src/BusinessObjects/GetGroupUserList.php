<?php
namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessFunction;
use DB;
/**
 * @in
 *   - keyword
 */
class GetGroupUserList implements BusinessFunction {

    public function getDescription(){
        return "Get Group User List";
    }

    public function execute($dto){

        $tenantId = $dto['tenantId'];


        $groupUserList = \DB::select("SELECT A.group_user_id AS group_id,A.group_user_code,A.group_user_name,
                A.group_user_desc,COALESCE(B.group_user_id,0) AS group_user_id,COALESCE(COUNT(B.group_members_id),0) AS total
            FROM t_group_user A
            LEFT JOIN t_group_members B
            ON A.group_user_id = B.group_user_id
            WHERE A.tenant_id = $tenantId
            GROUP BY  A.group_user_id,A.group_user_code,A.group_user_name,A.group_user_desc,B.group_user_id
            ORDER BY A.group_user_code ASC, A.group_user_name ASC ");

        return [
            "groupUserList" => $groupUserList
        ];
    }
}
