<?php
namespace Sts\PleafCommon\BO;

use Sts\PleafCore\BusinessTransaction;
use Sts\PleafCore\DefaultBusinessTransaction;
use Sts\PleafCommon\Model\Policy;
use Log;
use DB;

class RemovePolicy extends DefaultBusinessTransaction implements BusinessTransaction {

    public function getDescription(){
        return "xxx";
    }

    public function prepare ($dto, $originalDto){

    }
    public function process ($dto, $originalDto){
        
        $policy_id = $dto['policy_id'];

        $policy = Policy::find($policy_id);
        if($policy!=null)$policy->delete();

            
    }
}