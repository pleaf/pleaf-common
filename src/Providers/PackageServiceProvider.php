<?php

namespace Sts\PleafCommon\Providers;

use Illuminate\Support\ServiceProvider;

// Section Tenant
use Sts\PleafCommon\BO\AddTenant;
use Sts\PleafCommon\BO\EditTenant;
use Sts\PleafCommon\BO\RemoveTenant;
use Sts\PleafCommon\BO\GetTenantList;
use Sts\PleafCommon\BO\FindTenantById;
use Sts\PleafCommon\BO\AddCreateInitialTenant;

// Section Role
use Sts\PleafCommon\BO\AddRole;
use Sts\PleafCommon\BO\EditRole;
use Sts\PleafCommon\BO\RemoveRole;
use Sts\PleafCommon\BO\GetRoleList;
use Sts\PleafCommon\BO\FindRoleById;
use Sts\PleafCommon\BO\GetRoleType;
use Sts\PleafCommon\BO\FindRoleByIndex;
use Sts\PleafCommon\BO\FindRoleByIdForEdit;

// Section Role Task
use Sts\PleafCommon\BO\AddRoleTask;
use Sts\PleafCommon\BO\RemoveRoleTask;
use Sts\PleafCommon\BO\GetRoleTaskList;
use Sts\PleafCommon\BO\GetUserRoleTaskList;

// Section Policy
use Sts\PleafCommon\BO\AddPolicy;
use Sts\PleafCommon\BO\EditPolicy;
use Sts\PleafCommon\BO\RemovePolicy;
use Sts\PleafCommon\BO\GetPolicyList;
use Sts\PleafCommon\BO\FindPolicyById;
use Sts\PleafCommon\BO\FindPolicyByIndex;

// Section Ou Type
use Sts\PleafCommon\BO\AddOUType;
use Sts\PleafCommon\BO\EditOUType;
use Sts\PleafCommon\BO\RemoveOUType;
use Sts\PleafCommon\BO\GetOUTypeList;
use Sts\PleafCommon\BO\FindOUTypeById;
use Sts\PleafCommon\BO\FindOUTypeByIndex;

// Section User
use Sts\PleafCommon\BO\AddUser;
use Sts\PleafCommon\BO\EditUser;
use Sts\PleafCommon\BO\RemoveUser;
use Sts\PleafCommon\BO\RemoveUserRole;
use Sts\PleafCommon\BO\GetUserList;
use Sts\PleafCommon\BO\FindUserById;
use Sts\PleafCommon\BO\FindUserByIndex;
use Sts\PleafCommon\BO\AddManageUserRole;
use Sts\PleafCommon\BO\GetManageUserRoleList;
use Sts\PleafCommon\BO\FindUserRoleByIndex;

//Section User Login
use Sts\PleafCommon\BO\FindUserLoginByIndex;
use Sts\PleafCommon\BO\AddUserLogin;
use Sts\PleafCommon\BO\UpdateUserLogin;


// Section OU
use Sts\PleafCommon\BO\AddOU;
use Sts\PleafCommon\BO\EditOU;
use Sts\PleafCommon\BO\RemoveOU;
use Sts\PleafCommon\BO\GetOUList;
use Sts\PleafCommon\BO\FindOUById;
use Sts\PleafCommon\BO\FindOUByIndex;
use Sts\PleafCommon\BO\GetOuCreateList;

// Section System Config
use Sts\PleafCommon\BO\EditSystemConfig;
use Sts\PleafCommon\BO\GetSystemConfigList;
use Sts\PleafCommon\BO\GetSystemConfigDetail;

// Section Combo Constant
use Sts\PleafCommon\BO\AddComboConstant;
use Sts\PleafCommon\BO\EditComboConstant;
use Sts\PleafCommon\BO\RemoveComboConstant;
use Sts\PleafCommon\BO\GetComboConstantList;
use Sts\PleafCommon\BO\FindComboConstantById;
use Sts\PleafCommon\BO\FindComboConstantByIndex;

// Section DB Query
use Sts\PleafCommon\BO\DBQuery;

// Section Group User
use Sts\PleafCommon\BO\AddGroupUser;
use Sts\PleafCommon\BO\EditGroupUser;
use Sts\PleafCommon\BO\RemoveGroupUser;
use Sts\PleafCommon\BO\GetGroupUserList;
use Sts\PleafCommon\BO\FindGroupUserById;
use Sts\PleafCommon\BO\GetMembersByGroupUserId;
use Sts\PleafCommon\BO\GetUsersDetailFromGroupMember;
use Sts\PleafCommon\BO\RemoveMemberFromGroupUser;
use Sts\PleafCommon\BO\AddJoinMember;
use Sts\PleafCommon\BO\FindGroupUserByIndex;

// Section Login Password
use Sts\PleafCommon\BO\AddForgotPassword;
use Sts\PleafCommon\BO\GetSessionLogin;
use Sts\PleafCommon\BO\GetForgotPassword;
use Sts\PleafCommon\BO\UpdateUserForgot; //---> Nama file masih belum sesuai
use Sts\PleafCommon\BO\GetLoggedUserData;
use Sts\PleafCommon\BO\SetNewPassword;
use Sts\PleafCommon\BO\ResetUserPassword;
use Sts\PleafCommon\BO\GetListRole;

// Section Project
use Sts\PleafCommon\BO\AddProject;
use Sts\PleafCommon\BO\EditProject;
use Sts\PleafCommon\BO\GetProjectList;

// SECTION COOKIE
use Sts\PleafCommon\BO\AddCookieUser;
use Sts\PleafCommon\BO\RemoveCookieUser;
use Sts\PleafCommon\BO\GetCookieUser;

// GENERAL
use Sts\PleafCommon\Core\MenuManager;
use Sts\PleafCommon\BO\GetRoleListForManagerRole;

// MANAGE USER TENANT
use Sts\PleafCommon\BO\AddPolicyTenantUser;
use Sts\PleafCommon\BO\GetManageUserTenantList;
use Sts\PleafCommon\BO\GetPolicyTenantUserList;
use Sts\PleafCommon\BO\RemovePolicyTenantUser;

class PackageServiceProvider extends ServiceProvider {

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        require __DIR__ . '/../routes.php';
        require __DIR__ . '/../Helpers/navigation_menu.php';
        // Load views
        $this->loadViewsFrom(__DIR__.'/../views', 'pleaf-common');

        // Publish assets
        $this->publishes([
            __DIR__.'/../assets' => public_path('sts/pleaf-common')
        ], 'pleaf-common');

        // Publish config
        $this->publishes([
            __DIR__.'/../config/pleaf_config.php' => config_path('pleaf_config.php'),
        ], 'pleaf-config');

        // Publish to public folder
        $this->publishes([
            __DIR__.'/../public' => public_path(),
        ], 'pleaf-public');


    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // SECTION TENANT
        $this->app->singleton('addTenant', function(){  return new AddTenant(); });
        $this->app->singleton('addCreateInitialTenant', function(){  return new AddCreateInitialTenant(); });
        $this->app->singleton('getTenantList', function(){  return new GetTenantList(); });
        $this->app->singleton('findTenantById', function(){  return new FindTenantById(); });
        $this->app->singleton('editTenant', function(){  return new EditTenant(); });
        $this->app->singleton('removeTenant', function(){  return new RemoveTenant(); });

        //SECTION  POLICY
        $this->app->singleton('getPolicyList', function(){  return new GetPolicyList(); });
        $this->app->singleton('addPolicy', function(){  return new AddPolicy(); });
        $this->app->singleton('removePolicy', function(){  return new RemovePolicy(); });
        $this->app->singleton('findPolicyById', function(){  return new FindPolicyById(); });
        $this->app->singleton('findPolicyByIndex', function(){  return new FindPolicyByIndex(); });
        $this->app->singleton('editPolicy', function(){  return new EditPolicy(); });

        //SECTION OU TYPE
        $this->app->singleton('getOUTypeList', function(){ return new GetOUTypeList(); });
        $this->app->singleton('addOUType',function(){ return new AddOUType(); });
        $this->app->singleton('removeOUType', function() { return new RemoveOUType(); });
        $this->app->singleton('findOUTypeById', function() { return new FindOUTypeById(); });
        $this->app->singleton('findOUTypeByIndex', function() { return new FindOUTypeByIndex(); });
        $this->app->singleton('editOUType', function() { return new EditOUType(); });

        //SECTION OU
        $this->app->singleton('getOUList', function() { return new GetOUList(); });
        $this->app->singleton('getOuCreateList', function() { return new GetOuCreateList(); });
        $this->app->singleton('addOU',function(){return new AddOU(); });
        $this->app->singleton('editOU', function(){  return new EditOU(); });
        $this->app->singleton('removeOU', function() { return new RemoveOU(); });
        $this->app->singleton('findOUById', function() { return new FindOUById(); });
        $this->app->singleton('findOUByIndex', function() { return new FindOUByIndex(); });

        //SECTION ROLE
        $this->app->singleton('getRoleListCommon', function() { return new GetRoleList(); });
        $this->app->singleton('findRoleByIdCommon', function() { return new FindRoleById(); });
        $this->app->singleton('findRoleByIndex', function() { return new FindRoleByIndex(); });
        $this->app->singleton('getListRole', function() { return new GetListRole(); });
        $this->app->singleton('getRoleType', function() { return new GetRoleType(); });
        $this->app->singleton('addRole',function(){ return new AddRole(); });
        $this->app->singleton('findRoleByIdForEdit', function(){  return new FindRoleByIdForEdit(); });
        $this->app->singleton('editRole', function() { return new EditRole(); });
        $this->app->singleton('removeRole', function() { return new RemoveRole(); });
        $this->app->singleton('getRoleTaskList', function() { return new GetRoleTaskList(); });
        $this->app->singleton('addRoleTask',function(){ return new AddRoleTask(); });
        $this->app->singleton('getUserRoleTaskList',function(){ return new GetUserRoleTaskList();
        });
        $this->app->singleton('removeRoleTask', function() { return new RemoveRoleTask(); });

        //SECTION USER
        $this->app->singleton('getUserListCommon', function(){ return new GetUserList(); });
        $this->app->singleton('addUserCommon',function(){ return new AddUser(); });
        $this->app->singleton('addManageUserRole',function(){ return new AddManageUserRole(); });
        $this->app->singleton('removeUser', function() { return new RemoveUser(); });
        $this->app->singleton('removeUserRole', function() { return new RemoveUserRole(); });
        $this->app->singleton('findUserByIndex', function() { return new FindUserByIndex(); });
        $this->app->singleton('findUserByIdCommon', function() { return new FindUserById(); });
        $this->app->singleton('editUser', function() { return new EditUser(); });
        $this->app->singleton('getManageUserRoleList', function() { return new GetManageUserRoleList(); });
        $this->app->singleton('findUserRoleByIndex', function() { return new FindUserRoleByIndex(); });
        $this->app->singleton('getRoleListForManagerRole', function() { return new GetRoleListForManagerRole(); });

        // SECTION USER LOGIN
        $this->app->singleton('findUserLoginByIndex', function() { return new FindUserLoginByIndex(); });
        $this->app->singleton('addUserLogin', function() { return new AddUserLogin(); });
        $this->app->singleton('updateUserLogin', function() { return new UpdateUserLogin(); });

        //SECTION COMBO CONSTANT
        $this->app->singleton('addComboConstant', function(){  return new AddComboConstant(); });
        $this->app->singleton('getComboConstantList', function() { return new GetComboConstantList(); });
        $this->app->singleton('findComboConstantById', function(){  return new FindComboConstantById(); });
        $this->app->singleton('editComboConstant', function(){  return new EditComboConstant(); });
        $this->app->singleton('removeComboConstant', function(){  return new RemoveComboConstant(); });
        $this->app->singleton('findComboConstantByIndex', function(){  return new FindComboConstantByIndex(); });
        $this->app->singleton('getSystemConfigList', function() { return new GetSystemConfigList(); });
        $this->app->singleton('getSystemConfigDetail', function() { return new GetSystemConfigDetail(); });
        $this->app->singleton('editSystemConfig', function() { return new EditSystemConfig(); });
        $this->app->singleton('dBQuery', function() { return new DBQuery(); });

        //SECTION GROUP USER
        $this->app->singleton('getGroupUserList', function() { return new GetGroupUserList(); });
        $this->app->singleton('addGroupUser', function() { return new AddGroupUser(); });
        $this->app->singleton('findGroupUserById', function(){return new FindGroupUserById(); });
        $this->app->singleton('editGroupUser', function(){return new EditGroupUser(); });
        $this->app->singleton('removeGroupUser', function(){return new RemoveGroupUser(); });
        $this->app->singleton('getMembersByGroupUserId', function(){return new GetMembersByGroupUserId(); });
        $this->app->singleton('removeMemberFromGroupUser', function(){return new RemoveMemberFromGroupUser(); });
        $this->app->singleton('addJoinMember', function(){return new AddJoinMember(); });
        $this->app->singleton('findGroupUserByIndex', function(){return new FindGroupUserByIndex(); });
        $this->app->singleton('getUsersDetailFromGroupMember', function(){return new GetUsersDetailFromGroupMember(); });


        //SECTION SESSION USER
        $this->app->singleton('getSessionLogin', function() { return new GetSessionLogin(); });
        $this->app->singleton('addForgotPassword', function() { return new AddForgotPassword(); });
        $this->app->singleton('getForgotPassword', function() { return new GetForgotPassword(); });
        $this->app->singleton('updateUserForgot', function() { return new updateUserForgot(); });
        $this->app->singleton('getLoggedUserData', function() { return new GetLoggedUserData(); });
        $this->app->singleton('setNewPassword', function() { return new SetNewPassword(); });
        $this->app->singleton('resetUserPassword',function(){return new ResetUserPassword();});

        //SECTION PROJECT
        $this->app->singleton('addProject', function(){  return new AddProject(); });
        $this->app->singleton('getProjectList', function(){  return new GetProjectList(); });
        $this->app->singleton('editProject', function(){  return new EditProject(); });

        //SECTION COOKIE USER
        $this->app->singleton('addCookieUser', function() { return new AddCookieUser(); });
        $this->app->singleton('removeCookieUser', function() { return new RemoveCookieUser(); });
        $this->app->singleton('getCookieUser', function(){  return new GetCookieUser(); });

        // MENU MANAGER
        $this->app->singleton('menuManager',function() { return new MenuManager(); });

        //  MANAGE USER TENANT
        $this->app->singleton("getManageUserTenantList", function () { return new GetManageUserTenantList(); });
        $this->app->singleton("getPolicyTenantUserList", function () { return new GetPolicyTenantUserList(); });
        $this->app->singleton("addPolicyTenantUser", function () { return new AddPolicyTenantUser(); });
        $this->app->singleton("removePolicyTenantUser", function () { return new RemovePolicyTenantUser(); });

    }
}